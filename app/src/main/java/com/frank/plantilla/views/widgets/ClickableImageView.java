package com.frank.plantilla.views.views.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatImageView;

@SuppressLint("DrawAllocation")
public class ClickableImageView extends AppCompatImageView {

    private boolean mIsSelected;

    public ClickableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void init() {
        mIsSelected = false;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (!enabled) {
            getDrawable().setColorFilter(0x99000000, PorterDuff.Mode.SRC_ATOP);
        } else {
            getDrawable().clearColorFilter();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {


        if (isEnabled()) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_BUTTON_PRESS:
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_HOVER_ENTER:
                    if (!mIsSelected) {
                        getDrawable().setColorFilter(0x99000000, PorterDuff.Mode.SRC_ATOP);
                        mIsSelected = true;
                    }
                    break;

                case MotionEvent.ACTION_BUTTON_RELEASE:
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_HOVER_EXIT:
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_CANCEL:
                    if (mIsSelected) {
                        getDrawable().clearColorFilter();
                        mIsSelected = false;
                    }
                    break;
            }
        }

        return super.onTouchEvent(event);
    }

}
