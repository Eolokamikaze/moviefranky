package com.frank.plantilla.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.frank.plantilla.R;
import com.frank.plantilla.model.adapters.MovieListAdapter;
import com.frank.plantilla.model.adapters.VideoListAdapter;
import com.frank.plantilla.model.classes.blueprints.RestViewFragment;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.presenter.MovieDetailsPresenter;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.views.activities.YoutubeActivity;
import com.frank.plantilla.views.widgets.CustomScrollView;

import java.util.ArrayList;

public class MovieDetailFragment extends RestViewFragment implements Responses.OnResponse, View.OnClickListener, CustomScrollView.OnScroll {


    private MovieDetailsPresenter presenter;
    private MovieListAdapter adapter;

    public static MovieDetailFragment newInstance(Bundle args) {
        MovieDetailFragment fragment = new MovieDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_details, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }


    private void initViews(View view) {
        presenter = new MovieDetailsPresenter(this);
        onCreate(presenter);

        getActivity().findViewById(R.id.ivActionBarRight).setVisibility(View.GONE);
        ((ImageView) getActivity().findViewById(R.id.ivActionBarLeft)).setImageResource(R.drawable.icon_back_white);
        setViewInitialized(view, getArguments());

        CustomScrollView scroll = (CustomScrollView) view.findViewById(R.id.scroll);
        scroll.setOnScroll(this);
        presenter.getMovieDetail(getArguments().getString(Constants.Movie.ID));


    }

    @Override
    public void onScroll(CustomScrollView scroll, int l, int t, int oldl, int oldt) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            ((ImageView) getActivity().findViewById(R.id.ivActionBarLeft))
                    .setImageResource(R.drawable.menu_white);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResponse(String handlerCode, Object response, Object data) {
        switch (handlerCode) {
            case "ViewVideo":
                Bundle item = (Bundle) response;
                if (item.getString(Constants.Video.SITE).equals(Constants.Video.YOUTUBE)) {
                    Intent intent = new Intent(getActivity(), YoutubeActivity.class);
                    intent.putExtra(Constants.Video.KEY, item.getString(Constants.Video.KEY));
                    startActivity(intent);
                }
                break;
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivActionBarRight:
                Utils.fragmentChooser(R.id.flMain, MainSearchMovieListFragment.newInstance(),
                        getFragmentManager(), MainSearchMovieListFragment.class.getName());
                break;
        }
    }

    public void setViewInitialized(View view, Bundle arg) {
        TextView tvName = view.findViewById(R.id.tvMovieName);
        TextView tvDescription = view.findViewById(R.id.tvMovieDescription);
        TextView tvRate = view.findViewById(R.id.tvMovieRate);
        TextView tvMovies = view.findViewById(R.id.tvMovieRate);
        ImageView iv = view.findViewById(R.id.ivMovie);
        Bundle item = getArguments();
        tvName.setText(item.getString(Constants.Movie.TITLE));
        tvDescription.setText(item.getString(Constants.Movie.OVERVIEW));
        tvRate.setText("Calificación: " + item.getString(Constants.Movie.VOTE_AVERAGE));
        String imgUrl = Constants.Endpoints.IMAGE_PATH + item.getString(Constants.Movie.POSTER_PATH);
        Glide.with(getContext()).load(imgUrl).apply(Utils.getOptionFitCenter()).into(iv);
    }

    public void showError(String msg) {
        showMsg(Constants.Service.DIALOG_DISCLAIMER, msg);
    }


    public void setData(Bundle data) {
        TextView tvGenre = getView().findViewById(R.id.tvMovieGenre);
        TextView tvDate = getView().findViewById(R.id.tvMovieDate);
        TextView tvTagline = getView().findViewById(R.id.tvMovieTagline);

        String genre = "";
        ArrayList<Bundle> genreList = data.getParcelableArrayList(Constants.Movie.GENRES);
        if (genreList != null) {
            for (int i = 0; i < genreList.size(); i++) {
                genre += genreList.get(i).getString("name");
                if (i < genreList.size() - 1) {
                    genre += ",";
                }
            }
        }
        tvGenre.setText(genre);
        tvDate.setText("Estreno: " + data.getString(Constants.Movie.RELEASE_DATE));
        tvTagline.setText(data.getString(Constants.Movie.RELEASE_DATE));
    }

    public void setVideoList(ArrayList<Bundle> videoList) {
        TextView tvVideo = getView().findViewById(R.id.tvMovieVideos);
        if (videoList != null && videoList.size() > 0) {
            tvVideo.setText("Videos(" + videoList.size() + ")");
        } else {
            tvVideo.setText("Videos(0)");
        }
        RecyclerView rvList = getView().findViewById(R.id.rvMovieVideos);
        rvList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,
                false));
        VideoListAdapter adapter = new VideoListAdapter(videoList, getContext(), this);
        rvList.setAdapter(adapter);
    }
}
