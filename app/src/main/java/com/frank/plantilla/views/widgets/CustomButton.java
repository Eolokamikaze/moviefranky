package com.frank.plantilla.views.views.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.utils.CustomFontsLoader;


@SuppressLint("DrawAllocation")
public class CustomButton extends AppCompatButton {

    private Drawable mDrawableCenter;
    private boolean mIsSelected;

    public CustomButton(Context context){
        super(context);
        init(context, null);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_typeFace);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface typeface;
        try {
            typeface = CustomFontsLoader.getTypeface(ctx, asset);
        } catch (Exception e) {
            return false;
        }

        setTypeface(typeface);
        return true;
    }

    private void init(Context context, AttributeSet attrs){
        setCustomFont(context, attrs);

        if(attrs!=null){
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs, R.styleable.CustomButton, 0, 0);

            try {
                setCenterDrawable(a.getDrawable(R.styleable.CustomButton_drawableCenter));

            } finally {
                a.recycle();
            }
        }
    }

    public void setCenterDrawable(@Nullable Drawable center) {
        int[] state;
        state = getDrawableState();
        if (center != null) {
            center.setState(state);
            center.setBounds(0, 0, center.getIntrinsicWidth(), center.getIntrinsicHeight());
            center.setCallback(this);
        }
        mDrawableCenter = center;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if(mDrawableCenter!=null) {
            setMeasuredDimension(Math.max(getMeasuredWidth(), mDrawableCenter.getIntrinsicWidth()), Math.max(getMeasuredHeight(), mDrawableCenter.getIntrinsicHeight()));
        }
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (mDrawableCenter != null) {
            int[] state = getDrawableState();
            mDrawableCenter.setState(state);
            mDrawableCenter.setBounds(0, 0, mDrawableCenter.getIntrinsicWidth(), mDrawableCenter.getIntrinsicHeight());
        }
        invalidate();
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);

        if (mDrawableCenter != null) {
            Rect rect = mDrawableCenter.getBounds();
            canvas.save();
            canvas.translate(getTranslate(getWidth() / 2 - rect.right / 2), getHeight() / 2 - rect.bottom / 2);
            mDrawableCenter.draw(canvas);
            canvas.restore();
        }
    }

    private float getTranslate(int trans){
        String text = getText() != null? getText().toString() : "";
        float textWidth = text.length() > 0? new Paint().measureText(text) + 10 : 0;

        return trans + textWidth;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (isEnabled()) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_BUTTON_PRESS:
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_HOVER_ENTER:
                    if (!mIsSelected) {
                        getBackground().setColorFilter(0x99000000, PorterDuff.Mode.SRC_ATOP);
                        mIsSelected = true;
                    }
                    break;

                case MotionEvent.ACTION_BUTTON_RELEASE:
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_HOVER_EXIT:
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_CANCEL:
                    if (mIsSelected) {
                        getBackground().clearColorFilter();
                        mIsSelected = false;
                    }
                    break;
            }
        }

        return super.onTouchEvent(event);
    }

}