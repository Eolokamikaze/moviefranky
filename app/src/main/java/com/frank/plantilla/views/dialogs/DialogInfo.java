package com.frank.plantilla.views.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.frank.plantilla.R;
import com.frank.plantilla.retrofit.Responses;

/**
 * Created by Frank on 3/23/17.
 */

public class DialogInfo extends DialogFragment implements View.OnClickListener {

    private boolean cancelable = true;

    public static com.frank.plantilla.views.dialogs.DialogInfo newInstance(String dialogId, String title, String text, Bundle... data) {
        com.frank.plantilla.views.dialogs.DialogInfo mDialogInfo = new com.frank.plantilla.views.dialogs.DialogInfo();
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putString("title", title);
        args.putString("dialogId", dialogId);

        if (data != null && data.length > 0) {
            args.putBundle("data", data[0]);
        }

        mDialogInfo.setArguments(args);
        return mDialogInfo;
    }

    public void show(FragmentManager manager, String tag, boolean cancelable) {
        this.cancelable = cancelable;
        show(manager, tag);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.dialogFullScreen);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setCancelable(cancelable);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_alert, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        setCancelable(cancelable);
        getDialog().setCanceledOnTouchOutside(cancelable);

        ((TextView) view.findViewById(R.id.tvDialogTitle)).setText(getArguments().getString("title", ""));

        ((TextView) view.findViewById(R.id.tvDialog)).setText(getArguments().getString("text", ""));
        view.findViewById(R.id.tvDialogYes).setOnClickListener(this);

        View ivDialogClose = view.findViewById(R.id.ivDialogClose);

        ivDialogClose.setVisibility(cancelable ? View.VISIBLE : View.GONE);
        ivDialogClose.setOnClickListener(cancelable ? this : null);
    }

    private Responses.OnResponse getResponse() {
        Responses.OnResponse mResponse = null;

        if (getParentFragment() instanceof Responses.OnResponse) {
            mResponse = (Responses.OnResponse) getParentFragment();
        } else if (getActivity() instanceof Responses.OnResponse) {
            mResponse = (Responses.OnResponse) getActivity();
        }

        return mResponse;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDialogYes:

                dismiss();
                Responses.OnResponse mResponse = getResponse();
                if (mResponse != null) {
                    mResponse.onResponse(getArguments().getString("dialogId", ""), true, getArguments().getBundle("data"));
                }
                break;

            case R.id.ivDialogClose:
                dismiss();
                break;
        }
    }

}