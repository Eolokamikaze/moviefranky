package com.frank.plantilla.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.presenter.MainMovieListPresenter;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.model.adapters.MovieListAdapter;
import com.frank.plantilla.model.classes.blueprints.RestViewFragment;
import com.frank.plantilla.model.classes.constants.Constants;

import java.util.ArrayList;

public class MainMovieListFragment extends RestViewFragment implements Responses.OnResponse, View.OnClickListener {


    private MainMovieListPresenter presenter;
    private MovieListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pantalla_movie_list, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }


    private void initViews(View view) {
        presenter = new MainMovieListPresenter(this);
        onCreate(presenter);
        getActivity().findViewById(R.id.ivActionBarRight).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.ivActionBarRight).setOnClickListener(this);
        RecyclerView rvList = view.findViewById(R.id.rvListMovies);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MovieListAdapter(new ArrayList<>(), getContext(), this);
        rvList.setAdapter(adapter);
        presenter.getMovieList();
    }


    @Override
    public void onResponse(String handlerCode, Object response, Object data) {
        switch (handlerCode) {
            case "ViewDetail":
                Utils.fragmentChooser(R.id.flMain, MovieDetailFragment.newInstance((Bundle) response),
                        getParentFragmentManager(), MovieDetailFragment.class.getName());
                break;
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivActionBarRight:
                Utils.fragmentChooser(R.id.flMain, MainSearchMovieListFragment.newInstance(),
                        getFragmentManager(), MainSearchMovieListFragment.class.getName());
                break;
        }
    }

    public void showError(String msg) {
        showMsg(Constants.Service.DIALOG_DISCLAIMER, msg);
    }

    public void sendData(ArrayList<Bundle> moviesList) {

        adapter.setItems(moviesList);

    }

    public void setData(ArrayList<Bundle> list) {
        adapter.setItems(list);
    }
}
