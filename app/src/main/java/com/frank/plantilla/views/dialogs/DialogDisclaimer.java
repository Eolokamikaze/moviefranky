package com.frank.plantilla.views.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.R;



public class DialogDisclaimer extends DialogFragment implements View.OnClickListener {

    private boolean cancelable = false;

    public static DialogDisclaimer newInstance(String dialogId, String title, String text, Bundle... data) {
        DialogDisclaimer mDialogChoice = new DialogDisclaimer();
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putString("title", title);
        args.putString("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static DialogDisclaimer newInstance(String dialogId, String title, String text, boolean fromConnectivity) {
        DialogDisclaimer mDialogChoice = new DialogDisclaimer();
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putString("title", title);
        args.putString("dialogId", dialogId);
        args.putBoolean("fromConnectivity", fromConnectivity);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static DialogDisclaimer newInstanceSubText(String dialogId, String title, String text, String subText, String textButton, Bundle... data) {
        DialogDisclaimer mDialogChoice = new DialogDisclaimer();
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putString("subText", subText);
        args.putString("title", title);
        args.putString("textButton", textButton);
        args.putString("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static DialogDisclaimer newInstance(String dialogId, String title, String text, String textButton, Bundle... data) {
        DialogDisclaimer mDialogChoice = new DialogDisclaimer();
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putString("title", title);
        args.putString("textButton", textButton);
        args.putString("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static DialogDisclaimer newInstance(int dialogId, String title, String text, String textButton, Bundle... data) {
        DialogDisclaimer mDialogChoice = new DialogDisclaimer();
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putString("title", title);
        args.putString("textButton", textButton);
        args.putInt("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static DialogDisclaimer newInstance(int dialogId, String title, SpannableString text, Bundle... data) {
        DialogDisclaimer mDialogChoice = new DialogDisclaimer();
        Bundle args = new Bundle();
        args.putCharSequence("text", text);
        args.putString("title", title);
        args.putInt("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
//            super.show(manager, tag);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.dialogFullScreen);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setCancelable(cancelable);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_disclaimer, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        setCancelable(cancelable);
        getDialog().setCanceledOnTouchOutside(false);

        ((TextView) view.findViewById(R.id.tvDialogTitle)).setText(getArguments().getString("title"));

        CharSequence sequence = getArguments().getCharSequence("text");
        SpannableString spannableText;

        try {
            spannableText = (SpannableString) sequence;
        } catch (Exception e) {
            spannableText = null;
        }

        if (getArguments().getBoolean("fromConnectivity")) {
            view.findViewById(R.id.ivDialogTitle).setVisibility(View.VISIBLE);
        }

        ((TextView) view.findViewById(R.id.tvDialog)).setText(spannableText != null ? spannableText : getArguments().getString("text"));
        if (Utils.stringNotNull(getArguments().getString("textButton"))) {
            ((TextView) view.findViewById(R.id.tvDialogYes)).setText(getArguments().getString("textButton"));
        }
        if (Utils.stringNotNull(getArguments().getString("subText", ""))) {
            view.findViewById(R.id.tvDialogSubcontent).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.tvDialogSubcontent)).setText(getArguments().getString("subText"));
        }
        view.findViewById(R.id.tvDialogYes).setOnClickListener(this);
        view.findViewById(R.id.ivDialogClose).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.ivDialogClose:
                dismiss();
                break;

            case R.id.tvDialogYes:
                if (getParentFragment() instanceof Responses.OnResponse) {
                    ((Responses.OnResponse) getParentFragment()).onResponse(getArguments().getString("dialogId"), true, getArguments().getBundle("data"));
                } else if (getActivity() instanceof Responses.OnResponse) {
                    ((Responses.OnResponse) getActivity()).onResponse(getArguments().getString("dialogId"), true, getArguments().getBundle("data"));
                }
                dismiss();
                break;

        }
    }

}
