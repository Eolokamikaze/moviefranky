package com.frank.plantilla.views.fragments;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frank.plantilla.R;
import com.frank.plantilla.presenter.MenuPresenter;
import com.frank.plantilla.model.adapters.MenuAdapter;
import com.frank.plantilla.model.classes.blueprints.RestViewFragment;
import com.frank.plantilla.model.classes.constants.Constants;

import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.views.activities.MainActivity;

import java.util.ArrayList;

public class MenuFragment extends RestViewFragment implements Responses.OnResponse, View.OnClickListener {

    private final static String DIALOG_LOGOUT = "102";
    private final static String DIALOG_STATUS = "103";

    private MenuAdapter menuAdapter;

    private TextView tvMenuCompany;
    private TextView tvMenuName;
    private TextView tvMenuPosition;

    private RadioGroup rgMenuStatus;

    private BroadcastReceiver mStatusChangeReceiver;
    private BroadcastReceiver mTicketReceived;
    private MenuPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mStatusChangeReceiver, new IntentFilter(Constants.Receiver.STATUS_CHANGE));
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mTicketReceived, new IntentFilter(Constants.Receiver.TICKETS_RECEIVED));

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mStatusChangeReceiver);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mTicketReceived);
        super.onPause();
    }


    public void refreshMenu() {
        if (menuAdapter != null) {
            menuAdapter.notifyDataSetChanged();
        }
    }

    private ArrayList<Bundle> createList() {
        String[] titles = getResources().getStringArray(R.array.menulistTitle);
        String[] sections = getResources().getStringArray(R.array.menulistSection);
        ArrayList<Bundle> list = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            Bundle b = new Bundle();
            b.putString(Constants.Menu.TITLE, titles[i]);
            b.putString(Constants.Menu.SECTION, sections[i]);
            list.add(b);
        }
        return list;
    }

    private void initViews(View view) {

        presenter = new MenuPresenter(this);
        onCreate(presenter);


        view.findViewById(R.id.bMenuLogout).setOnClickListener(this);
        view.findViewById(R.id.llMenuUser).setOnClickListener(this);
        view.findViewById(R.id.bMenuStatus).setOnClickListener(this);


        RecyclerView lvMenu = (RecyclerView) view.findViewById(R.id.lvMenuSection);


        try {
            lvMenu.setLayoutManager(new LinearLayoutManager(getContext()));
            ArrayList<Bundle> menuList = createList();
            menuAdapter = new MenuAdapter(menuList, getContext(), this);
            menuAdapter.setSelected(0);
            lvMenu.setAdapter(menuAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void removeSection(ArrayList<Bundle> list, String type) {
        int pos = -1;
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getString("type", "").equals(type)) {
                    pos = i;
                    break;
                }
            }
        }
        if (pos >= 0) {
            list.remove(pos);
        }
    }




    @Override
    public void onResponse(String handlerCode, Object o, Object m) {
        int position = 0;

        try {
            clearStack();
//            ImageView iv = (ImageView) getActivity().findViewById(R.id.ivActionBarLeft);
//            iv.setImageResource(R.drawable.btn_menu);
        } catch (Exception e) {

        }

        if (menuAdapter != null) {
            ((MainActivity) getActivity()).closeMenu();
        }
        ArrayList<Bundle> menuList = createList();
        Bundle item = menuList.get((int) o);
        String section = item.getString(Constants.Menu.SECTION);
        ((MainActivity) getActivity()).setActionTitle(item.getString(Constants.Menu.TITLE));
        menuAdapter.setSelected((int)o);
        switch (section) {
            case Constants.Sections.SECTION_POPULARES:
                Utils.fragmentChooser(R.id.flMain, new MainMovieListFragment(), getActivity().getSupportFragmentManager(),
                        Constants.Sections.SECTION_POPULARES);
                break;

            case Constants.Sections.SECTION_RATE:
                Utils.fragmentChooser(R.id.flMain, new MainMovieRatedFragment(), getActivity().getSupportFragmentManager(),
                        Constants.Sections.SECTION_RATE);
                break;

        }
    }

    private void clearStack() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setTitle(Bundle section) {
        if (!section.getString("type", "").equals(Constants.Sections.ABOUT)) {
            ((MainActivity) getActivity()).setActionTitle(section.getString("title", getString(R.string.app_name)));
        } else {
            ((MainActivity) getActivity()).setActionTitle("");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    public void showError(String msg) {
        showMsg(Constants.Service.DIALOG_DISCLAIMER, msg);
    }
}
