package com.frank.plantilla.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.receivers.LocationUpdatesService;
import com.frank.plantilla.model.classes.utils.PowerSaverHelper;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.views.fragments.MainMovieListFragment;
import com.frank.plantilla.views.fragments.MenuFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private DrawerLayout mDrawerLayout;
    private TextView tvActionBar;
    private View tvMainOffline;
    private ImageView ivActionBarRight;
    private ImageView ivActionBarLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initViews();
        setToolbar();
    }

    public void setActionTitle(String title) {
        tvActionBar.setText(title);
    }


    private void setToolbar() {
        Toolbar tbActionBar = (Toolbar) findViewById(R.id.tbActionBar);
        tbActionBar.setTitle("");
        tbActionBar.setSubtitle("");


//        ivActionBarRight = (ImageView) findViewById(R.id.ivActionBarRight);


//        ivActionBarRight.setOnClickListener(this);
//        findViewById(R.id.ivActionBarNotification).setOnClickListener(this);

        tvActionBar = (TextView) tbActionBar.findViewById(R.id.tvActionBar);
        ArrayList<Bundle> menuList = Utils.createList(this);
        tvActionBar.setText(menuList.get(0).getString(Constants.Menu.TITLE));
        setSupportActionBar(tbActionBar);

    }

    private void setMenu() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.dlMenu);
    }

    public void openMenu() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeMenu() {
        mDrawerLayout.closeDrawers();
    }


    private void initViews() {

        setMenu();
        Utils.fragmentChooserWithoutAddToBackStack(R.id.flMenu, new MenuFragment(), getSupportFragmentManager(), Constants.Sections.MENU_FRAGMENT);
        Utils.fragmentChooser(R.id.flMain, new MainMovieListFragment(), getSupportFragmentManager(),
                Constants.Sections.SECTION_POPULARES);
        ivActionBarLeft = (ImageView) findViewById(R.id.ivActionBarLeft);
        ivActionBarLeft.setOnClickListener(this);

//        if (Utils.isUserActive(getBaseContext())) {
//            if (!checkPermissions()) {
//                requestPermissions();
//            }
//        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivActionBarLeft:
                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    openMenu();
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }

    }

    public void requestLocationUpdates(boolean request) {
        Intent intent = PowerSaverHelper.prepareIntentForWhiteListingOfBatteryOptimization(
                getBaseContext(), getApplicationContext().getPackageName(), false);
        if (intent != null) {
            startActivity(intent);
        }

        Intent locationUpdateService = new Intent(getBaseContext(), LocationUpdatesService.class);
        if (request) {
            startService(locationUpdateService);
        } else {
            stopService(locationUpdateService);
        }


    }


}