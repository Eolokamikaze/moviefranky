package com.frank.plantilla.views.views.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.utils.CustomFontsLoader;

public class CustomEditText extends AppCompatEditText {

    TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String txt = getText().toString();

            if (txt.length() == 1) {
                removeTextChangedListener(textWatcher);
                setText(txt.toUpperCase());
                setSelection(txt.length());
                addTextChangedListener(textWatcher);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_typeFace);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface typeface;
        try {
            typeface = CustomFontsLoader.getTypeface(ctx, asset);
        } catch (Exception e) {
            return false;
        }

        setTypeface(typeface);
        return true;
    }
}
