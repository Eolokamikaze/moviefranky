package com.frank.plantilla.views.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.frank.plantilla.R;
import com.frank.plantilla.retrofit.Responses;

/**
 * Created by Frank on 3/23/17.
 */

public class DialogChoice extends DialogFragment implements View.OnClickListener {

    private boolean cancelable = true;

    public static com.frank.plantilla.views.dialogs.DialogChoice newInstance(String dialogId, String title, String text, Bundle... data) {
        com.frank.plantilla.views.dialogs.DialogChoice mDialogChoice = new com.frank.plantilla.views.dialogs.DialogChoice();
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putString("title", title);
        args.putString("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static com.frank.plantilla.views.dialogs.DialogChoice newInstance(String dialogId, String title, String text, String yesText, String noText, Bundle... data) {
        com.frank.plantilla.views.dialogs.DialogChoice mDialogChoice = new com.frank.plantilla.views.dialogs.DialogChoice();
        Bundle args = new Bundle();
        args.putString("yesText", yesText);
        args.putString("noText", noText);
        args.putString("text", text);
        args.putString("title", title);
        args.putString("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static com.frank.plantilla.views.dialogs.DialogChoice newInstance(int dialogId, String title, SpannableString text, Bundle... data) {
        com.frank.plantilla.views.dialogs.DialogChoice mDialogChoice = new com.frank.plantilla.views.dialogs.DialogChoice();
        Bundle args = new Bundle();
        args.putCharSequence("text", text);
        args.putString("title", title);
        args.putInt("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        mDialogChoice.setArguments(args);
        return mDialogChoice;
    }

    public static com.frank.plantilla.views.dialogs.DialogChoice newInstance(String dialogId, String title, String text, String yesText, String noText, String changeColor, Bundle... data){
        com.frank.plantilla.views.dialogs.DialogChoice mDialogChoice = new com.frank.plantilla.views.dialogs.DialogChoice();
        Bundle args = new Bundle();
        args.putString("yesText", yesText);
        args.putString("noText", noText);
        args.putString("text", text);
        args.putString("title", title);
        args.putString("dialogId", dialogId);
        args.putBundle("data", data != null && data.length > 0 ? data[0] : null);
        args.putString("changeColorButton", changeColor);
        mDialogChoice.setArguments(args);

        return mDialogChoice;
    }

    public void show(FragmentManager manager, String tag, boolean cancelable) {
        this.cancelable = cancelable;
        show(manager, tag);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.dialogFullScreen);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setCancelable(cancelable);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_choice, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        setCancelable(true);
        ((TextView) view.findViewById(R.id.tvDialogTitle)).setText(getArguments().getString("title"));

        CharSequence sequence = getArguments().getCharSequence("text");
        SpannableString spannableText;

        try {
            spannableText = (SpannableString) sequence;
        } catch (Exception e) {
            spannableText = null;
        }

        ((TextView) view.findViewById(R.id.tvDialog)).setText(spannableText != null ? spannableText : getArguments().getString("text"));

        TextView tvDialogYes = (TextView) view.findViewById(R.id.tvDialogYes);
        TextView tvDialogNo = (TextView) view.findViewById(R.id.tvDialogNo);

        if( !getArguments().getString("yesText", "").equals("")){
            tvDialogYes.setText(getArguments().getString("yesText", "Aceptar"));
        }

        if( !getArguments().getString("noText", "").equals("")){
            tvDialogNo.setText(getArguments().getString("noText", "Cancelar"));
        }

        if (getArguments().getString("changeColorButton","").equals("no")){
            tvDialogNo.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.main_green));
            tvDialogYes.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.main_green));
        }

        tvDialogYes.setOnClickListener(this);
        tvDialogNo.setOnClickListener(this);
        view.findViewById(R.id.ivDialogClose).setOnClickListener(this);
    }

    private Responses.OnResponse getResponse() {
        Responses.OnResponse mResponse = null;

        if (getParentFragment() instanceof Responses.OnResponse) {
            mResponse = (Responses.OnResponse) getParentFragment();
        } else if (getActivity() instanceof Responses.OnResponse) {
            mResponse = (Responses.OnResponse) getActivity();
        }

        return mResponse;
    }

    public void handleResponse(boolean hasAccepted) {
        Responses.OnResponse mResponse = getResponse();
        if (mResponse != null) {
            mResponse.onResponse(getArguments().getString("dialogId", ""), hasAccepted, getArguments().getBundle("data"));
        }
        dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDialogYes:
                handleResponse(true);
                break;

            case R.id.tvDialogNo:
                handleResponse(false);
                break;

            case R.id.ivDialogClose:
                dismiss();
                break;
        }
    }
}