package com.frank.plantilla.model.classes.blueprints;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.utils.MyProgressDialog;
import com.frank.plantilla.views.dialogs.DialogDisclaimer;
import com.frank.plantilla.views.dialogs.DialogInfo;

public class RestViewDialog extends DialogFragment implements ViewInteractor {

    private final static String DIALOG_CONNECTIVITY = "connectivity";

    public BaseRestPresenter presenter;

    public void onCreate(BaseRestPresenter presenter) {
        this.presenter = presenter;
    }

    public void showMsg(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }

    public void showMsg(String dialogId, String error) {
        try {
            DialogDisclaimer.newInstance(dialogId, getString(R.string.atention), error).
                    show(getChildFragmentManager(), DialogDisclaimer.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showLoader() {
        MyProgressDialog.show(getContext());
    }

    public void hideLoader() {
        MyProgressDialog.hide();
    }

    @Override
    public void displayNetworkError() {
        DialogInfo.newInstance(DIALOG_CONNECTIVITY, getString(R.string.network), getString(R.string.network_issue)).show(getFragmentManager(), "", false);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.rxUnSubscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.tryRest();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.saveState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.setState(savedInstanceState);
    }
}
