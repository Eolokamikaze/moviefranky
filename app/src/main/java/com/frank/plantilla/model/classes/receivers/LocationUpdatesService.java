package com.frank.plantilla.model.classes.receivers;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.frank.plantilla.BuildConfig;
import com.frank.plantilla.R;
import com.frank.plantilla.model.RxApplication;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.DateUtils;
import com.frank.plantilla.model.classes.utils.JSONUtils;
import com.frank.plantilla.model.classes.utils.NetworkUtils;
import com.frank.plantilla.model.classes.utils.NotificationsChannels;

import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.NetworkService;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.retrofit.observer.CacheObserver;
import com.frank.plantilla.retrofit.observer.ResponseObserver;

import com.facebook.network.connectionclass.ConnectionClassManager;
import com.facebook.network.connectionclass.ConnectionQuality;
import com.facebook.network.connectionclass.DeviceBandwidthSampler;
import com.frank.plantilla.views.activities.MainActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Frank on 4/17/17.
 * FROM: https://github.com/googlesamples/android-play-location/blob/master/LocationUpdatesForegroundService/app/src/main/java/com/google/android/gms/location/sample/locationupdatesforegroundservice/LocationUpdatesService.java
 */

public class LocationUpdatesService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, Responses.OnResponse, ConnectionClassManager.ConnectionClassStateChangeListener {

    //TODO catch no google play services installed. or google play services is out of date.
    //TODO catch google play services not connected yet issue.
    private Calendar lastPullTickets;
    private static int fallos = 0;

    private static final String PACKAGE_NAME = "com.google.android.gms.location.sample.locationupdatesforegroundservice";

    private static final String TAG = LocationUpdatesService.class.getSimpleName();

    static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME + ".started_from_notification";

    private Handler mTimeoutHandler;
    private Runnable mTimeoutRunnable;

    private Handler mTicketHandler;
    private Runnable mTicketRunnable;
    private Bundle ticket;
    private LatLng mLatLng;
    Bundle responseTickets;
    private ArrayList<String> ticketsInStack;
    private Calendar lastUpdateLocation;
    private Calendar failedRegisterLocation;
    private boolean inArrivingProcess = false;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static long UPDATE_INTERVAL_IN_MILLISECONDS;
    private static int UPDATE_INTERVAL_PARAMETER;

    /**
     * The desired interval for timeout.
     */
    private static long TIMEOUT_INTERVAL_IN_MILLISECONDS;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private static long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS;

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 12345678;

    private NotificationManager mNotificationManager;

    /**
     * The entry point to Google Play Services.
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;

    private Handler mServiceHandler;

    /**
     * The current location.
     */
    private Location mLocation;
    private boolean attemptedToStartService = false;
    private boolean attemptedToStopService = false;


    /**
     * Rest
     **/
    private NetworkService mService;
    private Observable observable;
    private Observer observer;
    private Subscription subscription;
    private ConnectionQuality cq;
    private DeviceBandwidthSampler sampler;

    public LocationUpdatesService() {

    }

    @Override
    public void onCreate() {
        SharedPreferences preferences = Utils.getSharedPreferences(getApplicationContext());
        UPDATE_INTERVAL_PARAMETER = Integer.parseInt(preferences.
                getString(Constants.UPDATE_LOCATION, "60"));
        UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_PARAMETER * 1000;
        TIMEOUT_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS * 2;
        FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 4;

        ConnectionClassManager ccManager = ConnectionClassManager.getInstance();
        ccManager.register(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        attemptedToStartService = true;
        createLocationRequest();
        sampler = DeviceBandwidthSampler.getInstance();
        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        lastUpdateLocation = Calendar.getInstance();
        mTimeoutHandler = new Handler();
        mTimeoutRunnable = new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "TIMEOUT");
                onLocationChanged(null);
            }
        };


        mTicketHandler = new Handler();
        mTicketRunnable = new Runnable() {
            @Override
            public void run() {
//                switch (TicketUtils.getIdUnApp(getApplicationContext())) {
//                    case Constants.ServiceGroup.GESTORES:
//                        getLogin();
//                        break;
//
//                    case Constants.ServiceGroup.ABOGADOS:
//                        getAgenda();
//                        break;
//
//                    case Constants.ServiceGroup.GRUAS:
//                        pullTickets();
//                        break;
//                }

            }
        };

        mService = ((RxApplication) getApplication()).getNetworkService();

        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        startForeground(NOTIFICATION_ID, getNotification());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");
        if (mGoogleApiClient.isConnected()) {
            requestLocationUpdates();
            Utils.hideNotification(getApplicationContext(), Constants.Notification.INTERNET);
        } else {
            if (Utils.isGpsOn(getApplicationContext()))
                Utils.showNotification(getApplicationContext(), Constants.Notification.INTERNET);

        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        removeTimeoutHandler();
        attemptedToStartService = false;
        mServiceHandler.removeCallbacksAndMessages(null);
        mGoogleApiClient.disconnect();
    }

    private void checkErrors() {
        if (fallos >= 4) {
            fallos = 0;
            Log.i(TAG, "Reiniciando");
            resendService(getApplicationContext());
        }
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (LocationUpdatesService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void resendService(Context context) {
        Intent srvLocation = new Intent(context, LocationUpdatesService.class);
        if (isMyServiceRunning()) {
            context.stopService(srvLocation);
        }
        context.startService(srvLocation);
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            Log.i(TAG, "Requesting location updates");
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, LocationUpdatesService.this);
                setTimeoutHandler();
                setTicketHandler();
            } catch (SecurityException unlikely) {
                Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
            }

            attemptedToStartService = false;
        } else {
            attemptedToStartService = true;
        }
    }

    public boolean isNetworkActive(final Context context) {
        Intent networkChange = new Intent(Constants.Receiver.CONNECTIVITY_CHANGE);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null;
    }

    private void setTicketHandler() {
        removeTicketHandler();
        mTicketHandler.postDelayed(mTicketRunnable, UPDATE_INTERVAL_IN_MILLISECONDS / 2);
    }

    private void removeTicketHandler() {
        mTicketHandler.removeCallbacks(mTicketRunnable);
    }

    private void setTimeoutHandler() {
        removeTimeoutHandler();
        mTimeoutHandler.postDelayed(mTimeoutRunnable, FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
    }

    private void removeTimeoutHandler() {
        mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            Log.i(TAG, "Removing location updates");
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, LocationUpdatesService.this);
                removeTimeoutHandler();
                stopSelf();
            } catch (SecurityException unlikely) {
                Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
            }
            attemptedToStopService = false;

            return;
        }
        attemptedToStopService = true;
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, LocationUpdatesService.class);


        CharSequence text = Utils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
//        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, SplashActivity.class), 0);
//        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alert);
        Notification notification = null;
        if (text.equals("No se pudo enviar tu ubicación")) {
//            notification = new NotificationCompat.Builder(this, NotificationsChannels.UBICATION_CHANNEL_ID)
//                    .addAction(R.drawable.btn_active_user, "Abrir Autolink", activityPendingIntent)
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
//                    .setContentText(text)
//                    .setContentTitle(Utils.getLocationTitle(this))
//                    .setOngoing(true)
//                    .setSound(defaultSoundUri)
//                    .setGroupSummary(true)
//                    .setSmallIcon(R.drawable.icon)
//                    .setTicker(text)
//                    .setWhen(System.currentTimeMillis()).build();
        } else {
//            notification = new NotificationCompat.Builder(this, NotificationsChannels.NO_SOUND_CHANNEL_ID)
//                    .addAction(R.drawable.btn_active_user, "Abrir Autolink", activityPendingIntent)
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
//                    .setContentText(text)
//                    .setContentTitle(Utils.getLocationTitle(this))
//                    .setOngoing(true)
//                    .setGroupSummary(true)
//                    .setSmallIcon(R.drawable.icon)
//                    .setTicker(text)
//                    .setWhen(System.currentTimeMillis()).build();
        }

        return notification;

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        try {
            //mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLocation = null;
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }

        if (attemptedToStartService) {
            requestLocationUpdates();
        }

        if (attemptedToStopService) {
            removeLocationUpdates();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        // In this example, we merely log the suspension.
        Log.e(TAG, "GoogleApiClient connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // In this example, we merely log the failure.
        Log.e(TAG, "GoogleApiClient connection failed.");
    }

    private boolean verifyProviders() {
        boolean isGpsActive = Utils.isGpsOn(this);
        boolean isNetworkActive = isNetworkActive(getBaseContext());

        Intent intent = new Intent(Constants.Receiver.CONNECTIVITY_CHANGE);
        intent.putExtra("isGpsActive", isGpsActive);
        intent.putExtra("isNetworkActive", isNetworkActive);

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        if (serviceIsRunningInForeground(this) && Utils.isUserActive(this) && !isGpsActive) {
            manageSession(false);
        }

        // Gps is off. Application can not continue correctly. Will not be able to obtain or send updated location.
        // Some devices will continue to obtain location even with GPS off. But location will not be updated.
        if (Utils.isUserActive(getApplicationContext())) {
            if (!isGpsActive || !isNetworkActive) {
                if (isGpsActive && !isNetworkActive) {
                    Utils.showNotification(getBaseContext(), Constants.Notification.INTERNET);
                } else if (!isGpsActive && isNetworkActive) {
                    Utils.showNotification(getBaseContext(), Constants.Notification.GPS);
                } else if (!isGpsActive && !isNetworkActive) {
                    Utils.showNotification(getBaseContext(), Constants.Notification.GPS);
                    Utils.showNotification(getBaseContext(), Constants.Notification.INTERNET);
                }
                return false;
            }
        }

        Utils.hideNotification(getBaseContext(), Constants.Notification.GPS);

        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        setTimeoutHandler();

        if (!verifyProviders() || location == null) {
//            Utils.showNotification(getBaseContext(), Constants.Notification.GPS);
            fallos++;
            checkErrors();
            return;
        }

        fallos = 0;
        checkErrors();

        mLocation = location;
        ((RxApplication) getApplication()).updateLocation(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()));

        // Notify anyone listening for broadcasts about the new location.
        Intent intent = new Intent(Constants.Receiver.LOCATION_RECEIVED);
        intent.putExtra(EXTRA_LOCATION, location);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        // Update notification content if running as a foreground service.
        mNotificationManager.notify(NOTIFICATION_ID, getNotification());

        sendLocation2();
    }

    private void sendLocation() {
        Date mDate = new Date();
        mDate.setTime(mLocation.getTime());
        Log.e(TAG, "New location: " + mLocation + " date: " + DateFormat.format("yyyy-MM-dd hh:mm:ss a", mDate));

//        post(Constants.Service.UPDATE_LOCATION, NetworkUtils.urlEncode(getParams()), this);
    }

    private void sendLocation2() {
        Calendar calendar = Calendar.getInstance();
        if (lastUpdateLocation.getTimeInMillis() <= calendar.getTimeInMillis()) {
            sampler.startSampling();
            Log.i(Constants.UPDATE_LOCATION, DateUtils.getCustomDate(lastUpdateLocation, "HH:mm:ss", false));
            lastUpdateLocation.add(Calendar.SECOND, UPDATE_INTERVAL_PARAMETER);
            Date mDate = new Date();
            mDate.setTime(mLocation.getTime());
//            Log.e(TAG, "New location: " + mLocation + " date: " + DateFormat.format("yyyy-MM-dd hh:mm:ss a", mDate));
//            get(Constants.Service.UPDATE_LOCATION_2, NetworkUtils.urlEncode(getParamswithUUid()), this, Calendar.getInstance());
        }
    }

    private void pullTickets() {
        HashMap<String, String> params = new HashMap<>();
        if (lastPullTickets == null) {
            lastPullTickets = Calendar.getInstance();
            sampler.startSampling();
            if (mLocation != null) {
                params.put("latitude", Utils.handleCoordinate(mLocation.getLatitude()));
                params.put("longitude", Utils.handleCoordinate(mLocation.getLongitude()));
                params.put("credentials", NetworkUtils.getCredentials(getBaseContext()));

                String uuid = Utils.getSharedPreferences(this).getString("uid", "");
                JSONObject input = new JSONObject();
                JSONUtils.put(input, "latitude", Utils.handleCoordinate(mLocation.getLatitude()));
                JSONUtils.put(input, "longitude", Utils.handleCoordinate(mLocation.getLongitude()));
                JSONUtils.put(input, "uuid", uuid);


//            post(Constants.Service.PULL_TICKETS, NetworkUtils.urlEncode(params), this);
//                postBen(Constants.Service.PULL_TICKETS, input, this);
            }
        } else {
            if (DateUtils.compareTodayDayWithSeconds(lastPullTickets, 5)) {
                lastPullTickets = Calendar.getInstance();
                sampler.startSampling();
                if (mLocation != null) {
                    params.put("latitude", Utils.handleCoordinate(mLocation.getLatitude()));
                    params.put("longitude", Utils.handleCoordinate(mLocation.getLongitude()));
                    params.put("credentials", NetworkUtils.getCredentials(getBaseContext()));

                    String uuid = Utils.getSharedPreferences(this).getString("uid", "");
                    JSONObject input = new JSONObject();
                    JSONUtils.put(input, "latitude", Utils.handleCoordinate(mLocation.getLatitude()));
                    JSONUtils.put(input, "longitude", Utils.handleCoordinate(mLocation.getLongitude()));
                    JSONUtils.put(input, "uuid", uuid);


//            post(Constants.Service.PULL_TICKETS, NetworkUtils.urlEncode(params), this);
//                    postBen(Constants.Service.PULL_TICKETS, input, this);
                }
            }

        }
    }

    private HashMap<String, String> getParams() {
        DecimalFormat df = new DecimalFormat("####0.00######");

        HashMap<String, String> params = new HashMap<>();
        params.put("latitude", df.format(mLocation.getLatitude()));
        params.put("longitude", df.format(mLocation.getLongitude()));
        params.put("credentials", NetworkUtils.getCredentials(getBaseContext()));

        return params;
    }

    private HashMap<String, String> getParamswithUUid() {
        DecimalFormat df = new DecimalFormat("####0.00######");
        String uuid = Utils.getSharedPreferences(getApplicationContext()).getString("uid", "");
        HashMap<String, String> params = new HashMap<>();
        params.put("latitud", df.format(mLocation.getLatitude()));
        params.put("longitud", df.format(mLocation.getLongitude()));
        params.put("UUID", uuid);
        params.put("reactivar", "" + Utils.isUserActive(getApplicationContext()));
        params.put("credentials", uuid);

        return params;
    }

    public void deactivateProvider() {

        HashMap<String, String> params = new HashMap<>();
        params.put("note", "");
        params.put("credentials", NetworkUtils.getCredentials(this));

//        post(Constants.Service.SESSION_END, NetworkUtils.urlEncode(params), this);
    }

    public void manageSession(boolean isActive) {
//        SharedPreferences preferences = Utils.getSharedPreferences(getApplicationContext());
//        JSONObject data = new JSONObject();
//        JSONUtils.put(data, Constants.User.USER_EMAIL, preferences.getString("mail", ""));
//        JSONUtils.put(data, Constants.User.UUID, preferences.getString("code", ""));
//        JSONUtils.put(data, Constants.User.USER_PASSWORD, preferences.getString("pass", ""));
//        JSONUtils.put(data, Constants.User.DATE_OF_ACTION, DateUtils.getChatTimestamp());
//        JSONUtils.put(data, Constants.User.IS_ACTIVE, isActive);
//
//        JSONObject input = new JSONObject();
//        JSONUtils.put(input, Constants.DATA, data);
//        JSONUtils.put(input, "credentials", NetworkUtils.getCredentials(getApplicationContext()));
//        post(Constants.Service.MANAGE_SESSION, input, this, false);
    }

    public void acceptService(Bundle ticket) {
        this.ticket = ticket;
        mLatLng = ((RxApplication) getApplication()).getLocation();

        if (mLatLng != null) {
            acceptService2(ticket);
        }
    }

    public void acceptService2(Bundle ticket) {
        this.ticket = ticket;
        if (mLatLng != null) {

            JSONObject answerRequest = new JSONObject();
            JSONUtils.put(answerRequest, "ticket_id", ticket.getString("ticket_id", ""));
            JSONUtils.put(answerRequest, "reject_type_id", 0);
            JSONUtils.put(answerRequest, "comments", "");
            JSONUtils.put(answerRequest, "latitude", mLatLng.latitude);
            JSONUtils.put(answerRequest, "longitude", mLatLng.longitude);

            HashMap<String, String> params = new HashMap<>();
            params.put("credentials", NetworkUtils.getCredentials(getApplicationContext()));
            params.put("data", answerRequest.toString());

//            ticket.putString("ticket_status", String.valueOf(Constants.TicketStatus.ACCEPTED));
//            getAuth(Constants.Service.TICKET_ANSWER_REQUEST2, NetworkUtils.urlEncode(params), this, false, ticket);
        }

    }

    private void makeArrival(String inProc) {
        if (inProc != null && !inProc.equals("")) {
            String[] tickets = inProc.split(",");
            if (tickets != null && tickets.length > 0) {
                getLogin();
            }
        }
    }


    public void getLogin() {
        if (mLocation != null) {
            Bundle login = Utils.getLogin(Utils.getSharedPreferences(getBaseContext()));
            JSONObject params = new JSONObject();
            try {
                params.put("id", login.getString("mail", ""));
                params.put("code", login.getString("code", ""));
                params.put("pass", login.getString("pass", ""));
                params.put("token", login.getString("token", ""));
                params.put(Constants.VERSION_APP, BuildConfig.VERSION_NAME);
//                postBen(Constants.Service.LOGIN, params, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    public void getAgenda() {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        Bundle login = Utils.getLogin(Utils.getSharedPreferences(getBaseContext()));
        JSONObject params = new JSONObject();
        try {
            params.put("id", login.getString("mail", ""));
            params.put("code", login.getString("code", ""));
            params.put("pass", login.getString("pass", ""));
            params.put("anio", year + "");
            if (month > 9) {
                params.put("mes", month);
            } else {
                params.put("mes", "0" + month);
            }

            params.put(Constants.VERSION_APP, BuildConfig.VERSION_NAME);
//            postBen(Constants.Service.OBTEN_AGENDA, params, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void marcarAccion(String message, Bundle ticket) {
        if (!inArrivingProcess) {
            HashMap<String, String> params = new HashMap<>();
            params.put("credentials", NetworkUtils.getCredentials(getBaseContext()));
            String ticketId = ticket.getString("ticket_id");
            params.put("data", getTicketData(message, ticketId));
            inArrivingProcess = true;
//        post(Constants.Service.DO_CHAT, NetworkUtils.urlEncode(params), this, ticketId);
//            getBen(Constants.Service.MARCAR_ACCION, params, this, ticket);
        }
    }

//    public void getChecklistData(Bundle ticket) {
//        String serviceNumber = TicketUtils.getServiceNumber(ticket);
//        HashMap<String, String> params = new HashMap<>();
//        params.put("numservicio", serviceNumber);
//        getBen(Constants.Service.OBTEN_CHECKLIST_DATA, params, this, ticket);
//    }


    public void insertaMensaje(Bundle ticket, String msg, String content, String type, String msg_id) {
        JSONObject data = new JSONObject();
        String uuid = Utils.getSharedPreferences(getApplicationContext()).getString("uid", "");
        try {
            JSONUtils.put(data, "ticket_id", ticket.getString("ticket_id", ""));
            JSONUtils.put(data, "prov_id", uuid);
            JSONUtils.put(data, "msg", msg);
            JSONUtils.put(data, "isfinish", false);
            if (content != null)
                JSONUtils.put(data, "file", content);
            JSONUtils.put(data, "type_file", type);
            JSONUtils.put(data, "is_customer", false);
            JSONUtils.put(data, "fecha", DateUtils.getChatTimestamp());
            JSONUtils.put(data, "user_name", "");
            if (msg_id != null)
                JSONUtils.put(data, "msg_id", msg_id);


//            post(Constants.Service.INSERTA_MENSAJE, data, this, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getTicketData(String msg, String ticketId) {

        if (mLocation != null) {
            LatLng mLatlng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());

            JSONObject json = new JSONObject();
            JSONUtils.put(json, "id", 0);
            JSONUtils.put(json, "ticket_id", ticketId);
            JSONUtils.put(json, "text", msg);
            JSONUtils.put(json, "date_time", DateUtils.getChatTimestamp());
            JSONUtils.put(json, "finish", false);
            JSONUtils.put(json, "is_customer", false);
            JSONUtils.put(json, "latitude", mLatlng.latitude);
            JSONUtils.put(json, "longitude", mLatlng.longitude);
            JSONUtils.put(json, "is_geofence", true);

            return json.toString();
        }

        return "";
    }

    private int getTickets(ArrayList<Bundle> list) {
        int number = 0;
        if (ticketsInStack == null) {
            ticketsInStack = new ArrayList<>();
        }
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (ticketsInStack.size() == 0) {
//                    ticketsInStack.add(TicketUtils.getServiceNumber(list.get(i)));
                    if (i == list.size() - 1) {
                        number = list.size();
                    }
                } else {
                    String serviceNumber = "";
                    for (int j = 0; j < ticketsInStack.size(); j++) {
//                        if (TicketUtils.getServiceNumber(list.get(i)).equals(ticketsInStack.get(j))) {
//                            serviceNumber = "";
//                            break;
//                        } else {
////                            serviceNumber = TicketUtils.getServiceNumber(list.get(i));
//                        }
                    }
//                    if (Utils.stringNotNull(serviceNumber)) {
//                        ticketsInStack.add(serviceNumber);
//                        number += 1;
//                    }
                }
            }
        }
        return number;
    }

    private Notification getNotificationUpdateLocationFailed() {
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alert);
        String text = "No se pudo enviar tu ubicación";
        Notification notification = new NotificationCompat.Builder(this, NotificationsChannels.UBICATION_CHANNEL_ID)
//                .addAction(R.drawable.btn_active_user, "Abrir Autolink", activityPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setContentText(text)
                .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setSound(defaultSoundUri)
                .setGroupSummary(true)
//                .setSmallIcon(R.drawable.icon)
                .setTicker(text)
                .setWhen(System.currentTimeMillis()).build();

        return notification;
    }

    private Notification getNotificationUpdateLocationSuccess() {
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alert);
        String text = "Actualizando ubicación";

        Notification notification = new NotificationCompat.Builder(this, NotificationsChannels.NO_SOUND_CHANNEL_ID)
//                .addAction(R.drawable.btn_active_user, "Abrir Autolink", activityPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setContentText(text)
                .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setGroupSummary(true)
//                .setSmallIcon(R.drawable.icon)
                .setTicker(text)
                .setWhen(System.currentTimeMillis()).build();

        return notification;
    }

    @Override
    public void onResponse(String requestKey, Object response, Object data) {

        switch (requestKey) {

//            case Constants.Service.PULL_TICKETS:
//                setTicketHandler();
//
//                Intent intent = new Intent(Constants.Receiver.TICKETS_RECEIVED);
//
//                ArrayList<Bundle> tickets;
//                String message;
//                String inProc;
//                if (response != null && response instanceof Bundle) {
//                    Utils.hideNotification(getBaseContext(), Constants.Notification.INTERNET);
//
//                    Bundle tmp = (Bundle) response;
//
//                    tickets = tmp.getParcelableArrayList("tickets");
//                    tickets = tickets != null ? tickets : new ArrayList<Bundle>();
//
//                    inProc = tmp.getString("inproc_tickets_csv", "");
//                    message = tmp.getString("message", "");
//                    makeArrival(inProc);
//                    if (message.equals("INACTIVE")) {
//                        Utils.deactivateUser(this);
//
//                        String title = getString(R.string.notification_service_title);
//                        String text = "Tu estatus ha cambiado a inactivo. Reactívate en la aplicación.";
//
//                        Intent in = new Intent(this, SplashActivity.class);
//                        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0, in, 0);
//
//                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
//                                NotificationsChannels.INTERNET_CHANNEL_ID)
//                                .setWhen(Calendar.getInstance().getTimeInMillis())
//                                .setContentIntent(activityPendingIntent)
//                                .setSmallIcon(R.drawable.icon_white)
//                                .setContentTitle(title)
//                                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
//                                .setContentText(text)
//                                .setAutoCancel(false)
//                                .setOngoing(false)
//                                .setDefaults(Notification.DEFAULT_LIGHTS);
//
//                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                        mNotificationManager.notify(Constants.Notification.DESACTIVADO, mBuilder.build());
//                    }
//                } else {
//                    if (response != null && !response.equals("Sin Info en PullTickets."))
//                        if (Utils.isUserActive(getApplicationContext()))
//                            Utils.showNotification(getBaseContext(), Constants.Notification.INTERNET);
//                    return;
//                }
//                boolean hasTicketGestoria = false;
//                boolean hasTicketVial = false;
//                if (tickets.size() > 0) {
//                    if (ticketsInStack == null) {
//                        ticketsInStack = new ArrayList<>();
//                    }
//                    int number = getTickets(tickets);
//                    if (number > 0) {
////                        TicketUtils.saveNumServices(getApplicationContext(), tickets.size());
//                        for (int i = 0; i < tickets.size(); i++) {
//
//                            if (tickets.get(i).getString("serv_group_id", "").equals(Constants.ServiceGroup.GESTORES)) {
//                                hasTicketGestoria = true;
//                                acceptService(tickets.get(i));
//                            }
//                        }
//                    }
//                    if (!hasTicketVial) {
//                        for (int i = 0; i < tickets.size(); i++) {
//                            if (TicketUtils.getServiceGroup(tickets.get(i)).equals(Constants.ServiceGroup.GRUAS)) {
//                                hasTicketVial = true;
//                            }
//                        }
//                    }
//
//                    if (hasTicketGestoria || hasTicketVial) {
//                        if (tickets.size() > 0) {
//                            if (!hasTicketGestoria) {
//                                Utils.showNotification(getBaseContext(), Constants.Notification.SERVICE);
//                                ((RxApplication) getApplication()).playRingtone();
//                            }
//                            if (number > 0) {
//                                TicketUtils.saveNumServices(getApplicationContext(), tickets.size());
//                                for (int i = 0; i < tickets.size(); i++) {
//                                    TicketUtils.saveNotificationHistory(getApplicationContext(),
//                                            TicketUtils.getServiceNumber(tickets.get(i)),
//                                            getApplicationContext().getString(R.string.notification_service_text),
//                                            "manual_assignment");
//                                }
//                            }
//                        }
//                        this.responseTickets = (Bundle) response;
//                    }
//
//                }
//
//                break;
//
//
//            case Constants.Service.UPDATE_LOCATION:
//                if (response != null && response instanceof Bundle) {
//
//                    Utils.hideNotification(getBaseContext(), Constants.Notification.INTERNET);
//                    sendLocation2();
//                } else {
//                    Utils.showNotification(getBaseContext(), Constants.Notification.INTERNET);
//                }
//                Log.e(TAG, "RESULT: " + response);
//                break;
//
//            case Constants.Service.UPDATE_LOCATION_2:
//                Bundle info = new Bundle();
//                Calendar dateInput = (Calendar) data;
//                sampler.stopSampling();
//                if (response != null && response instanceof Bundle) {
//                    Utils.hideNotification(getBaseContext(), Constants.Notification.INTERNET);
//                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                    mNotificationManager.notify(NOTIFICATION_ID, getNotificationUpdateLocationSuccess());
//
//                } else {
//                    if (Utils.isUserActive(getApplicationContext())) {
//                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                        mNotificationManager.notify(NOTIFICATION_ID, getNotificationUpdateLocationFailed());
//
//                    }
//                }
//                Log.e(TAG, "RESULT: " + response);
//                break;
//
//            case Constants.Service.MANAGE_SESSION:
//                if (response != null && response instanceof Bundle) {
//                    Bundle output = (Bundle) response;
//                    if (output.getBoolean("success", false)) {
//                        Utils.deactivateUser(this);
//                        Utils.hideNotification(getApplicationContext(), Constants.Notification.LOW_BAND_WIDTH);
//                        Utils.hideNotification(getApplicationContext(), Constants.Notification.INTERNET);
//                        String title = getString(R.string.notification_service_title);
//                        String text = "Tu estatus ha cambiado a inactivo, verifica que tu GPS esté encendido.";
//
//                        Intent in = new Intent(this, SplashActivity.class);
//                        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0, in, 0);
//
//                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
//                                NotificationsChannels.INTERNET_CHANNEL_ID)
//                                .setWhen(Calendar.getInstance().getTimeInMillis())
//                                .setContentIntent(activityPendingIntent)
//                                .setSmallIcon(R.drawable.icon_white)
//                                .setContentTitle(title)
//                                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
//                                .setContentText(text)
//                                .setAutoCancel(false)
//                                .setOngoing(false)
//                                .setDefaults(Notification.DEFAULT_LIGHTS);
//
//                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                        mNotificationManager.notify(Constants.Notification.DESACTIVADO, mBuilder.build());
//                    }
//                } else {
//                    if (Utils.isUserActive(getApplicationContext())) {
//                        Utils.showNotification(getBaseContext(), Constants.Notification.INTERNET);
//                    }
//                }
//                break;
//
//            case Constants.Service.TICKET_ANSWER_REQUEST:
//
//                if (response instanceof Bundle) {
//                    Bundle resp = (Bundle) response;
//                    acceptService2(ticket);
//                }
//                break;
//
//            case Constants.Service.TICKET_ANSWER_REQUEST2:
//                if (response instanceof Bundle) {
//                    Bundle resp = (Bundle) response;
//                    if (resp.getBoolean("success", false)) {
//                        if (responseTickets != null) {
//                            tickets = responseTickets.getParcelableArrayList("tickets");
//                            tickets = tickets != null ? tickets : new ArrayList<Bundle>();
//
//                            inProc = responseTickets.getString("inproc_tickets_csv", "");
//                            message = responseTickets.getString("message", "");
//
//                            Intent in = new Intent(Constants.Receiver.TICKETS_RECEIVED);
//                            in.putParcelableArrayListExtra("tickets", tickets);
//                            in.putExtra("inProcess", inProc);
//                            in.putExtra("message", message);
//
//                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);
//                        }
//                    }
//                }
//                break;
//            case Constants.Service.MARCAR_ACCION:
//                inArrivingProcess = false;
//                if (response instanceof Bundle) {
//                    Bundle ticket = (Bundle) data;
//                    Date date = DateUtils.getDate(ticket.getString("creation_date", ""),
//                            "yyyy-MM-dd'T'HH:mm:ss");
//                    String msg = getString(R.string.arriving_message) + " " + TicketUtils.getServiceNumber(ticket) + " en " +
//                            DateUtils.getMinDifference(date) + " minutos";
//                    Utils.showNotificationMsgLng(getBaseContext(), msg, Constants.Notification.ARRIBO);
//                    Intent messageReceived = new Intent(Constants.Receiver.ARRIVING_RECEIVED);
//                    messageReceived.putExtra("message", msg);
//                    messageReceived.putExtra(Constants.Ticket.TICKET, ticket);
//                    LocalBroadcastManager.getInstance(this).sendBroadcast(messageReceived);
//                }
//
//
//                break;
//
//            case Constants.Service.LOGIN:
//
//                if (response instanceof Bundle) {
//
//                    Bundle resp = (Bundle) response;
//                    Bundle provider = resp.getBundle("provider");
//                    ArrayList<Bundle> mTickets = provider != null ? provider.<Bundle>getParcelableArrayList("tickets") : new ArrayList<Bundle>();
//
//                    Location oLocation = new Location("B");
//                    double distance, distanceTreshold = Double.valueOf(Utils.getSharedPreferences(getBaseContext()).getString("checkin_distance_m", "0"));
//
//                    if (mTickets != null && mTickets.size() > 0 && mLocation != null) {
//                        for (Bundle ticket : mTickets) {
//                            distance = 0;
//                            oLocation.setLatitude(Double.valueOf(ticket.getString("latitude", "0")));
//                            oLocation.setLongitude(Double.valueOf(ticket.getString("longitude", "0")));
//
//                            distance = mLocation.distanceTo(oLocation);
////                            if (ticket.getString("serv_group_id", "").equals(Constants.ServiceGroup.ABOGADOS)) {
//                            if (TicketUtils.checkStatus(ticket).equals(Constants.ChatActions.NO_STATUS) && distance <= distanceTreshold) {
//                                String fechaAtencion = TicketUtils.getFechaAtencion(ticket);
//                                if (Utils.stringNotNull(fechaAtencion) &&
//                                        DateUtils.compareToFuture(getApplicationContext(), fechaAtencion, "dd/MM/yyyy HH:mm:ss", 2)) {
//                                    marcarAccion(Constants.ChatActions.ARRIVED_MSG, ticket);
//                                }
////                            }
//                            }
//                        }
//                    }
//                }
//
//                break;
//
//            case Constants.Service.OBTEN_AGENDA:
//                if (response instanceof Bundle) {
//                    Bundle output = (Bundle) response;
//                    if (NetworkUtils.isSuccess(output)) {
//                        Location oLocation = new Location("B");
//                        double distance, distanceTreshold = Double.valueOf(Utils.getSharedPreferences(getBaseContext()).getString("checkin_distance_m", "0"));
//                        ArrayList<Bundle> mTickets = output.getParcelableArrayList("ResultObject");
//                        if (mTickets != null && mTickets.size() > 0 && mLocation != null) {
//                            for (Bundle ticket : mTickets) {
//                                distance = 0;
//                                oLocation.setLatitude(Double.valueOf(ticket.getString("latitude", "0")));
//                                oLocation.setLongitude(Double.valueOf(ticket.getString("longitude", "0")));
//
//                                distance = mLocation.distanceTo(oLocation);
//                                if (TicketUtils.checkStatus(ticket).equals(Constants.ChatActions.NO_STATUS) && distance <= distanceTreshold) {
//                                    String fechaAtencion = TicketUtils.getFechaAtencion(ticket);
//                                    if (Utils.stringNotNull(fechaAtencion) &&
//                                            DateUtils.compareToFuture(getApplicationContext(), fechaAtencion, "dd/MM/yyyy HH:mm:ss", 2)) {
////                                        getChecklistData(ticket);
//                                        marcarAccion(Constants.ChatActions.ARRIVED_MSG, ticket);
//                                    }
////                            }
//                                }
//                            }
//                        }
//                    }
//                }
//                break;
//
//
//            case Constants.Service.OBTEN_CHECKLIST_DATA:
//                if (response instanceof Bundle) {
//                    Bundle output = (Bundle) response;
//                    Bundle ticket = (Bundle) data;
//                    ticket.putString(Constants.Ticket.TICKET_ID,
//                            output.getString(Constants.Ticket.TICKET_ID));
//                    marcarAccion(Constants.ChatActions.ARRIVED_MSG, ticket);
//                } else if (response instanceof String) {
//
//                }
//                break;
        }

    }


    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * POST
     **/
    public void post(String serviceKey, String params, Responses.OnResponse onResponse, Object...
            data) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(getBaseContext()).getString(serviceKey, "");

            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl + " : " + params);
            }

            observable = mService.getPreparedObservable(mService.getPostAPI().post(serviceUrl, RequestBody.create(MediaType.parse("text/plain"), params)), "", false);
            observer = new ResponseObserver(serviceKey, getBaseContext(), onResponse, data);
            subscription = observable.subscribe(observer);

        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    public void post(String serviceKey, JSONObject params, Responses.OnResponse
            onResponse, Object... data) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(getBaseContext()).getString(serviceKey, "");

            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl + " : " + params);
            }

            observable = mService.getPreparedObservable(mService.getPostAPI().post(serviceUrl, params), "", false);
            observer = new ResponseObserver(serviceKey, getBaseContext(), onResponse, data);
            subscription = observable.subscribe(observer);

        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    public void postBen(String serviceKey, JSONObject params, Responses.OnResponse onResponse) {
        String serviceUrl = Utils.getSharedPreferences(getBaseContext()).getString(serviceKey, "");
        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl + " : " + params);
            Utils.saveInDebugFile(getApplicationContext(), serviceUrl, params.toString(), true);
        }

        observable = mService.getPreparedObservable(mService.getPostAuthAPI().post(serviceUrl, params), "", false);
        observer = new ResponseObserver(serviceKey, getBaseContext(), onResponse);
        subscription = observable.subscribe(observer);
    }

    public void get(String serviceKey, String params, Responses.OnResponse onResponse) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(getBaseContext()).getString(serviceKey, "");
            serviceUrl = serviceUrl + "?" + params.substring(1);
            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl);
            }

            observable = mService.getPreparedObservable(mService.getPostAuthAPI().get(serviceUrl), "", false);
            observer = new ResponseObserver(serviceKey, getBaseContext(), onResponse);
            subscription = observable.subscribe(observer);

        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    public void get(String serviceKey, String params, Responses.OnResponse onResponse, Object data) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(getBaseContext()).getString(serviceKey, "");
            serviceUrl = serviceUrl + "?" + params.substring(1);
            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl);
            }

            observable = mService.getPreparedObservable(mService.getPostAuthAPI().get(serviceUrl), "", false);
            observer = new ResponseObserver(serviceKey, getBaseContext(), onResponse, data);
            subscription = observable.subscribe(observer);

        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    public void getBen(String serviceKey, HashMap<String, String> params, Observer...
            mObserver) {

        observable = mService.getBenApi()
                .get(Utils.getSharedPreferences(getApplicationContext()).getString(serviceKey, serviceKey), params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (mObserver == null || mObserver.length == 0) {
            observer = new ResponseObserver(serviceKey, getApplicationContext(), new Responses.OnResponse() {
                @Override
                public void onResponse(String requestKey, Object response, Object data) {
                    switch (requestKey) {

                    }
                }
            });
        } else {
            observer = mObserver[0];
        }

        subscription = observable.subscribe(observer);
    }

    public void getBen(String serviceKey, HashMap<String, String> params,
                       Responses.OnResponse onResponse, Object data, Observer... mObserver) {

        String serviceUrl = Utils.getSharedPreferences(getApplicationContext()).getString(serviceKey, serviceKey);
        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl);
            Utils.saveInDebugFile(getApplicationContext(), serviceUrl, params, false);
        }

        observable = mService.getBenApi()
                .get(Utils.getSharedPreferences(getApplicationContext()).getString(serviceKey, serviceKey), params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());


        if (mObserver == null || mObserver.length == 0) {
            observer = new ResponseObserver(serviceKey, getApplicationContext(), onResponse, data);
        } else {
            observer = mObserver[0];
        }

        subscription = observable.subscribe(observer);
    }

    public void getAuth(String serviceKey, String params, Responses.OnResponse onResponse,
                        boolean useCache, Object... data) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(getApplicationContext()).getString(serviceKey, "");
            serviceUrl = serviceUrl + "?" + params.substring(1);
            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl);
                Utils.saveInDebugFile(getApplicationContext(), serviceUrl, "", false);
            }
            observable = mService.getPreparedObservable(mService.getPostAuthAPI().get(serviceUrl), "", false);
            observer = useCache ? new CacheObserver(serviceKey, getApplicationContext(), onResponse) :
                    new ResponseObserver(serviceKey, getApplicationContext(), onResponse, data);
            subscription = observable.subscribe(observer);


        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    @Override
    public void onBandwidthStateChange(ConnectionQuality bandwidthState) {
        Intent intent = new Intent(Constants.Receiver.LOW_CONNECTION);
        switch (bandwidthState) {
            case UNKNOWN:
                Utils.hideNotification(getApplicationContext(), Constants.Notification.LOW_BAND_WIDTH);
                intent.putExtra("LowConnection", false);
                break;

            case POOR:
                if (Utils.isUserActive(getApplicationContext())) {
                    Utils.showNotification(getApplicationContext(), Constants.Notification.LOW_BAND_WIDTH);
                    intent.putExtra("LowConnection", true);
                } else {
                    Utils.hideNotification(getApplicationContext(), Constants.Notification.LOW_BAND_WIDTH);
                    intent.putExtra("LowConnection", false);
                }
                break;

            default:
                Utils.hideNotification(getApplicationContext(), Constants.Notification.LOW_BAND_WIDTH);
                intent.putExtra("LowConnection", false);
                break;
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }
}

