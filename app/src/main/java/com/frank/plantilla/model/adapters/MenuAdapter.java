package com.frank.plantilla.model.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.retrofit.Responses;


import java.util.ArrayList;
import java.util.List;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.Holder> {

    int[] colors;
    private Responses.OnResponse mOnResponse;
    private List<Bundle> items;
    private int selected = -1;
    private int sel;
    private int reg;
    private int padd;
    private Context context;
    private ArrayList iconsMenu;

    public MenuAdapter(List<Bundle> items, final Context context, final Responses.OnResponse mOnResponse) {
        this.items = items;
        this.mOnResponse = mOnResponse;
        this.context = context;

        sel = ContextCompat.getColor(context, R.color.main_red);
        reg = Color.TRANSPARENT;
        padd = (int) context.getResources().getDimension(R.dimen.menu_padding);

        iconsMenu = new ArrayList();
        for (Bundle b : items) {
            iconsMenu.add(b.getString("image"));
        }
    }

    @Override
    public MenuAdapter.Holder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_menu, parent, false);
        return new Holder(v, mOnResponse);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {

        Bundle item = items.get(position);
        holder.tvMenu.setText(item.getString(Constants.Menu.TITLE));
        holder.tvMenu.setTextColor(ContextCompat.getColor(context, R.color.white));
        holder.llMenu.setBackgroundColor(selected == position ? sel : reg);
        holder.llMenu.setPadding(padd, padd, padd, padd);

    }

    @Override
    public int getItemCount() {
        if (items != null && items.size() > 0) {
            return items.size();
        } else {
            return 0;
        }
    }

    public Bundle setSelected(int position) {
        this.selected = position;
        notifyDataSetChanged();

        if (position > -1 && items != null && items.size() > 0) {
            return items.get(position);
        }

        return null;
    }

    public int getSelected() {
        return selected;
    }

    public int getPosition(String type) {
        selected = -1;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getString("type", "").equals(type))
                return i;
        }
        return -1;
    }

    public static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivMenu;
        TextView tvMenu;
        LinearLayout llMenu;


        Responses.OnResponse mOnResponse;

        public Holder(View itemView, Responses.OnResponse mOnResponse) {
            super(itemView);
            this.mOnResponse = mOnResponse;

            tvMenu = (TextView) itemView.findViewById(R.id.tvMenu);
            llMenu = (LinearLayout) itemView.findViewById(R.id.llMenu);
            ivMenu = (ImageView) itemView.findViewById(R.id.ivMenu);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                mOnResponse.onResponse("v.getId()", getAdapterPosition(), null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
