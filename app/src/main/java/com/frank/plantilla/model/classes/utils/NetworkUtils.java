package com.frank.plantilla.model.classes.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import java.util.HashMap;

import static com.frank.plantilla.model.classes.utils.Utils.getSharedPreferences;

/**
 * Created by Frank on 3/30/17.
 */

public class NetworkUtils {

    public static String urlEncode(HashMap<String, String> request) {
        String post = "";
        for (String pair : request.keySet()) {
            post = post + "&" + pair + "=" + request.get(pair);
        }
        return post;
    }

    public static void saveValue(final Context context, String value) {
        SharedPreferences mSharedPreferences = getSharedPreferences(context);
        mSharedPreferences.edit().putString("value", value).apply();
    }

    public static String getValue(final Context context) {
        SharedPreferences mSharedPreferences = getSharedPreferences(context);
        return mSharedPreferences.getString("value", "");
    }

    public static boolean isNetworkAvailable(final Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getCredentials(final Context context) {
        SharedPreferences mSharedPreferences = Utils.getSharedPreferences(context);
//        String value = getValue(context);
//        String pass = mSharedPreferences.getString("pass", "");
//        String encript = CryptLib.decrypt(pass, value);
        String uuid = mSharedPreferences.getString("uid", "");
//        String credentials = String.format("%s|%s", uuid, pass);

        return uuid;
    }

    public static String getCredentials(String credential, final Context context) {

        String encrypted;
        try {
            encrypted = CryptLib.encrypt(credential, getValue(context));
        } catch (Exception e) {
            encrypted = "";
        }

        return encrypted;
    }

    public static boolean isSuccess(Bundle resp) {
        Bundle response = resp.getBundle("response") != null ? resp.getBundle("response") : resp.getBundle("Response");
        if (resp.getBoolean("success", false) || (response != null && (response.getString("ReturnCode", "").equals("200") || response.getString("returnCode", "").equals("200")))) {
            return true;
        }
        if (resp.getString("status", "").equals("OK")) {
            return true;
        }
        if (resp.getString("statusCode").equals("200")){
            return true;
        }
        return false;
    }

    public static String getMessage(Bundle resp) {
        Bundle response = resp.getBundle("response");
        if (response != null) {
            return response.getString("ReturnMessage", "");
        } else {
            if (resp.containsKey("Response")) {
                response = resp.getBundle("Response");
                if (response != null) {
                    return response.getString("ReturnMessage", "");
                }
            } else if (resp.containsKey("Message")) {
                return resp.getString("Message", "");
            } else if (resp.containsKey("error_message")) {
                return resp.getString("error_message", "");
            } else if (resp.containsKey("status")) {
                if (resp.getString("status").contains("ZERO_RESULTS")) {
                    return "No se pudo encontrar una dirección asociada.";
                }
            } else if (resp.containsKey("message")) {
                if (Utils.stringNotNull(resp.getString("description"))) {
                    return resp.getString("message") + " " + resp.getString("description");
                } else {
                    return resp.getString("message", "");
                }
            }

        }
        return resp.getString("message", "");
    }

}
