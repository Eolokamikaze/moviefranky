package com.frank.plantilla.model.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.model.classes.constants.Constants;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;


public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.Holder> {

    int[] colors;
    private Responses.OnResponse mOnResponse;
    private List<Bundle> items;
    private int selected = -1;
    private int sel;
    private int reg;
    private int padd;
    private Context context;
    private ArrayList iconsMenu;

    public MovieListAdapter(List<Bundle> items, final Context context, final Responses.OnResponse mOnResponse) {
        this.items = items;
        this.mOnResponse = mOnResponse;
        this.context = context;
    }

    @Override
    public MovieListAdapter.Holder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_movie, parent, false);
        return new Holder(v, mOnResponse);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {

        Bundle item = items.get(position);
        holder.tvName.setText(item.getString(Constants.Movie.TITLE));
        holder.tvDescription.setText(item.getString(Constants.Movie.OVERVIEW));
        holder.tvAverage.setText("Calificación: " + item.getString(Constants.Movie.VOTE_AVERAGE));
        String imgUrl = Constants.Endpoints.IMAGE_PATH + item.getString(Constants.Movie.POSTER_PATH);
        Glide.with(context).load(imgUrl).apply(Utils.getOptionFitCenter()).into(holder.ivMovie);

        if (item.getBoolean("expand")) {
            holder.expandable.expand();
        } else {
            holder.expandable.collapse();
        }

    }

    @Override
    public int getItemCount() {
        if (items != null && items.size() > 0) {
            return items.size();
        } else {
            return 0;
        }
    }

    public void setItems(List<Bundle> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView tvName;
        ImageView ivMovie;
        TextView tvAverage;
        TextView tvDescription;
        View btnDetails;
        ExpandableLayout expandable;


        Responses.OnResponse mOnResponse;

        public Holder(View itemView, Responses.OnResponse mOnResponse) {
            super(itemView);
            this.mOnResponse = mOnResponse;
            tvName = (TextView) itemView.findViewById(R.id.tvRowMovieName);
            tvAverage = (TextView) itemView.findViewById(R.id.tvRowMovieAverage);
            tvDescription = (TextView) itemView.findViewById(R.id.tvRowMovieDescription);
            ivMovie = (ImageView) itemView.findViewById(R.id.ivRowMovie);
            expandable = (ExpandableLayout) itemView.findViewById(R.id.expandable);
            expandable.setDuration(100);
            itemView.findViewById(R.id.llRowMovieTop).setOnClickListener(this);
            itemView.findViewById(R.id.btnRowMovieDetails).setOnClickListener(this);

            //itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.llRowMovieTop:
                    if (items.get(getAdapterPosition()).getBoolean("expand", false)) {
                        items.get(getAdapterPosition()).putBoolean("expand", false);
                    } else {
                        items.get(getAdapterPosition()).putBoolean("expand", true);
                    }
                    notifyItemChanged(getAdapterPosition());
                    break;

                case R.id.btnRowMovieDetails:
                    try {
                        mOnResponse.onResponse("ViewDetail", items.get(getAdapterPosition()), null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
