package com.frank.plantilla.model.classes.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.frank.plantilla.BuildConfig;
import com.frank.plantilla.model.RxApplication;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.NetworkUtils;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.NetworkService;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.retrofit.observer.ResponseObserver;
//import com.frank.plantilla.views.activities.SplashActivity;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created by Frank on 4/12/17.
 * FROM: http://stackoverflow.com/questions/32547006/connectivitymanager-getnetworkinfoint-deprecated
 */

public class NetworkChangeReceiver extends BroadcastReceiver implements Responses.OnResponse {

    private Context context;

    /**
     * Rest
     **/
    private NetworkService mService;
    private Observable observable;
    private Observer observer;
    private Subscription subscription;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        this.context = context;

        mService = ((RxApplication) context.getApplicationContext()).getNetworkService();

        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            verifyProviders(context);
        }
    }

    private boolean verifyProviders(final Context context) {
        boolean isGpsActive = Utils.isGpsOn(context);
        boolean isNetworkActive = isNetworkActive(context);

        Intent intent = new Intent(Constants.Receiver.CONNECTIVITY_CHANGE);
        intent.putExtra("isGpsActive", isGpsActive);
        intent.putExtra("isNetworkActive", isNetworkActive);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

//        if (Utils.isUserActive(context) && !isGpsActive) {
//            manageSession(false);
//        }

        // Gps is off. Application can not continue correctly. Will not be able to obtain or send updated location.
        // Some devices will continue to obtain location even with GPS off. But location will not be updated.
        if (Utils.isUserActive(context)) {
            if (!isGpsActive || !isNetworkActive) {
                if (isGpsActive && !isNetworkActive) {
                    Utils.showNotification(context, Constants.Notification.INTERNET);
                } else if (!isGpsActive && isNetworkActive) {
                    Utils.showNotification(context, Constants.Notification.GPS);
                } else if (!isGpsActive && !isNetworkActive) {
                    Utils.showNotification(context, Constants.Notification.GPS);
                    Utils.showNotification(context, Constants.Notification.INTERNET);
                }
                return false;
            }
        }

        Utils.hideNotification(context, Constants.Notification.GPS);

        return true;
    }

//    public void manageSession(boolean isActive) {
//        SharedPreferences preferences = Utils.getSharedPreferences(context);
//        JSONObject data = new JSONObject();
//        JSONUtils.put(data, Constants.User.USER_EMAIL, preferences.getString("mail", ""));
//        JSONUtils.put(data, Constants.User.UUID, preferences.getString("code", ""));
//        JSONUtils.put(data, Constants.User.USER_PASSWORD, preferences.getString("pass", ""));
//        JSONUtils.put(data, Constants.User.DATE_OF_ACTION, DateUtils.getChatTimestamp());
//        JSONUtils.put(data, Constants.User.IS_ACTIVE, isActive);
//
//        JSONObject input = new JSONObject();
//        JSONUtils.put(input, Constants.DATA, data);
//        JSONUtils.put(input, "credentials", NetworkUtils.getCredentials(context));
//        post(Constants.Service.MANAGE_SESSION, input, this);
//    }

    public void deactivateProvider(final Context context) {

        HashMap<String, String> params = new HashMap<>();
        params.put("note", "");
        params.put("credentials", NetworkUtils.getCredentials(context));

//        post(Constants.Service.SESSION_END, NetworkUtils.urlEncode(params), this);
    }

    public boolean isNetworkActive(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null;
    }

    /**
     * POST
     **/
    public void post(String serviceKey, String params, Responses.OnResponse onResponse) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");

            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl + " : " + params);
            }

            observable = mService.getPreparedObservable(mService.getPostAPI().post(serviceUrl, RequestBody.create(MediaType.parse("text/plain"), params)), "", false);
            observer = new ResponseObserver(serviceKey, context, onResponse);
            subscription = observable.subscribe(observer);

        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    public void post(String serviceKey, JSONObject params, Responses.OnResponse onResponse, Object... data) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");

            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl + " : " + params);
            }

            observable = mService.getPreparedObservable(mService.getPostAPI().post(serviceUrl, params), "", false);
            observer = new ResponseObserver(serviceKey, context, onResponse, data);
            subscription = observable.subscribe(observer);

        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    @Override
    public void onResponse(String requestKey, Object response, Object data) {
        switch (requestKey) {

//            case Constants.Service.MANAGE_SESSION:
//                if (response != null && response instanceof Bundle) {
//                    Bundle output = (Bundle) response;
//                    if (output.getBoolean("success", false)) {
//                        Utils.deactivateUser(context);
//                        Utils.hideNotification(context, Constants.Notification.LOW_BAND_WIDTH);
//                        Utils.hideNotification(context, Constants.Notification.INTERNET);
//                        String title = context.getString(R.string.notification_service_title);
//                        String text = "Tu estatus ha cambiado a inactivo, verifica que tu GPS esté encendido.";
//
//                        Intent in = new Intent(context, SplashActivity.class);
//                        PendingIntent activityPendingIntent = PendingIntent.getActivity(context, 0, in, 0);
//
//                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
//                                .setWhen(Calendar.getInstance().getTimeInMillis())
//                                .setContentIntent(activityPendingIntent)
//                                .setSmallIcon(R.drawable.icon_white)
//                                .setContentTitle(title)
//                                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
//                                .setContentText(text)
//                                .setAutoCancel(false)
//                                .setOngoing(false)
//                                .setDefaults(Notification.DEFAULT_LIGHTS);
//
//                        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//                        mNotificationManager.notify(Constants.Notification.DESACTIVADO, mBuilder.build());
//
//                        Intent intent = new Intent(Constants.Receiver.STATUS_CHANGE);
//                        intent.putExtra("isActive", false);
//                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//                    }
//
//                }
//                break;
        }
    }

}
