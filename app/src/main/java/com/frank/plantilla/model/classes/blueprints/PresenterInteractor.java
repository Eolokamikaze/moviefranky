package com.frank.plantilla.model.classes.blueprints;

import android.os.Bundle;

import com.frank.plantilla.retrofit.Responses;


import org.json.JSONObject;

/**
 * Created by Frank on 3/28/17.
 * FROM: https://github.com/teegarcs/RetroRx/blob/master/app/src/main/java/com/captech/retrorx/PresenterInteractor.java
 */

public interface PresenterInteractor {
    void rxUnSubscribe();

    void saveState(Bundle state);

    void setState(Bundle state);

    void tryRest();

    void retry();

    void showMessage(String msg);

    void networkError();

    void post(String serviceKey, String params, Responses.OnResponse onResponse, boolean cache, Object... data);

    void post(String serviceKey, JSONObject params, Responses.OnResponse onResponse, boolean cache, Object... data);

    void getAuth(String serviceKey, String params, Responses.OnResponse onResponse, boolean cache, Object... data);

    void getAuth(String serviceKey, String url, String params, Responses.OnResponse onResponse, boolean cache, Object... data);

    void post(String serviceKey, String params, Responses.OnResponse onResponse);

    void postBen(String serviceKey, JSONObject params, Responses.OnResponse onResponse);

    void postBen(String serviceKey, String serviceUrl, JSONObject params, Responses.OnResponse onResponse);

    void postBen(String serviceKey, JSONObject params, Responses.OnResponse onResponse, Object... data);

    void postBen(String serviceKey, String serviceUrl, JSONObject params, Responses.OnResponse onResponse, Object... data);

}