package com.frank.plantilla.model.classes.utils;

import android.content.Context;
import android.graphics.Typeface;

import com.frank.plantilla.R;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Frank on 3/23/17.
 */

public class CustomFontsLoader {

    private static boolean fontsLoaded = false;
    private static HashMap<String, Typeface> fonts;


    /**
     * Returns a loaded custom font based on it's identifier.
     *
     * @param context        - the current context
     * @param fontIdentifier = the identifier of the requested font
     * @return Typeface object of the requested font.
     */
    public static Typeface getTypeface(Context context, String fontIdentifier) {
        if (!fontsLoaded) {
            loadFonts(context);
        }
        return fonts.get(fontIdentifier);
    }


    private static void loadFonts(Context context) {
        fonts = new HashMap<>();

        fonts.put(context.getString(R.string.font_century), Typeface.createFromAsset(context.getAssets(), String.format(Locale.getDefault(), "fonts/%s", context.getString(R.string.font_century))));
        fonts.put(context.getString(R.string.font_helvetica), Typeface.createFromAsset(context.getAssets(), String.format(Locale.getDefault(), "fonts/%s", context.getString(R.string.font_helvetica))));
        fonts.put(context.getString(R.string.font_helvetica_light), Typeface.createFromAsset(context.getAssets(), String.format(Locale.getDefault(), "fonts/%s", context.getString(R.string.font_helvetica_light))));
        fonts.put(context.getString(R.string.font_helvetica_bold), Typeface.createFromAsset(context.getAssets(), String.format(Locale.getDefault(), "fonts/%s", context.getString(R.string.font_helvetica_bold))));

        fontsLoaded = true;
    }
}