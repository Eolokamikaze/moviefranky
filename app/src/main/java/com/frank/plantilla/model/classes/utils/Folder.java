package com.frank.plantilla.model.classes.utils;

import android.os.Bundle;

import java.util.ArrayList;

public class Folder {
    private String title;
    private ArrayList<Bundle> items;
    private boolean isExpandend = false;

    public Folder(String title, ArrayList<Bundle> items) {
        this.title = title;
        this.items = items;

    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Bundle> getItems() {
        return items;
    }

    public boolean isExpandend() {
        return isExpandend;
    }

    public void setExpandend(boolean expandend) {
        isExpandend = expandend;
    }
}
