package com.frank.plantilla.model.classes.utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;

import com.frank.plantilla.R;

public class NotificationsChannels extends ContextWrapper {

    public static final String UBICATION_CHANNEL_ID = "UbicationChannel2";
    public static final String INTERNET_CHANNEL_ID = "InternetChannel2";
    public static final String SERVICE_CHANNEL_ID = "ServiceChannel2";
    public static final String SUBASTA_CHANNEL_ID = "SubastaChannel2";
    public static final String MESSAGE_CHANNEL_ID = "MessageChannel2";
    public static final String NO_SOUND_CHANNEL_ID = "NoSoundChannel";
    public static final String QUERELLA_CHANNEL_ID = "QuerllaChannel";

    public static final String LAST_SUBASTA_CHANNEL_ID = "SubastaChannel";
    public static final String LAST_SERVICE_CHANNEL_ID = "ServiceChannel";
    public static final String LAST_UBICATION_CHANNEL_ID = "UbicationChannel";
    public static final String LAST_INTERNET_CHANNEL_ID = "InternetChannel";
    public static final String LAST_MESSAGE_CHANNEL_ID = "MessageChannel";

    public static final String UBICATION_CHANNEL_NAME = "Ubicación";
    public static final String INTERNET_CHANNEL_NAME = "Internet";
    public static final String SERVICE_CHANNEL_NAME = "Servicios";
    public static final String SUBASTA_CHANNEL_NAME = "Subasta";
    public static final String MESSAGE_CHANNEL_NAME = "Mensajes";
    public static final String NO_SOUND_CHANNEL_NAME = "Notificaciones muteadas";
    public static final String QUERELLA_CHANNEL_NAME = "Querllas";

    private NotificationManager mManager;

    public NotificationsChannels(Context base) {
        super(base);
        createChannels();
    }

    @TargetApi(26)
    public void createChannels() {

        Uri alarmLarge = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alarm_extra_large);
        Uri alarm = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alert);
        Uri alarmQuerella = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.querella_alert);
        AudioAttributes att = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();


        getManager().deleteNotificationChannel(LAST_SERVICE_CHANNEL_ID);
        getManager().deleteNotificationChannel(LAST_SUBASTA_CHANNEL_ID);
        getManager().deleteNotificationChannel(LAST_MESSAGE_CHANNEL_ID);
        getManager().deleteNotificationChannel(LAST_UBICATION_CHANNEL_ID);
        getManager().deleteNotificationChannel(LAST_INTERNET_CHANNEL_ID);

        // create ubication channel
        NotificationChannel ubicationChannel = new NotificationChannel(UBICATION_CHANNEL_ID,
                UBICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        ubicationChannel.enableLights(true);
        ubicationChannel.setLightColor(Color.GREEN);
        ubicationChannel.setSound(alarm, att);
        ubicationChannel.setVibrationPattern(null);
        ubicationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        getManager().createNotificationChannel(ubicationChannel);

        // create internet channel
        NotificationChannel internetChannel = new NotificationChannel(INTERNET_CHANNEL_ID,
                INTERNET_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        internetChannel.enableLights(true);
        internetChannel.setLightColor(Color.GREEN);
        internetChannel.setSound(alarm, att);
        internetChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(internetChannel);


        NotificationChannel serviceChannel = new NotificationChannel(SERVICE_CHANNEL_ID,
                SERVICE_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        serviceChannel.enableLights(true);
        serviceChannel.enableVibration(true);
        serviceChannel.setLightColor(Color.GREEN);
        serviceChannel.setSound(null, null);
        serviceChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(serviceChannel);


        NotificationChannel subastaChannel = new NotificationChannel(SUBASTA_CHANNEL_ID,
                SUBASTA_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        subastaChannel.enableLights(true);
        subastaChannel.enableVibration(true);
        subastaChannel.setLightColor(Color.GREEN);
        subastaChannel.setSound(null, null);
        subastaChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(subastaChannel);

        NotificationChannel messageChannel = new NotificationChannel(MESSAGE_CHANNEL_ID,
                MESSAGE_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        messageChannel.enableLights(true);
        messageChannel.enableVibration(true);
        messageChannel.setLightColor(Color.GREEN);
        messageChannel.setSound(alarm, att);
        getManager().createNotificationChannel(messageChannel);
        messageChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        NotificationChannel nosoundChannel = new NotificationChannel(NO_SOUND_CHANNEL_ID,
                NO_SOUND_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        nosoundChannel.enableLights(true);
        nosoundChannel.setLightColor(Color.GREEN);
        nosoundChannel.setSound(null, null);
        nosoundChannel.setVibrationPattern(null);
        nosoundChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(nosoundChannel);


//        NotificationChannel querellaChannel = new NotificationChannel(QUERELLA_CHANNEL_ID,
//                QUERELLA_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
//        querellaChannel.enableLights(true);
//        querellaChannel.enableVibration(true);
//        querellaChannel.setLightColor(Color.GREEN);
//        querellaChannel.setSound(alarmQuerella, att);
//        querellaChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
//        getManager().createNotificationChannel(querellaChannel);

    }

    private NotificationManager getManager() {
        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mManager;
    }
}
