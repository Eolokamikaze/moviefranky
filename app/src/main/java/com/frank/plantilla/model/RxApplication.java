package com.frank.plantilla.model;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.NetworkService;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.FirebaseApp;

import java.util.ArrayList;

/**
 * Created by Frank on 9/29/16.
 */

public class RxApplication extends Application {

    private NetworkService networkService;
    private SharedPreferences mSharedPreferences;
    private LatLng mLatLng;
    private ArrayList<Bundle> tickets;
    private MediaPlayer defaultRingtone;

    @Override
    public void onCreate() {
        super.onCreate();
        networkService = new NetworkService();
        mSharedPreferences = Utils.getSharedPreferences(this);
        FirebaseApp.initializeApp(getApplicationContext());
        defaultRingtone = new MediaPlayer();
    }

    public MediaPlayer getDefaultRingtone() {
        return defaultRingtone;
    }

    public void setDefaultRingtone(MediaPlayer defaultRingtone) {
        this.defaultRingtone = defaultRingtone;
    }

    public void updateLocation(LatLng mLatLng) {
        this.mLatLng = mLatLng;
    }

    public NetworkService getNetworkService() {
        return networkService;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public LatLng getLocation() {
        return mLatLng;
    }

    public void setTickets(ArrayList<Bundle> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(Bundle newTicket) {
        if (tickets == null) {
            tickets = new ArrayList<>();
        }
        tickets.add(newTicket);
    }

    public ArrayList<Bundle> getTickets() {
        boolean isUserActive = Utils.isUserActive(this);

        if (isUserActive) {
            ArrayList<Bundle> newList = new ArrayList<>();
            if (tickets != null) {
                for (Bundle ticket : tickets) {
                    newList.add((Bundle) ticket.clone());
                }
            }

            return newList;
        }

        return new ArrayList<>();
    }

    public void playRingtone() {
        Uri defaultSoundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName()
                + "/raw/alarm_extra_large");
        defaultRingtone = getDefaultRingtone();
        try {
            AudioManager mobilemode = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mobilemode.setStreamVolume(AudioManager.STREAM_MUSIC, mobilemode.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            if (defaultRingtone.isPlaying())
                defaultRingtone.stop();
            defaultRingtone = new MediaPlayer();
            defaultRingtone.setDataSource(this, defaultSoundUri);
            defaultRingtone.prepare();
            defaultRingtone.start();
            defaultRingtone.setLooping(true);
            setDefaultRingtone(defaultRingtone);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playRingtoneQuerella() {
        Uri defaultSoundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName()
                + "/raw/alert_querella.mp3");
        defaultRingtone = getDefaultRingtone();
        try {
            AudioManager mobilemode = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mobilemode.setStreamVolume(AudioManager.STREAM_MUSIC, mobilemode.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            if (defaultRingtone.isPlaying())
                defaultRingtone.stop();
            defaultRingtone = new MediaPlayer();
            defaultRingtone.setDataSource(this, defaultSoundUri);
            defaultRingtone.setLooping(true);
            defaultRingtone.prepare();
            defaultRingtone.start();
            setDefaultRingtone(defaultRingtone);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
