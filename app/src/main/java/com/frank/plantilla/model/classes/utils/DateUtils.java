package com.frank.plantilla.model.classes.utils;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Frank on 6/18/17.
 */

public class DateUtils {

    public static String getChatTimestamp() {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return df.format(new Date());
    }

    public static String getChatTimestamp(Calendar calendar) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return df.format(calendar.getTime());
    }

    public static long getMessageReference(String dateTime) {
        if (dateTime.contains("Date")) {
            String tmp = dateTime.substring(6, dateTime.length() - 2);
            return Long.valueOf(tmp);
        } else {

        }

        return new Date().getTime();
    }

    public static String getTicketTimestamp(String dateTime) {
        Date date;

        if (dateTime.contains("Date")) {
            String tmp = dateTime.substring(6, dateTime.length() - 2);
            date = new Date(Long.valueOf(tmp));
        } else {
            date = DateUtils.getDate(dateTime);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd 'de' MMM yyyy',' hh:mm a", Locale.getDefault());
        return dateFormat.format(date);
    }

    public static String getTicketTimestamp(String dateTime, String txtDateFormat) {
        Date date;

        if (dateTime.contains("Date")) {
            String tmp = dateTime.substring(6, dateTime.length() - 2);
            date = new Date(Long.valueOf(tmp));
        } else {
            date = new Date();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(txtDateFormat, Locale.getDefault());
        return dateFormat.format(date);
    }

    public static Date getTicketDate(String dateTime) {
        Date date;

        if (dateTime.contains("Date")) {
            String tmp = dateTime.substring(6, dateTime.length() - 2);
            date = new Date(Long.valueOf(tmp));
        } else {
            date = new Date();
        }

        return date;
    }

    public static Date getServiceDate(String dateStr) {
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = formatter.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static Date getDate(String dateStr, String format) {
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            date = formatter.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String getTimestamp() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", new Locale("es_MX"));
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
        String timeStamp;

        try {
            timeStamp = URLDecoder.decode(currentDateandTime, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            timeStamp = "";
        }

        return timeStamp;
    }

    public static Calendar getCalendarFromUTCSTRING(Context context, String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date dateD = null;
        Calendar cal = Calendar.getInstance();
        try {
            dateD = formatter.parse(date);
            cal.setTime(dateD);
        } catch (ParseException e) {
            e.printStackTrace();
            Utils.saveInDebugFile(context, e.toString(), date + " " + format, false);
        }
        return cal;
    }

    public static Calendar getCalendarFromUTC(Context context, String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dateD = null;
        Calendar cal = Calendar.getInstance();
        try {
            dateD = formatter.parse(date);
            cal.setTime(dateD);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.saveInDebugFile(context, e.toString(), date + " " + format, false);
        }
        return cal;
    }


    public static Calendar getCalendarFromUTCSTRING(String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date dateD = null;
        Calendar cal = Calendar.getInstance();
        try {
            dateD = formatter.parse(date);
            cal.setTime(dateD);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    public static String getNow(String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        return formatter.format(cal.getTime());
    }

    public static String getCustomDate(Calendar date, String format, boolean isUtcformat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            if (isUtcformat) {
                TimeZone timeZone = TimeZone.getTimeZone("UTC");
                sdf.setTimeZone(timeZone);
            }
            return sdf.format(date.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean compareToday(Context context, String date, String dateFormat) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);

        if (today.get(Calendar.DAY_OF_MONTH) >= compare.get(Calendar.DAY_OF_MONTH)) {
            if (today.get(Calendar.MONTH) == compare.get(Calendar.MONTH)) {
                if (today.get(Calendar.YEAR) == compare.get(Calendar.YEAR)) {
                    res = true;
                }
            }
        }

        return res;
    }

    public static boolean compareDay(Calendar c1, Calendar c2) {
        boolean res = false;
        if (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
            if (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
                    res = true;
                }
            }
        }

        return res;
    }

    public static boolean compareToday(long dateInMillis) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = Calendar.getInstance();
        compare.setTimeInMillis(dateInMillis);

        if (today.get(Calendar.DAY_OF_MONTH) >= compare.get(Calendar.DAY_OF_MONTH)) {
            if (today.get(Calendar.MONTH) == compare.get(Calendar.MONTH)) {
                if (today.get(Calendar.YEAR) == compare.get(Calendar.YEAR)) {
                    res = true;
                }
            }
        }

        return res;
    }


    public static boolean compareTodayDayWithSeconds(Calendar calendar, int seconds) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = Calendar.getInstance();
        compare.setTime(calendar.getTime());
        compare.add(Calendar.SECOND, seconds);
        if (compare.getTimeInMillis() <= today.getTimeInMillis()) {
            res = true;
        }
        return res;
    }


    public static boolean compareTodayDay(long dateInMillis) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = Calendar.getInstance();
        compare.setTimeInMillis(dateInMillis);

        if (today.get(Calendar.DAY_OF_MONTH) == compare.get(Calendar.DAY_OF_MONTH)) {
            if (today.get(Calendar.MONTH) == compare.get(Calendar.MONTH)) {
                if (today.get(Calendar.YEAR) == compare.get(Calendar.YEAR)) {
                    res = true;
                }
            }
        }

        return res;
    }


    public static int compareRank(Calendar c1, Calendar c2, long timeMilis) {
        int res = 0;
        Calendar compare = Calendar.getInstance();
        compare.setTimeInMillis(timeMilis);
        compare.set(Calendar.MINUTE, 0);
        compare.set(Calendar.HOUR_OF_DAY, 0);
        compare.set(Calendar.SECOND, 0);
        compare.set(Calendar.MILLISECOND, 0);

        if (compareDay(c1, c2)) {
            if (c1.get(Calendar.DAY_OF_MONTH) == compare.get(Calendar.DAY_OF_MONTH)) {
                if (c1.get(Calendar.MONTH) == compare.get(Calendar.MONTH)) {
                    if (c1.get(Calendar.YEAR) == compare.get(Calendar.YEAR)) {
                        return 1;
                    }
                }

            }

        } else {

            if (c1.get(Calendar.DAY_OF_MONTH) == compare.get(Calendar.DAY_OF_MONTH)) {
                if (c1.get(Calendar.MONTH) == compare.get(Calendar.MONTH)) {
                    if (c1.get(Calendar.YEAR) == compare.get(Calendar.YEAR)) {
                        return 2;
                    }
                }
            }

            if (c2.get(Calendar.DAY_OF_MONTH) == compare.get(Calendar.DAY_OF_MONTH)) {
                if (c2.get(Calendar.MONTH) == compare.get(Calendar.MONTH)) {
                    if (c2.get(Calendar.YEAR) == compare.get(Calendar.YEAR)) {
                        return 3;
                    }
                }
            }

            if (c1.getTimeInMillis() <= compare.getTimeInMillis() && compare.getTimeInMillis() <= c2.getTimeInMillis()) {
                return 4;
            }
        }

        return res;
    }

    public static boolean compareToFuture(Context context, String date, String dateFormat) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);
        compare.set(Calendar.HOUR_OF_DAY, 0);
        compare.set(Calendar.MINUTE, 0);
        if (today.getTimeInMillis() >= compare.getTimeInMillis()) {
            res = true;
        }
        return res;
    }

    public static boolean compareToFuture(Context context, String date, String dateFormat,
                                          int hourBefore) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);
        compare.add(Calendar.HOUR_OF_DAY, hourBefore * -1);
        if (today.getTimeInMillis() >= compare.getTimeInMillis()) {
            res = true;
        }
        return res;
    }

    public static boolean compareToMinutesFuture(Context context, String date, String
            dateFormat, int minutes) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);
        compare.add(Calendar.MINUTE, minutes);
        if (today.getTimeInMillis() < compare.getTimeInMillis()) {
            res = true;
        }
        return res;
    }

    public static boolean compareToFutureWithNow(Context context, String date, String
            dateFormat) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);
        if (today.getTimeInMillis() >= compare.getTimeInMillis()) {
            res = true;
        }
        return res;
    }

    public static boolean compareToFutureWithMinutes(Context context, String date, String
            dateFormat) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);
        if (today.getTimeInMillis() >= compare.getTimeInMillis()) {
            res = true;
        }
        return res;
    }

    public static boolean compareToFutureWithMinutesBefore(Context context, String date, String
            dateFormat, int min) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);
        compare.add(Calendar.MINUTE, -1 * min);
        if (today.getTimeInMillis() >= compare.getTimeInMillis()) {
            res = true;
        }
        return res;
    }

    public static boolean compareToFutureWithMinutesAfter(Context context, String date, String
            dateFormat, int min) {
        boolean res = false;
        Calendar today = Calendar.getInstance();
        Calendar compare = getCalendarFromUTCSTRING(context, date, dateFormat);
        compare.add(Calendar.MINUTE, min);
        if (today.getTimeInMillis() >= compare.getTimeInMillis()) {
            res = true;
        }
        return res;
    }

    public static boolean compareWithDatesHourAndTime(Context context, String date1, String
            dateFormat1,
                                                      Calendar calendar2) {
        boolean res = false;
        Calendar calendar1 = getCalendarFromUTCSTRING(context, date1, dateFormat1);

        if (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)) {
            if (calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH)) {
                if (calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)) {
                    if (calendar1.get(Calendar.HOUR_OF_DAY) == calendar2.get(Calendar.HOUR_OF_DAY)) {
                        if (calendar1.get(Calendar.MINUTE) == calendar2.get(Calendar.MINUTE)) {
                            res = true;
                        }
                    }
                }
            }

        }
        return res;
    }


    public static Date getDate(String dt) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dt);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static long getMinDifference(Date date) {
        long min = 0;
        try {
            Date now = Calendar.getInstance().getTime();
            long difference = (now.getTime() - date.getTime()) / 1000;
            long hours = difference % (24 * 3600) / 3600;
            long minute = difference % 3600 / 60;
            min = minute + (hours * 60);

        } catch (Throwable e) {
            e.printStackTrace();
        }
        return min;
    }

    public static String getDifferenceInHours(Calendar c1, Calendar c2) {
        long seconds = (c2.getTimeInMillis() - c1.getTimeInMillis()) / 1000;
        int hours = (int) (seconds / 3600);
        int minutes = (int) (seconds / 60) - (hours * 60);
        String date = "";
        if (hours > 0) {
            date = hours + " hr(s) " + minutes + " min(s)";
        } else {
            date = minutes + " mins";
        }
        return date;
    }
}
