package com.frank.plantilla.model.fcm;

import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.GCUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by TR3SCO on 1/10/17.
 */

public class FirebaseService extends FirebaseInstanceIdService {

    private static final String TAG = FirebaseService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "FCM Token: " + fcmToken);
        sendTokenToServer(fcmToken);

    }

    private void sendTokenToServer(String fcmToken) {
        GCUtils.storeRegistrationId(this, fcmToken);
        Intent registrationComplete = new Intent(Constants.GCM.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }
}
