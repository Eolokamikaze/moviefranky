package com.frank.plantilla.model.classes.blueprints;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.MyProgressDialog;
import com.frank.plantilla.views.dialogs.DialogDisclaimer;
import com.frank.plantilla.views.dialogs.DialogInfo;

public class RestViewFragment extends Fragment implements ViewInteractor {

    private final static String DIALOG_CONNECTIVITY = "connectivity";
    private DialogDisclaimer disclaimerNoConnection;

    public BaseRestPresenter presenter;
    public ProgressDialog progress;

    public void onCreate(BaseRestPresenter presenter) {
        this.presenter = presenter;
    }

    public void showMsg(String error) {

    }

    public void showMsg(String dialogId, String error) {
        try {
            DialogDisclaimer.newInstance(dialogId, getString(R.string.atention), error).
                    show(getChildFragmentManager(), DialogDisclaimer.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showMsgFromConnectivity() {
        try {
            if (disclaimerNoConnection == null) {
                disclaimerNoConnection = DialogDisclaimer.newInstance(Constants.Service.DIALOG_DISCLAIMER, getString(R.string.ConnectivityError),
                        getString(R.string.ServerErrorConecction2), true);
                disclaimerNoConnection.show(getChildFragmentManager(), DialogDisclaimer.class.getName());
            } else if (!disclaimerNoConnection.isVisible()) {
                disclaimerNoConnection = DialogDisclaimer.newInstance(Constants.Service.DIALOG_DISCLAIMER, getString(R.string.ConnectivityError),
                        getString(R.string.ServerErrorConecction2), true);
                disclaimerNoConnection.show(getChildFragmentManager(), DialogDisclaimer.class.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showLoader() {
        progress = ProgressDialog.show(getContext(), "Franky Movies",
                "Cargando", true);

    }

    public void hideLoader() {
        try {
            progress.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void displayNetworkError() {
        DialogInfo.newInstance(DIALOG_CONNECTIVITY, getString(R.string.network), getString(R.string.network_issue)).show(getFragmentManager(), "", false);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.rxUnSubscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.tryRest();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.saveState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.setState(savedInstanceState);
    }
}
