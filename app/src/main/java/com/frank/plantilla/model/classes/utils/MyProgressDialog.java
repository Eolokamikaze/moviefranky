package com.frank.plantilla.model.classes.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.frank.plantilla.R;

/**
 * Created by Frank on 3/23/17.
 */

public class MyProgressDialog {

    private static ProgressDialog progressDialog = null;

    private static void createProgressDialog(final Context context) {

        if (context != null) {
            progressDialog = new ProgressDialog(context, R.style.dialogFullScreen);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            progressDialog.setTitle(context.getString(R.string.loading));
            progressDialog.setMessage(context.getString(R.string.loading));


            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
        }
    }

    public static void show(final Context context) {

        if (progressDialog == null) {
            createProgressDialog(context);
        }

        if (progressDialog != null && !progressDialog.isShowing()) {
            LayoutInflater inflater = LayoutInflater.from(context);
//            View view = inflater.inflate(R.layout.progress_dialog, null);
//
//            ImageView icon = (ImageView) view.findViewById(R.id.ivLoading);
//            icon.setBackgroundResource(R.drawable.loading);

//            AnimationDrawable frameAnimation = (AnimationDrawable) icon.getBackground();
//            try {
//                progressDialog.show();
//                progressDialog.setContentView(view);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            frameAnimation.start();
        }
    }

    public static void hide() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {

            }
        }
        progressDialog = null;
    }

}
