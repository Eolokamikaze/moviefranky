package com.frank.plantilla.model.fcm;

import android.content.SharedPreferences;
import android.media.MediaPlayer;

import com.frank.plantilla.model.classes.utils.GCUtils;
import com.frank.plantilla.model.classes.utils.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by TR3SCO on 1/10/17.
 */

public class MyFireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFireBaseMessagingService.class.getSimpleName();
    private MediaPlayer defaultRingtone;
    private String lastNumService = "";
    private String actionType = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        displayNotification(remoteMessage.getNotification(), remoteMessage.getData());
    }

    @Override
    public void onNewToken(String s) {
        GCUtils.storeRegistrationId(getApplicationContext(), s);
        super.onNewToken(s);
    }


    private void displayNotification(RemoteMessage.Notification notification, Map<String, String> data) {

        String message = "";
        Utils.showNotificationMsg(getApplicationContext(), message, 1);


    }

    private void saveinPreferences(String numService, String action) {
        SharedPreferences.Editor editor = Utils.getSharedPreferences(getApplicationContext()).edit();
        editor.putString("LastNumberService", numService);
        editor.putString("ActionType", action);
        editor.commit();
    }




}
