package com.frank.plantilla.model.classes.constants;

/**
 * Created by Frank on 6/3/15.
 */
public interface Constants {

    interface General {
        String PREFERENCES_NAME = "sharedPreferences";
        String URL = "url";
    }

    String USER = "user";
    String MENU = "menu";
    String TYPE = "type";
    String CONTENT = "content";
    String SERVICES = "services";
    String SECTION = "section";
    String ABOUT = "about";
    String TABS = "tabs";
    String CONFIG = "config";
    String METRICS = "metrics";
    String APP_DATA = "appData";
    String FROM_LOGIN = "fromLogin";
    String DATA = "Data";
    String HEADER = "Header";

    String LATITUD = "Latitud";
    String LONGITUD = "Longitud";
    String COMMENT = "Comment";
    String FROM_NOTIFICATION = "FROM_NOTIFICATION";
    String NUM_SERVICE = "NumerosDeServicios";
    String FECHA_DE_SERVICIOS = "FechaDeServcios";
    String UPDATE_LOCATION = "UpdateLocationVar";
    String APP_VERSION_ID = "appVersionId";
    String APP_VERSION_URL = "appUrl_android";
    String VERSION_APP = "version";
    String JSON_PORCENTAGES = "JsonPorcentages";
    String NEED_RESIZE = "NeedResize";
    String IMG_SCALE = "ImgScale";
    String PHONE = "phone";


    interface Notification {
        int SERVICE = 1001;
        int GPS = 1003;
        int INTERNET = 1002;
        int DESACTIVADO = 1003;
        int ARRIBO = 1004;
        int LOW_BAND_WIDTH = 1005;
        int LOW_BATTERY = 1006;
    }

    interface Types {

    }

    interface GCM {
        String REGISTRATION_COMPLETE = "registrationComplete";
        String NOTIFICATION_RECEIVED = "notificationReceived";
        String MENU_RECEIVED = "menuReceived";
    }

    interface Auth {
        String AUTH = "Auth";
        String TIMESTAMP = "Timestamp";
        String SIGNATURE = "Signature";
        String CREDENTIAL_LOGIN = "CredentialLogin";
        String CREDENTIAL_PWD = "CredentialPwd";
        String CREDENTIAL_PLATFORM = "CredentialPlatform";
        String CREDENTIAL_DATA = "CredentialData";
        String API_KEY = "293711ac44becee513be06114015c02b";
        String API_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyOTM3MTFhYzQ0YmVjZWU1MTNiZTA2MTE0MDE1YzAyYiIsInN1YiI6IjYwZjc0NDYzZjE3NTljMDA1ZjM0NmM0MSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.zguxR-E9E2vsEDZcc5eZKqcyohzfCvZ5h4-7PHMPGAo";
        String ACCOUNT_ID = "60f74463f1759c005f346c41";
        String API_KEY_YOUTUBE = "AIzaSyD3q96q-dxn0bYkA3_X9s6EJGJVey-YPDI";
    }

    interface Receiver {
        String STATUS_CHANGE = "statusChangeReceiver";
        String CONNECTIVITY_CHANGE = "connectivityChangeReceiver";
        String TICKETS_RECEIVED = "ticketsReceivedReceiver";
        String MESSAGE_RECEIVED = "messageReceivedReceiver";
        String LOCATION_RECEIVED = "locationReceivedReceiver";
        String LOW_CONNECTION = "lowConnectionReceiver";
    }

    interface Service {
        String NO_CONNECTION = "NoConnection";
        String DIALOG_DISCLAIMER = "DialogDisclaimer";
        String DIALOG_CHOICE = "DialogChoice";

        String GET_MOVIE_LIST = "GetMovieList";
        String GET_MOVIE_RATED = "GetMovieRated";
        String GET_SEARCH_MOVIE_LIST = "GetSearchMovieList";
        String GET_MOVIE_DETAIL = "GetMovieDetail";
        String GET_VIDEO_LIST = "GetVideoList";

    }

    interface User {

    }

    interface Menu {
        String TITLE = "title";
        String SECTION = "section";
    }

    interface Sections {
        String MENU_FRAGMENT = "MenuFragment";
        String ABOUT = "AboutFragment";
        String SECTION_POPULARES = "SectionPopulares";
        String SECTION_RATE = "SectionRated";
    }

    interface Endpoints {
        String MOVIE_LIST = "https://api.themoviedb.org/3//movie/popular";
        String IMAGE_PATH = "https://image.tmdb.org/t/p/w500";
        String MOVIE_SEARCH = "https://api.themoviedb.org/3/search/movie";
        String MOVIE_RATED = "https://api.themoviedb.org/3/movie/top_rated";
        String MOVIE_DETAIL = "https://api.themoviedb.org/3//movie/";
        String MOVIE_VIDEO_LIST = "https://api.themoviedb.org/3/movie/[id_movie]]/videos";
        String YOUTUBE_THUMBNAIL = "http://img.youtube.com/vi/[VIDEO_ID]/0.jpg";
    }

    interface MovieList {
        String API_KEY = "api_key";
        String QUERY = "query";
        String ITEMS = "items";
        String RESULTS = "results";
    }

    interface Movie {
        String TITLE = "title";
        String VIDEO = "video";
        String OVERVIEW = "overview";
        String POSTER_PATH = "poster_path";
        String ID = "id";
        String RELEASE_DATE = "release_date";
        String VOTE_AVERAGE = "vote_average";
        String GENRES = "genres";
        String TAGLINE = "tagline";
    }

    interface Offline {
        String MOVIE_LIST = "MovieList";
        String MOVIE_RATED = "MovieRated";
    }

    interface Video {
        String KEY = "key";
        String NAME = "name";
        String SITE = "site";
        String YOUTUBE="YouTube";
    }

}
