package com.frank.plantilla.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;


import com.frank.plantilla.model.classes.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Frank on 4/10/17.
 * FROM: https://github.com/android-database-best-practices/device-database/blob/master/app/src/main/java/me/adamstroud/devicedatabase/provider/DevicesProvider.java
 */

public class AutolinkProvider {

    private DBHelper helper;

    private static final HashMap<String, String> TABLE_MAP = new HashMap<>();

    static {
        TABLE_MAP.put(DBHelper.Keys.LANGUAGE, DBHelper.Tables.LANGUAGE);
        TABLE_MAP.put(DBHelper.Keys.GENDER, DBHelper.Tables.GENDER);
        TABLE_MAP.put(DBHelper.Keys.TICKET_STATUS, DBHelper.Tables.TICKET_STATUS);
        TABLE_MAP.put(DBHelper.Keys.COUNTRY, DBHelper.Tables.COUNTRY);
        TABLE_MAP.put(DBHelper.Keys.STATE, DBHelper.Tables.STATE);
        TABLE_MAP.put(DBHelper.Keys.POSITION, DBHelper.Tables.POSITION);
        TABLE_MAP.put(DBHelper.Keys.REJECT_TYPE, DBHelper.Tables.REJECT_TYPE);
        TABLE_MAP.put(DBHelper.Keys.QUESTION_TYPE, DBHelper.Tables.QUESTION_TYPE);
        TABLE_MAP.put(DBHelper.Keys.SERVICE_TYPE, DBHelper.Tables.SERVICE_TYPE);
        TABLE_MAP.put(DBHelper.Keys.SERVICE_GROUP, DBHelper.Tables.SERVICE_GROUP);
        TABLE_MAP.put(DBHelper.Keys.MUNICIPALITY, DBHelper.Tables.MUNICIPALITY);
        TABLE_MAP.put(DBHelper.Keys.CACHE, DBHelper.Tables.CACHE);
        TABLE_MAP.put(DBHelper.Keys.TICKETS, DBHelper.Keys.TICKETS);
    }

    public AutolinkProvider(final Context context) {
        super();
        helper = new DBHelper(context);
    }

    public void close() {
        if (helper.getWritableDatabase().isOpen()) {
            helper.getWritableDatabase().close();
        }

        if (helper.getReadableDatabase().isOpen()) {
            helper.getReadableDatabase().close();
        }
    }

    public void insertTicket(String key, Bundle ticket) {

        ContentValues values = new ContentValues();
        values.put("ticket_id", ticket.getString("ticket_id", ""));
        values.put("ticket_status", ticket.getString("ticket_status", ""));
        values.put("ticket_seen_finish", 0);
        values.put("json", ticket.getString("json", ""));

        insert(key, values);
    }

    public long insert(String key, ContentValues values) {
        return helper
                .getWritableDatabase()
                .insertWithOnConflict(TABLE_MAP.get(key), null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

//    public int delete(String key, String selection, String[] selectionArgs) {
//
//        if (key.equals(Constants.Service.CAT_MOTIVOS_RECHAZO) || key.equals(Constants.Service.CAT_MOTIVOS_NO_CONTACTO)) return 0;
//
//        return helper
//                .getWritableDatabase()
//                .delete(TABLE_MAP.get(key), selection, selectionArgs);
//    }

    public Cursor query(String key, String[] projection, String selection, String[] selectionArgs, String sortOrder) throws IllegalArgumentException {

        SQLiteDatabase database = helper.getReadableDatabase();

        return database.query(TABLE_MAP.get(key),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
    }

    public boolean bulkInsert(String key, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = helper.getWritableDatabase();

        db.beginTransaction();

        try {
            for (ContentValues value : values) {
                insert(key, value);
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            return false;
        } finally {
            db.endTransaction();
        }

        return true;
    }

    public ArrayList<Bundle> getRejectTypes() {
        final SQLiteDatabase db = helper.getReadableDatabase();

        String query = "SELECT * FROM reject_type";
        Log.e("QUERY", query);

        Cursor cursor = db.rawQuery(query, null);
        try {
            ArrayList<Bundle> itemList = new ArrayList<>();
            Bundle item;
            if (cursor.moveToFirst()) {
                do {
                    item = new Bundle();
                    item.putString(Contract.RejectTypes.REJECT_TYPE_ID, cursor.getString(cursor.getColumnIndex(Contract.RejectTypes.REJECT_TYPE_ID)));
                    item.putString(Contract.RejectTypes.REJECT_TYPE_NAME, cursor.getString(cursor.getColumnIndex(Contract.RejectTypes.REJECT_TYPE_NAME)));
                    item.putString(Contract.RejectTypes.REJECT_TYPE_DESC, cursor.getString(cursor.getColumnIndex(Contract.RejectTypes.REJECT_TYPE_DESC)));
                    itemList.add(item);
                } while (cursor.moveToNext());
            }
            return itemList;
        } catch (Exception ignored) {
            ignored.printStackTrace();
        } finally {
            cursor.close();
        }
        return null;
    }

    public ArrayList<Bundle> getHistoric() {
        final SQLiteDatabase db = helper.getReadableDatabase();

        String query = "SELECT * FROM tickets";
        Log.e("QUERY", query);

        Cursor cursor = db.rawQuery(query, null);
        try {
            ArrayList<Bundle> itemList = new ArrayList<>();
            Bundle item;
            String str, status;
            if (cursor.moveToFirst()) {
                do {
                    str = cursor.getString(cursor.getColumnIndex("json"));
                    status = cursor.getString(cursor.getColumnIndex("ticket_status"));
                    if (str != null && !str.equals("")) {
                        item = Utils.createBundle(new JSONObject(str));
                        item.putString(Contract.ServiceTypes.SERV_TYPE_NAME, getServiceTypeName(item.getString(Contract.ServiceTypes.SERV_TYPE_ID, "")));
                        item.putString("ticket_status", status);
                        itemList.add(item);
                    }
                } while (cursor.moveToNext());
            }
            return itemList;
        } catch (Exception ignored) {
            ignored.printStackTrace();
        } finally {
            cursor.close();
        }
        return null;
    }

    public Bundle updateTicket(String ticketId, String ticketStatus) {
        final SQLiteDatabase db = helper.getReadableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("ticket_status", ticketStatus);

        db.update("tickets", cv, "ticket_id=" + ticketId, null);

        Bundle ticket = getTicket(ticketId);

        return ticket;
    }

    public void shownFinishDialog(String ticketId) {
        final SQLiteDatabase db = helper.getReadableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("ticket_seen_finish", 1);

        db.update("tickets", cv, "ticket_id=" + ticketId, null);
    }

    public Bundle getTicket(String ticketId) {
        final SQLiteDatabase db = helper.getReadableDatabase();

        String query = "SELECT * FROM tickets WHERE ticket_id = " + ticketId;
        Log.e("QUERY", query);

        Cursor cursor = db.rawQuery(query, null);
        try {
            Bundle item = null;
            String str, status;
            if (cursor.moveToFirst()) {
                str = cursor.getString(cursor.getColumnIndex("json"));
                status = cursor.getString(cursor.getColumnIndex("ticket_status"));
                if (str != null && !str.equals("")) {
                    item = Utils.createBundle(new JSONObject(str));
                    item.putString(Contract.ServiceTypes.SERV_TYPE_NAME, getServiceTypeName(item.getString(Contract.ServiceTypes.SERV_TYPE_ID, "")));
                    item.putString("ticket_status", status);
                }
            }
            return item;
        } catch (Exception ignored) {
            ignored.printStackTrace();
        } finally {
            cursor.close();
        }
        return null;
    }

    public String getServiceTypeName(String id) {
        final SQLiteDatabase db = helper.getReadableDatabase();

        String query = "SELECT " + Contract.ServiceTypes.SERV_TYPE_NAME + " FROM " + TABLE_MAP.get(Contract.ServiceTypes.KEY) + " WHERE " + Contract.ServiceTypes.SERV_TYPE_ID + " = " + id;
        Log.e("QUERY", query);

        Cursor cursor = db.rawQuery(query, null);

        String serviceName = "";

        try {
            if (cursor.moveToFirst()) {
                serviceName = cursor.getString(cursor.getColumnIndex(Contract.ServiceTypes.SERV_TYPE_NAME));
            }
            return serviceName;
        } catch (Exception ignored) {
            ignored.printStackTrace();
        } finally {
            cursor.close();
        }
        return serviceName;
    }

    public String getServiceTypeStr(String[] serviceTypes) {
        final SQLiteDatabase db = helper.getReadableDatabase();

        StringBuilder result = new StringBuilder();
        for (String serviceType : serviceTypes) {
            result.append(serviceType);
            result.append(", ");
        }
        String ids = result.toString();
        ids = ids.substring(0, ids.length() - 2);

        String query = String.format(Locale.getDefault(), "SELECT group_concat(serv_type_name, ', ') AS result FROM service_type WHERE serv_type_id IN (%s)", ids);
        Log.e("QUERY", query);

        Cursor cursor = db.rawQuery(query, null);

        String serviceName = "";

        try {
            if (cursor.moveToFirst()) {
                serviceName = cursor.getString(cursor.getColumnIndex("result"));
            }
            return serviceName;
        } catch (Exception ignored) {
            ignored.printStackTrace();
        } finally {
            cursor.close();
        }
        return serviceName;
    }


}