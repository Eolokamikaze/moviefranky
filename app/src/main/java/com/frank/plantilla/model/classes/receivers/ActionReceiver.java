package com.frank.plantilla.model.classes.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.frank.plantilla.BuildConfig;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.NetworkService;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.retrofit.observer.ResponseObserver;

import org.json.JSONObject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created by Frank on 5/2/18.
 */

public class ActionReceiver extends BroadcastReceiver implements Responses.OnResponse {

    private Observable observable;
    private Observer observer;

    public Subscription subscription;
    public NetworkService service;

    public Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        this.context = context;

        service = new NetworkService();

//        if ("take".equals(action)) {
//            accept(context, intent.getStringExtra("ticketId"), intent.getIntExtra(NotificationClose.NOTIFICATION_ID, 1));
//        } else if ("reject".equals(action)) {
//            reject(context, intent.getStringExtra("ticketId"),
//                    intent.getStringExtra("message"), intent.getIntExtra(NotificationClose.NOTIFICATION_ID, 1));
//        }
    }

    public void accept(final Context context, String numServicio, int notificationId) {
        JSONObject json = new JSONObject();

//        try {
//            json.put("NumeroServicio", numServicio);
//            json.put("AceptoNotificacion", true);
//            json.put("token", GCUtils.getRegistrationId(context));
//            json.put("UUID", Utils.getSharedPreferences(context).getString("code", ""));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        post(Constants.Service.RESPUESTA_NOTIFICACION, json, this);
        Intent notificationIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        notificationIntent.setPackage(null); // The golden row !!!
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        notificationIntent.putExtra(Constants.Ticket.NO_SERVICIO, numServicio);
//        notificationIntent.putExtra(Constants.Service.ACEPTAR_DESDE_NOTIFICACION, true);
        context.startActivity(notificationIntent);

//        Intent messageReceived = new Intent(Constants.Receiver.TICKET_NOTIFICATION);
//        messageReceived.putExtra(Constants.Ticket.NO_SERVICIO, numServicio);
//        messageReceived.putExtra(Constants.Service.ACEPTAR_DESDE_NOTIFICACION, true);
//        LocalBroadcastManager.getInstance(context).sendBroadcast(messageReceived);
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.cancel(notificationId);
    }


    public void reject(final Context context, String ticketId, String message, int notificationId) {

//        TicketUtils.showNotificationDialog(context, message, ticketId);

        Intent notificationIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        notificationIntent.setPackage(null); // The golden row !!!
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.putExtra("ticketId", ticketId);
        notificationIntent.putExtra("message", message);
//        notificationIntent.putExtra(Constants.Service.ACEPTAR_DESDE_NOTIFICACION, false);
        context.startActivity(notificationIntent);

//        Intent messageReceived = new Intent(Constants.Receiver.TICKET_NOTIFICATION);
//        messageReceived.putExtra("message", message);
//        messageReceived.putExtra("ticketId", ticketId);
//        messageReceived.putExtra(Constants.Service.ACEPTAR_DESDE_NOTIFICACION, false);
//        LocalBroadcastManager.getInstance(context).sendBroadcast(messageReceived);
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.cancel(notificationId);
    }

    public void post(String serviceKey, JSONObject params, Responses.OnResponse onResponse) {
        String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");
        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl + " : " + params);
            Utils.saveInDebugFile(context, serviceUrl, params.toString(), true);
        }

        observable = service.getPreparedObservable(service.getPostAuthAPI().post(serviceUrl, params), "", false);
        observer = new ResponseObserver(serviceKey, context, onResponse);
        subscription = observable.subscribe(observer);
    }

    @Override
    public void onResponse(String requestKey, Object response, Object data) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (response != null && response instanceof Bundle && ((Bundle) response).getBoolean("success", false)) {
            notificationManager.cancel(3);
            Utils.showNotificationMsg(context, "Servicio aceptado", 100);
        } else if (response instanceof String) {
            Utils.showNotificationMsgLng(context, (String) response, 100);
            notificationManager.cancel(3);

        }
    }
}
