package com.frank.plantilla.model.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.frank.plantilla.R;

import java.util.List;

public class SpinnerCombo extends ArrayAdapter<Bundle> {

    private List<Bundle> items;
    private int selected = -1;

    public SpinnerCombo(@NonNull Context context, @NonNull List<Bundle> items) {
        super(context, android.R.layout.simple_spinner_item, items);
        this.items = items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        selected = position;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
            convertView = inflater.inflate(R.layout.row_simple_spinner, parent, false);
        }


        TextView tv = (TextView) convertView.findViewById(R.id.tvSpinner);
        tv.setText(items.get(position).getString("ComboItemName", ""));

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
            convertView = inflater.inflate(R.layout.row_simple_spinner, parent, false);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.tvSpinner);
        tv.setText(items.get(position).getString("ComboItemName", ""));

        return convertView;
    }
}
