package com.frank.plantilla.model.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

/**
 * Created by Frank on 11/27/15.
 */

public class SpinnerCustomAdapter extends ArrayAdapter<Bundle> {
    // Initialise custom font, for example:
//    Typeface font = Typeface.createFromAsset(getContext().getAssets(), getContext().getString(R.string.font_title));
    int color;
    int gravity;
    ArrayList<Bundle> items;
    private boolean customs;
    int textSize;
    String textKey;

    public SpinnerCustomAdapter(Context context, int resource, ArrayList<Bundle> items, String textKey, int color, int gravity) {
        super(context, resource, items);
        this.color = color;
        this.gravity = gravity;
        this.items = items;
        this.textKey = textKey;
    }

    public SpinnerCustomAdapter(Context context, int resource, ArrayList<Bundle> items, String textKey, int color, int gravity, boolean customs) {
        super(context, resource, items);
        this.color = color;
        this.gravity = gravity;
        this.items = items;
        this.customs = customs;
    }

    public SpinnerCustomAdapter(Context context, int resource, ArrayList<Bundle> items, String textKey, int color, int gravity, int textsize) {
        super(context, resource, items);
        this.color = color;
        this.gravity = gravity;
        this.items = items;
        this.textSize = textsize;
    }

    public ArrayList<Bundle> getItems() {
        return items;
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
//        view.setTypeface(font);
        if (textSize == 0)
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        else
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        view.setTextColor(ContextCompat.getColor(getContext(), color));
        view.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
        view.setText(items.get(position).getString(textKey));
        view.setGravity(gravity);
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
//        view.setTypeface(font);
        view.setTextColor(ContextCompat.getColor(getContext(), color));
        view.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
        view.setText(items.get(position).getString(textKey));
        view.setGravity(gravity);
        return view;
    }

    @Override
    public int getPosition(Bundle select) {

        if (items != null && items.size() > 0) {
            int i = 0;
            for (Bundle item : items) {
                if (item.getString(textKey).equals(select.getString(textKey))) {
                    return i;
                }
                i++;
            }
        }
        return 0;
    }
}
