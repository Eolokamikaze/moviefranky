package com.frank.plantilla.model.classes.blueprints;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.frank.plantilla.BuildConfig;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.NetworkUtils;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.NetworkService;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.retrofit.observer.CacheObserver;
import com.frank.plantilla.retrofit.observer.ResponseObserver;
import com.frank.plantilla.model.RxApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Frank on 3/30/17.
 */

public abstract class BaseRestPresenter implements PresenterInteractor, Responses.OnResponse {

    /**
     * Observer and observable to retry network requests when necessary
     **/
    private Observable observable;
    private Observer observer;

    public Subscription subscription;
    public NetworkService service;

    private Context context;

    public BaseRestPresenter(final Context context) {
        service = ((RxApplication) context).getNetworkService();
        this.context = context;
    }

    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    public void get(String serviceKey, HashMap<String, String> params, Observer mObserver) {

        observable = service.getGetAPI()
                .get(Utils.getSharedPreferences(context).getString(serviceKey, serviceKey), params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, Utils.getSharedPreferences(context).getString(serviceKey, serviceKey),
                    params, false);
        }


        observer = mObserver;

        subscription = observable.subscribe(observer);
    }

    public void getBen(String serviceKey, HashMap<String, String> params, Observer... mObserver) {

        observable = service.getBenApi()
                .get(Utils.getSharedPreferences(context).getString(serviceKey, serviceKey), params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, Utils.getSharedPreferences(context).getString(serviceKey, serviceKey),
                    params, false);
        }


        if (mObserver == null || mObserver.length == 0) {
            observer = new ResponseObserver(serviceKey, context, new Responses.OnResponse() {
                @Override
                public void onResponse(String requestKey, Object response, Object data) {
                    switch (requestKey) {

                    }
                }
            });
        } else {
            observer = mObserver[0];
        }

        subscription = observable.subscribe(observer);
    }

    public void getBen(String serviceKey, String serviceUrl, HashMap<String, String> params, Responses.OnResponse onResponse) {
        if (params != null) {
            observable = service.getBenApi()
                    .get(serviceUrl, params)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
        } else {
            observable = service.getBenApi()
                    .get(serviceUrl)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
        }


        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, serviceUrl,
                    params, false);
            Log.e("SERVICE", serviceUrl + " : " + params);

        }
        observer = new ResponseObserver(serviceKey, context, onResponse);
        subscription = observable.subscribe(observer);
    }

    public void getBen(String serviceKey, String serviceUrl, HashMap<String, String> params, Responses.OnResponse onResponse, Object data) {
        if (params != null) {
            observable = service.getBenApi()
                    .get(serviceUrl, params)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
        } else {
            observable = service.getBenApi()
                    .get(serviceUrl)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
        }


        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, serviceUrl,
                    params, false);
            Log.e("SERVICE", serviceUrl + " : " + params);

        }
        observer = new ResponseObserver(serviceKey, context, onResponse, data);
        subscription = observable.subscribe(observer);
    }

    public void getBen(String serviceKey, HashMap<String, String> params, Responses.OnResponse onResponse, Object... data) {
        String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, serviceKey);
        observable = service.getBenApi()
                .get(serviceUrl, params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, serviceUrl,
                    params, false);
        }
        observer = new ResponseObserver(serviceKey, context, onResponse, data);
        subscription = observable.subscribe(observer);
    }

    public void getBen(String serviceKey, HashMap<String, String> params, Responses.OnResponse mResponse, Observer... mObserver) {

        observable = service.getBenApi()
                .get(Utils.getSharedPreferences(context).getString(serviceKey, serviceKey), params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, Utils.getSharedPreferences(context).getString(serviceKey, serviceKey),
                    params, false);
        }


        if (mObserver == null || mObserver.length == 0) {
            observer = new ResponseObserver(serviceKey, context, mResponse);
        } else {
            observer = mObserver[0];
        }

        subscription = observable.subscribe(observer);
    }


    public void getBenUrl(String serviceKey, String url, HashMap<String, String> params, Responses.OnResponse mResponse, Observer... mObserver) {

        observable = service.getBenApi()
                .get(url, params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, url,
                    params, false);
        }


        if (mObserver == null || mObserver.length == 0) {
            observer = new ResponseObserver(serviceKey, context, mResponse);
        } else {
            observer = mObserver[0];
        }

        subscription = observable.subscribe(observer);
    }

    public void getService(String serviceKey, String url, Responses.OnResponse mResponse, Observer... mObserver) {

        observable = service.getPostAPI()
                .get(url)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, url, "", true);
            Log.e("APP", url);
        }


        if (mObserver == null || mObserver.length == 0) {
            observer = new ResponseObserver(serviceKey, context, mResponse);
        } else {
            observer = mObserver[0];
        }

        subscription = observable.subscribe(observer);
    }

    public void getServiceUrl(String serviceKey, String url, Responses.OnResponse mResponse, Bundle data) {

        observable = service.getPostAPI()
                .get(url)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (BuildConfig.DEBUG) {
            Utils.saveInDebugFile(context, url, "", true);
        }


        observer = new ResponseObserver(serviceKey, context, mResponse, data);
        subscription = observable.subscribe(observer);
    }

    @Override
    public void retry() {
        subscription = observable.subscribe(observer);
    }

    @Override
    public void post(String serviceKey, String params, Responses.OnResponse onResponse, boolean useCache, Object... data) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");

            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl + " : " + params);
                Utils.saveInDebugFile(context, serviceUrl, params, true);
            }

            observable = service.getPreparedObservable(service.getPostAPI().post(serviceUrl, RequestBody.create(MediaType.parse("text/plain"), params)), "", false);
            observer = useCache ? new CacheObserver(serviceKey, context, onResponse) : new ResponseObserver(serviceKey, context, onResponse, data);
            subscription = observable.subscribe(observer);

        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    private JSONObject addSignature(JSONObject json) {
        try {
            String signature = "signature";
            json.getJSONObject("Auth").put("Signature", signature);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    public void post(final String serviceKey, JSONObject json, Context context, final Responses.OnResponse mResponse, Object... mData) {
        if (json != null) {
            String url = Utils.getSharedPreferences(context).getString(serviceKey, "");
            json = addSignature(json);
            if (BuildConfig.DEBUG) {
                Log.e("APP", url + " :" + json.toString());
                Utils.saveInDebugFile(context, url, json.toString(), true);
            }


            observable = service.getPreparedObservable(service.getPostAPI().post(url, json), serviceKey, false);
            observer = new ResponseObserver(serviceKey, context, mResponse, mData);
            subscription = observable.subscribe(observer);

        } else {
            Log.e("APP", "Null Json Error");
        }
    }

    public void post(String url, final String serviceKey, JSONObject json, Context context, final Responses.OnResponse mResponse, Object... mData) {
        if (json != null) {
            json = addSignature(json);
            if (BuildConfig.DEBUG) {
                Log.e("APP", url + " :" + json.toString());
//                Utils.saveInDebugFile(context, url, json.toString(), true);
            }
            observable = service.getPreparedObservable(service.getFileAPI().post(url, json), serviceKey, false);
            observer = new ResponseObserver(serviceKey, context, mResponse, mData);
            subscription = observable.subscribe(observer);

        } else {
            Log.e("APP", "Null Json Error");
        }
    }

    @Override
    public void post(String serviceKey, String params, Responses.OnResponse onResponse) {
        post(serviceKey, params, onResponse, false);
    }


    @Override
    public void getAuth(String serviceKey, String params, Responses.OnResponse onResponse, boolean useCache, Object... data) {
        if (params != null) {

            String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");

            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl + " : " + params);
                Utils.saveInDebugFile(context, serviceUrl, params, false);
            }
            if (params.length() > 0)
                serviceUrl = serviceUrl + "?" + params.substring(1);

            observable = service.getPreparedObservable(service.getPostAuthAPI().get(serviceUrl), "", false);
            observer = useCache ? new CacheObserver(serviceKey, context, onResponse) : new ResponseObserver(serviceKey, context, onResponse, data);
            subscription = observable.subscribe(observer);


        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    @Override
    public void getAuth(String serviceKey, String serviceUrl, String params, Responses.OnResponse onResponse, boolean useCache, Object... data) {
        if (params != null) {
            if (BuildConfig.DEBUG) {
                Log.e("SERVICE", serviceUrl + " : " + params);
                Utils.saveInDebugFile(context, serviceUrl, params, false);
            }
            if (params.length() > 0)
                serviceUrl = serviceUrl + "?" + params.substring(1);

            observable = service.getPreparedObservable(service.getPostAuthAPI().get(serviceUrl), "", false);
            observer = useCache ? new CacheObserver(serviceKey, context, onResponse) : new ResponseObserver(serviceKey, context, onResponse, data);
            subscription = observable.subscribe(observer);


        } else {
            onResponse.onResponse(serviceKey, null, null);
        }
    }

    @Override
    public void post(String serviceKey, JSONObject params, Responses.OnResponse onResponse, boolean cache, Object... data) {
        String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");

        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl + " : " + params);
            Utils.saveInDebugFile(context, serviceUrl, params.toString(), true);
        }

        observable = service.getPreparedObservable(service.getBenApi().post(serviceUrl, params), "", false);
        observer = new ResponseObserver(serviceKey, context, onResponse, data);
        subscription = observable.subscribe(observer);
    }

    @Override
    public void postBen(String serviceKey, JSONObject params, Responses.OnResponse onResponse) {
        String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");
        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl + " : " + params);
            Utils.saveInDebugFile(context, serviceUrl, params.toString(), true);
        }

        observable = service.getPreparedObservable(service.getPostAuthAPI().post(serviceUrl, params), "", false);
        observer = new ResponseObserver(serviceKey, context, onResponse);
        subscription = observable.subscribe(observer);
    }

    @Override
    public void postBen(String serviceKey, String serviceUrl, JSONObject params, Responses.OnResponse onResponse) {
        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl + " : " + params);
            Utils.saveInDebugFile(context, serviceUrl, params.toString(), true);
        }
        observable = service.getPreparedObservable(service.getPostAuthAPI().post(serviceUrl, params), "", false);
        observer = new ResponseObserver(serviceKey, context, onResponse);
        subscription = observable.subscribe(observer);
    }

    @Override
    public void postBen(String serviceKey, JSONObject params, Responses.OnResponse onResponse, Object... data) {
        String serviceUrl = Utils.getSharedPreferences(context).getString(serviceKey, "");
        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl + " : " + params);
            Utils.saveInDebugFile(context, serviceUrl, params.toString(), true);
        }

        observable = service.getPreparedObservable(service.getPostAuthAPI().post(serviceUrl, params), "", false);
        observer = new ResponseObserver(serviceKey, context, onResponse, data);
        subscription = observable.subscribe(observer);
    }

    @Override
    public void postBen(String serviceKey, String serviceUrl, JSONObject params, Responses.OnResponse onResponse, Object... data) {
        if (BuildConfig.DEBUG) {
            Log.e("SERVICE", serviceUrl + " : " + params);
            Utils.saveInDebugFile(context, serviceUrl, params.toString(), true);
        }
        observable = service.getPreparedObservable(service.getPostAuthAPI().post(serviceUrl, params), "", false);
        observer = new ResponseObserver(serviceKey, context, onResponse, data);
        subscription = observable.subscribe(observer);
    }


    public Context getContext() {
        return context;
    }

    @Override
    public void onResponse(String requestCode, Object response, Object data) {
        boolean isNetworkActive;

        if (response instanceof String) {
            if (NetworkUtils.isNetworkAvailable(context)) {
                showMessage((String) response);
                isNetworkActive = true;
            } else {
                isNetworkActive = false;
            }
        } else {
            isNetworkActive = NetworkUtils.isNetworkAvailable(context);
        }

        Intent networkChange = new Intent(Constants.Receiver.CONNECTIVITY_CHANGE);
        networkChange.putExtra("isNetworkActive", isNetworkActive);
        networkChange.putExtra("isGpsActive", true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(networkChange);
    }
}
