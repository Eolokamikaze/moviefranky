package com.frank.plantilla.model.database;

import android.content.ContentValues;
import android.os.Bundle;

import java.util.ArrayList;

/**
 * Created by Frank on 4/10/17.
 */

public class JSONToCV {

    public static ContentValues bundleToCV(Bundle bundle) {

        ContentValues value = new ContentValues();

        for (String key : bundle.keySet()) {
            if (bundle.get(key) instanceof String) {
                value.put(key, bundle.getString(key));
            } else if (bundle.get(key) instanceof ArrayList) {
                //value.put(key, (ArrayList<Bundle>)bundle.getParcelableArrayList(key));
            }
        }

        return value;
    }

    public static ContentValues[] bundleArrayToCV(ArrayList<Bundle> list) {
        ContentValues[] values = new ContentValues[list.size()];

        for (int i = 0; i < list.size(); i++) {
            values[i] = bundleToCV(list.get(i));
        }

        return values;
    }

    public static ContentValues[] serviceGroupsToCV(ArrayList<Bundle> list) {
        int s = list.size();

        ContentValues[] values = new ContentValues[list.size()];
        for (int i = 0; i < s; i++) {
            values[i] = bundleToCV(list.get(i));
        }

        return values;
    }

    public static ContentValues[] serviceTypeToCV(ArrayList<Bundle> list, String groupId) {
        int s = list.size();

        ContentValues[] values = new ContentValues[list.size()];
        Bundle item;
        for (int i = 0; i < s; i++) {
            item = list.get(i);
            item.putString(Contract.Groups.SERV_GROUP_ID, groupId);
            values[i] = bundleToCV(item);
        }

        return values;
    }

}
