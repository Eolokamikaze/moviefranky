package com.frank.plantilla.model.classes.blueprints;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.util.Patterns;

import androidx.appcompat.app.AppCompatActivity;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.MyProgressDialog;
import com.frank.plantilla.views.dialogs.DialogDisclaimer;
import com.frank.plantilla.views.dialogs.DialogInfo;

public abstract class RestViewActivity extends AppCompatActivity implements ViewInteractor {

    private final static String DIALOG_CONNECTIVITY = "connectivity";

    public BaseRestPresenter presenter;
    private DialogDisclaimer disclaimerNoConnection;

    public void onCreate(BaseRestPresenter presenter) {
        this.presenter = presenter;
//        TestFairy.begin(this, "734bf114be76809488ffef279dc9047165d94af9");

//        try {
//            Account[] accounts =AccountManager.get(RestViewActivity.this).getAccounts();
//            for (Account account : accounts) {
//                if (Patterns.EMAIL_ADDRESS.matcher(account.name).matches()) {
////                    TestFairy.setUserId(account.name);
//                    break;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void showMsg(String msg) {
        DialogDisclaimer.newInstance(Constants.Service.DIALOG_DISCLAIMER, getString(R.string.atention),
                msg).show(getSupportFragmentManager(), DialogDisclaimer.class.getName());
    }

    public void showMsg(String dialogId, String msg) {
        DialogDisclaimer.newInstance(dialogId, getString(R.string.atention),
                msg).show(getSupportFragmentManager(), DialogDisclaimer.class.getName());
    }

    public void showMsgFromConnectivity() {
        try {
            if (disclaimerNoConnection == null) {
                disclaimerNoConnection = DialogDisclaimer.newInstance(Constants.Service.DIALOG_DISCLAIMER, getString(R.string.ConnectivityError),
                        getString(R.string.ServerErrorConecction2), true);
                disclaimerNoConnection.show(getSupportFragmentManager(), DialogDisclaimer.class.getName());
            } else if (!disclaimerNoConnection.isVisible()) {
                disclaimerNoConnection = DialogDisclaimer.newInstance(Constants.Service.DIALOG_DISCLAIMER, getString(R.string.ConnectivityError),
                        getString(R.string.ServerErrorConecction2), true);
                disclaimerNoConnection.show(getSupportFragmentManager(), DialogDisclaimer.class.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showLoader() {
        MyProgressDialog.show(this);
    }

    public void hideLoader() {
        MyProgressDialog.hide();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.rxUnSubscribe();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.tryRest();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        presenter.setState(savedInstanceState);
    }

    @Override
    public void displayNetworkError() {
        DialogInfo.newInstance(DIALOG_CONNECTIVITY, getString(R.string.atention), getString(R.string.network_issue)).show(getSupportFragmentManager(), "", false);
    }
}
