package com.frank.plantilla.model.classes.utils;

import android.os.Bundle;

import com.framgia.library.calendardayview.data.IEvent;

import java.util.ArrayList;
import java.util.Calendar;

public class ScheduleEvent implements IEvent {


    public static final int TYPE_SERVICE = 1;
    public static final int TYPE_EVENT = 2;


    private Calendar mStartTime;
    private Calendar mEndTime;
    private String mName;
    private String duration;
    private String start;
    private String end;
    private String serviceGroup;
    private int mColor;
    private boolean isFirstAtention;
    private boolean isMigrated;
    private boolean isActived = false;
    private int type;

    private int numberServices;
    private ArrayList<Bundle> serviceList;

    public ScheduleEvent(String mName, Calendar mStartTime, Calendar mEndTime, int mColor, int type) {
        this.mStartTime = mStartTime;
        this.mEndTime = mEndTime;
        this.mName = mName;
        this.mColor = mColor;
        this.type = type;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public int getColor() {
        return mColor;
    }

    @Override
    public Calendar getStartTime() {
        return mStartTime;
    }

    @Override
    public Calendar getEndTime() {
        return mEndTime;
    }

    public boolean isFirstAtention() {
        return isFirstAtention;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setFirstAtention(boolean firstAtention) {
        isFirstAtention = firstAtention;
    }

    public boolean isMigrated() {
        return isMigrated;
    }

    public void setMigrated(boolean migrated) {
        isMigrated = migrated;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(String serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public boolean isActived() {
        return isActived;
    }

    public void setActived(boolean actived) {
        isActived = actived;
    }


    public int getNumberServices() {
        return numberServices;
    }

    public void setNumberServices(int numberServices) {
        this.numberServices = numberServices;
    }

    public ArrayList<Bundle> getServiceList() {
        return serviceList;
    }

    public void setServiceList(ArrayList<Bundle> serviceList) {
        this.serviceList = serviceList;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
