package com.frank.plantilla.model.classes.blueprints;

/**
 * Created by Frank on 4/3/17.
 */

public interface ViewInteractor {

    void onCreate(BaseRestPresenter presenter);

    void showMsg(String error);

    void showMsg(String dialogId, String error);

    void showLoader();

    void hideLoader();

    void displayNetworkError();
}
