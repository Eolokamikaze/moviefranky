package com.frank.plantilla.model.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.Responses;

import java.util.ArrayList;
import java.util.List;


public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.Holder> {

    int[] colors;
    private Responses.OnResponse mOnResponse;
    private List<Bundle> items;
    private int selected = -1;
    private int sel;
    private int reg;
    private int padd;
    private Context context;
    private ArrayList iconsMenu;

    public VideoListAdapter(List<Bundle> items, final Context context, final Responses.OnResponse mOnResponse) {
        this.items = items;
        this.mOnResponse = mOnResponse;
        this.context = context;
    }

    @Override
    public VideoListAdapter.Holder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_video, parent, false);
        return new Holder(v, mOnResponse);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {

        Bundle item = items.get(position);
        if (item.getString(Constants.Video.SITE).equals(Constants.Video.YOUTUBE)) {
            String imgUrl = Constants.Endpoints.YOUTUBE_THUMBNAIL;
            imgUrl = imgUrl.replace("[VIDEO_ID]", item.getString(Constants.Video.KEY));
            Glide.with(context).load(imgUrl).apply(Utils.getOptionFitCenter()).into(holder.ivVideo);
        }
    }

    @Override
    public int getItemCount() {
        if (items != null && items.size() > 0) {
            return items.size();
        } else {
            return 0;
        }
    }

    public void setItems(List<Bundle> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        ImageView ivVideo;
        Responses.OnResponse mOnResponse;

        public Holder(View itemView, Responses.OnResponse mOnResponse) {
            super(itemView);
            this.mOnResponse = mOnResponse;
            ivVideo = (ImageView) itemView.findViewById(R.id.ivRowVideo);
            ivVideo.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivRowVideo:
                    try {
                        mOnResponse.onResponse("ViewVideo", items.get(getAdapterPosition()), null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
