package com.frank.plantilla.model.classes.utils;

import com.framgia.library.calendardayview.data.IPopup;

import java.util.Calendar;

public class SchedulePopup implements IPopup {


    Calendar startTime;
    Calendar endTime;
    String imageStart;
    String imageEnd;
    String title;
    String description;
    String quote;
    boolean isFirstAtention;


    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getQuote() {
        return null;
    }

    @Override
    public String getImageStart() {
        return null;
    }

    @Override
    public String getImageEnd() {
        return null;
    }

    @Override
    public Boolean isAutohide() {
        return null;
    }

    @Override
    public Calendar getStartTime() {
        return null;
    }

    @Override
    public Calendar getEndTime() {
        return null;
    }

    public boolean isFirstAtention() {
        return isFirstAtention;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Calendar endTime) {
        this.endTime = endTime;
    }

    public void setImageStart(String imageStart) {
        this.imageStart = imageStart;
    }

    public void setImageEnd(String imageEnd) {
        this.imageEnd = imageEnd;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public void setFirstAtention(boolean firstAtention) {
        isFirstAtention = firstAtention;
    }
}
