package com.frank.plantilla.model.database;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.frank.plantilla.BuildConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Created by Frank on 4/10/17.
 * FROM: https://github.com/android-database-best-practices/device-database/blob/master/app/src/main/java/me/adamstroud/devicedatabase/provider/DevicesOpenHelper.java
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int SCHEMA_VERSION = 1;
    private static final String DB_NAME = "autolink.db";
    private static final String TAG = "DB";

    private final Context context;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, SCHEMA_VERSION);

        this.context = context;

        SQLiteDatabase db = getWritableDatabase();
        db.enableWriteAheadLogging();
        db.execSQL("PRAGMA foreign_keys = 'ON';");

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (int i = 1; i <= SCHEMA_VERSION; i++) {
            applySqlFile(db, i);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,
                          int oldVersion,
                          int newVersion) {
        for (int i = (oldVersion + 1); i <= newVersion; i++) {
            applySqlFile(db, i);
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);

        setWriteAheadLoggingEnabled(true);
        db.setForeignKeyConstraintsEnabled(true);
    }

    private void applySqlFile(SQLiteDatabase db, int version) {
        BufferedReader reader = null;

        try {
            String filename = String.format("%s.%d.sql", DB_NAME, version);
            final InputStream inputStream =
                    context.getAssets().open(filename);
            reader =
                    new BufferedReader(new InputStreamReader(inputStream));

            final StringBuilder statement = new StringBuilder();

            for (String line; (line = reader.readLine()) != null; ) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Reading line -> " + line);
                }

                // Ignore empty lines
                if (!TextUtils.isEmpty(line) && !line.startsWith("--")) {
                    statement.append(line.trim());
                }

                if (line.endsWith(";")) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Running statement " + statement);
                    }

                    db.execSQL(line);
                    statement.setLength(0);
                }
            }

        } catch (IOException e) {
            Log.e(TAG, "Could not apply SQL file", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.w(TAG, "Could not close reader", e);
                }
            }
        }
    }

    public interface Keys {
        String LANGUAGE = "Languages";
        String GENDER = "Genders";
        String TICKET_STATUS = "TicketStatus";
        String COUNTRY = "Countries";
        String STATE = "States";
        String POSITION = "Positions";
        String REJECT_TYPE = "RejectTypes";
        String QUESTION_TYPE = "QuestionTypes";
        String SERVICE_TYPE = "Services";
        String SERVICE_GROUP = "groups";
        String MUNICIPALITY = "Municipalities";
        String CACHE = "cache";
        String TICKETS = "tickets";
    }

    public interface Tables {
        String LANGUAGE = "language";
        String GENDER = "gender";
        String TICKET_STATUS = "ticket_status";
        String COUNTRY = "country";
        String STATE = "state";
        String POSITION = "position";
        String REJECT_TYPE = "reject_type";
        String QUESTION_TYPE = "question_type";
        String SERVICE_TYPE = "service_type";
        String SERVICE_GROUP = "service_group";
        String MUNICIPALITY = "municipality";
        String CACHE = "cache";
    }

}
