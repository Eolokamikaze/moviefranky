package com.frank.plantilla.model.database;

/**
 * Created by Frank on 4/10/17.
 * FROM: https://github.com/android-database-best-practices/device-database/blob/master/app/src/main/java/me/adamstroud/devicedatabase/provider/DevicesContract.java
 */

public class Contract {

    public interface Languages {
        String KEY = DBHelper.Keys.LANGUAGE;
        String LANG_ID = "lang_id";
        String LANG_CODE = "lang_code";
        String LANG_DESC = "lang_desc";
    }

    public interface Genders {
        String KEY = DBHelper.Keys.GENDER;
        String GENDER_ID = "gender_id";
        String GENDER_NAME = "gender_name";
    }

    public interface TicketStatus {
        String TABLE = "ticket_status";
        String TCK_STAT_ID = "tck_stat_id";
        String TCK_STAT_NAME = "tck_stat_name";
        String TCK_STAT_DESC = "tck_stat_desc";
    }

    public interface Countries {
        String KEY = DBHelper.Keys.COUNTRY;
        String COUNTRY_ID = "country_id";
        String COUNTRY_CODE = "country_code";
        String COUNTRY_NAME = "country_name";
    }

    public interface States {
        String KEY = DBHelper.Keys.STATE;
        String STATE_ID = "state_id";
        String STATE_ABBREVIATION = "state_abbreviation";
        String STATE_NAME = "state_name";
        String COUNTRY_ID = "country_id";
        String BOUNDS = "bounds";
    }

    public interface Positions {
        String KEY = DBHelper.Keys.POSITION;
        String POS_ID = "pos_id";
        String POS_NAME = "pos_name";
        String POS_DESC = "pos_desc";
    }

    public interface RejectTypes {
        String KEY = DBHelper.Keys.REJECT_TYPE;
        String REJECT_TYPE_ID = "reject_type_id";
        String REJECT_TYPE_NAME = "reject_type_name";
        String REJECT_TYPE_DESC = "reject_type_desc";
    }

    public interface QuestionTypes {
        String KEY = DBHelper.Keys.QUESTION_TYPE;
        String QUEST_TYPE = "quest_type";
        String QUEST_TYPE_DESC = "quest_type_desc";
        String QUEST_CSV = "quest_csv";
    }

    public interface ServiceTypes {
        String KEY = DBHelper.Keys.SERVICE_TYPE;
        String SERV_TYPE_ID = "serv_type_id";
        String SERV_TYPE_NAME = "serv_type_name";
        String SERV_TYPE_DESC = "serv_type_desc";
    }

    public interface Groups {
        String KEY = DBHelper.Keys.SERVICE_GROUP;
        String SERV_GROUP_ID = "serv_group_id";
        String SERV_GROUP_NAME = "serv_group_name";
    }

    public interface Municipalities {
        String KEY = DBHelper.Keys.MUNICIPALITY;
        String MUNIC_ID = "munic_id";
        String MUNIC_NAME = "munic_name";
        String STATE_ID = "state_id";
        String BOUNDS = "bounds";
    }

    public interface Cache {
        String KEY = DBHelper.Keys.CACHE;
        String NAME = "name";
        String JSON = "json";
    }
}