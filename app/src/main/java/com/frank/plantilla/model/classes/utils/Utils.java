package com.frank.plantilla.model.classes.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.database.AutolinkProvider;
import com.frank.plantilla.model.database.Contract;

import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Frank on 3/23/17.
 */

public class Utils {

    public static void startAnimation(ImageView icon, int resource) {
        icon.setBackgroundResource(resource);
        AnimationDrawable frameAnimation = (AnimationDrawable) icon.getBackground();
        frameAnimation.start();
    }

    public static void fragmentChooserWithoutAddToBackStack(int fragmentReplace, Fragment fragmentSiguiente, FragmentManager fragmentManager, String tag) {
        FragmentTransaction transactionList = fragmentManager.beginTransaction();
        // transactionList.setCustomAnimations(R.anim.enter, R.anim.exit,
        // R.anim.pop_enter, R.anim.pop_exit);
        transactionList.replace(fragmentReplace, fragmentSiguiente, tag);
        transactionList.commitAllowingStateLoss();
    }


    public static void fragmentChooser(int fragmentReplace, Fragment fragmentSiguiente, FragmentManager fragmentManager, String tag) {

        FragmentTransaction transactionList = fragmentManager.beginTransaction();
        transactionList.setCustomAnimations(R.anim.enter_new_api, 0,
                R.anim.pop_enter_new_api, 0);
        transactionList.replace(fragmentReplace, fragmentSiguiente, tag);
        transactionList.addToBackStack("pila");
        transactionList.commit();

    }

    public static Snackbar makeSnackbar(View parent) {
        final Snackbar mSnackbar = Snackbar.make(parent, "", Snackbar.LENGTH_INDEFINITE);
        mSnackbar.setActionTextColor(ContextCompat.getColor(parent.getContext(), R.color.colorTextSecondary));

        View view = mSnackbar.getView();
        TextView tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(ContextCompat.getColor(parent.getContext(), R.color.colorTextSecondary));

        mSnackbar.setAction(parent.getContext().getString(R.string.accept), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSnackbar.dismiss();
            }
        });

        return mSnackbar;
    }

    public static ArrayList<Bundle> createBundleArray(JSONArray array) throws JSONException {
        ArrayList<Bundle> menu = new ArrayList<>();
        Bundle section;
        JSONObject obj;

        for (int i = 0; i < array.length(); i++) {

            obj = array.getJSONObject(i);
            section = new Bundle();

            Iterator<String> iter = obj.keys();

            while (iter.hasNext()) {
                String key = iter.next();

                if (obj.get(key) instanceof Integer) {
                    section.putInt(key, obj.getInt(key));
                } else if (obj.get(key) instanceof Double) {
                    section.putDouble(key, obj.getDouble(key));
                } else if (obj.get(key) instanceof Boolean) {
                    section.putBoolean(key, obj.getBoolean(key));
                } else if (obj.get(key) instanceof JSONArray) {
                    JSONArray innerArray = obj.getJSONArray(key);

                    ArrayList<Bundle> bElements = new ArrayList<>();
                    ArrayList<String> sElements = new ArrayList<>();

                    Bundle bInnerData;

                    for (int j = 0; j < innerArray.length(); j++) {
                        if (innerArray.get(j) instanceof JSONObject) {
                            bInnerData = createBundle(innerArray.getJSONObject(j));
                            bElements.add(bInnerData);
                        } else if (innerArray.get(j) instanceof String) {
                            sElements.add(innerArray.getString(j));
                        }
                    }

                    if (bElements.size() > 0) {
                        section.putParcelableArrayList(key, bElements);
                    }

                    if (sElements.size() > 0) {
                        section.putStringArrayList(key, sElements);
                    }
                } else {
                    section.putString(key, obj.getString(key));
                }

            }
            menu.add(section);
        }

        if (menu.size() == 0) {
            return null;
        }

        return menu;
    }

    public static Bundle createBundle(JSONObject obj) throws JSONException {

        Bundle data = new Bundle();
        Iterator<String> iter = obj.keys();

        while (iter.hasNext()) {
            String key = iter.next();

            if (obj.get(key) instanceof Boolean) {
                data.putBoolean(key, obj.getBoolean(key));
            } else if (obj.get(key) instanceof JSONObject) {
                data.putBundle(key, createBundle(obj.getJSONObject(key)));
            } else if (obj.get(key) instanceof JSONArray) {

                JSONArray innerArray = obj.getJSONArray(key);

                if (innerArray != null && innerArray.length() > 0) {

                    ArrayList<Bundle> bElements = new ArrayList<>();
                    ArrayList<String> sElements = new ArrayList<>();
                    Bundle bInnerData;

                    for (int j = 0; j < innerArray.length(); j++) {
                        if (innerArray.get(j) instanceof JSONObject) {
                            bInnerData = createBundle(innerArray.getJSONObject(j));
//                            if (key.equals("tickets") && bInnerData != null) {
//                                bInnerData.putString("json", innerArray.getJSONObject(j).toString());
//                            }

                            bElements.add(bInnerData);
                        } else if (innerArray.get(j) instanceof String) {
                            sElements.add(innerArray.getString(j));
                        }
                    }

                    if (bElements.size() > 0) {
                        data.putParcelableArrayList(key, bElements);
                    }

                    if (sElements.size() > 0) {
                        data.putStringArrayList(key, sElements);
                    }
                }
            } else if (!obj.isNull(key)) {
                data.putString(key, obj.getString(key));
            }
        }

        if (data.isEmpty()) {
            return null;
        }

        return data;
    }

    /**
     * Codify String to MD5
     *
     * @param message to codify
     * @return string codified
     */
    public static String StringToMD5(String message) {

        String digest;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8"));
            // converting byte array to Hexadecimal
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (Exception ex) {
            digest = "";
        }

        return digest;
    }

    public static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences(Constants.General.PREFERENCES_NAME, Activity.MODE_PRIVATE);
    }

    public static String getDeviceLanguage() {
        String lang = Locale.getDefault().getDisplayLanguage();
        if (lang.indexOf("en") == 0) {
            return "1";
        } else {
            return "2";
        }
    }

    private static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

//    public static boolean isValidForm(SparseArray<EditText> values) {
//        boolean isValidForm = true;
//
//        for (int i = 0, nsize = values.size(); i < nsize; i++) {
//            EditText et = values.valueAt(i);
//
//            if (et == null || et.getText() == null || et.getText().toString().isEmpty()) {
//                if (et != null) {
//                    et.setError("Campo obligatorio");
//                }
//                isValidForm = false;
//            } else {
//                if (et.getId() == R.id.etLoginMail) {
//                    isValidForm = isValidEmail(et.getText().toString());
//                    if (!isValidForm) {
//                        et.setError("Correo electrónico inválido");
//                    }
//                }
//            }
//        }
//
//        return isValidForm;
//    }

    public static Bundle getLogin(SharedPreferences mSharedPreferences) {
        Bundle login = new Bundle();
        String token = mSharedPreferences.getString(GCUtils.PROPERTY_REG_ID, "");
        login.putString("mail", mSharedPreferences.getString("mail", ""));
        login.putString("code", mSharedPreferences.getString("code", ""));
        login.putString("uid", mSharedPreferences.getString("uid", ""));
        login.putString("pass", mSharedPreferences.getString("pass", ""));
        login.putString("device_t", token.equals("") ? "0" : "2");
        login.putString("token", token);

        return login;
    }

    public static void doLogin(SharedPreferences mSharedPreferences, Bundle login) {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();

        mEditor.putBoolean("isUserLoggedIn", true);
        mEditor.putString("mail", login.getString("mail", ""));
        mEditor.putString("code", login.getString("code", ""));
        mEditor.putString("uid", login.getString("uid", ""));
        mEditor.putString("pass", login.getString("pass", ""));
        mEditor.putString("device_t", login.getString("device_t", ""));
        mEditor.putString("token", login.getString("token", ""));
        mEditor.apply();
    }

    public static void doLogout(SharedPreferences mSharedPreferences) {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();

        mEditor.remove("isUserLoggedIn");
        mEditor.remove("mail");
        mEditor.remove("code");
        mEditor.remove("uid");
        mEditor.remove("pass");
        mEditor.remove("device_t");
        mEditor.remove("token");

        mEditor.apply();
    }

    public static boolean isLogin(SharedPreferences mSharedPreferences) {
        return mSharedPreferences.getBoolean("isUserLoggedIn", false);
    }

    public static boolean isUserActive(final Context context) {
        boolean isActive;
        try {
            SharedPreferences mPreferences = getSharedPreferences(context);
            isActive = mPreferences.getBoolean("isUserActive", false);
        } catch (Exception e) {
            e.printStackTrace();
            isActive = false;
        }
        return isActive;
    }

    public static void toggleUserStatus(final Context context) {
        SharedPreferences mPreferences = getSharedPreferences(context);
        SharedPreferences.Editor mEditor = mPreferences.edit();
        boolean status = mPreferences.getBoolean("isUserActive", false);
        mEditor.putBoolean("isUserActive", !status);
        mEditor.apply();
    }

    public static void activateUser(final Context context) {
        SharedPreferences mPreferences = getSharedPreferences(context);
        SharedPreferences.Editor mEditor = mPreferences.edit();
        mEditor.putBoolean("isUserActive", true);

        mEditor.apply();
    }

    public static void deactivateUser(final Context context) {
        SharedPreferences mPreferences = getSharedPreferences(context);
        SharedPreferences.Editor mEditor = mPreferences.edit();
        mEditor.putBoolean("isUserActive", false);

        mEditor.apply();
    }

    public static boolean isGpsOn(final Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private static void insert(final Context context, String key, ContentValues value) {
        AutolinkProvider provider = new AutolinkProvider(context);
        provider.insert(key, value);
        provider.close();
    }

    public static void insertCache(final Context context, String key, JSONObject json) {
        insert(context, Contract.Cache.KEY, getContentValues(key, json));
    }

    private static ContentValues getContentValues(String key, JSONObject json) {
        ContentValues mValues = new ContentValues();
        mValues.put(Contract.Cache.NAME, key);
        mValues.put(Contract.Cache.JSON, json.toString());

        return mValues;
    }

    public static Bundle getCache(final Context context, String key) {
        Bundle cache = null;

        AutolinkProvider provider = new AutolinkProvider(context);
        Cursor cursor = provider.query(Contract.Cache.KEY, new String[]{Contract.Cache.JSON}, String.format("%s = ?", Contract.Cache.NAME), new String[]{key}, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            String value = cursor.getString(cursor.getColumnIndex(Contract.Cache.JSON));

            try {
                cache = createBundle(new JSONObject(value));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return cache;
    }

    public static void showNotificationMsg(Context context, String msg, int notificationId) {
        if (Utils.stringNotNull(msg)) {
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,
                    NotificationsChannels.MESSAGE_CHANNEL_ID)
                    .setSmallIcon(R.drawable.icon_add)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setSound(sound)
                    .setVibrate(new long[]{5000, 5000, 5000, 5000, 5000})
                    .setContentText(msg)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(notificationId, mBuilder.build());
        }


    }

    public static void showNotificationMsgLng(Context context, String msg, int notificationId) {
        if (Utils.stringNotNull(msg)) {
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,
                    NotificationsChannels.MESSAGE_CHANNEL_ID)
                    .setSmallIcon(R.drawable.icon_add)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setSound(sound)
                    .setVibrate(new long[]{5000, 5000, 5000, 5000, 5000})
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(msg))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(notificationId, mBuilder.build());
        }


    }

    public static void showNotification(final Context context, int notificationId) {

//        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        boolean isService = notificationId == Constants.Notification.SERVICE;
//
//        String title = context.getString(isService ? R.string.notification_service_title : notificationId == Constants.Notification.GPS ? R.string.notification_gps_title : R.string.notification_internet_title);
//        Uri sound = isService ? Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.alarm_extra_large) : Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.alarm);
//        Uri soundDefault = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.alert);
//        String text = context.getString(isService ? R.string.notification_service_text : notificationId == Constants.Notification.GPS ? R.string.notification_gps_text : R.string.notification_internet_text);
//
//        Intent intent = isService ? new Intent(context, SplashActivity.class) : new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//        PendingIntent activityPendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
//
//        NotificationCompat.Builder mBuilder;
//        if (isService) {
//            mBuilder = new NotificationCompat.Builder(context, NotificationsChannels.SERVICE_CHANNEL_ID)
//                    .setWhen(Calendar.getInstance().getTimeInMillis())
//                    .setContentIntent(activityPendingIntent)
//                    .setSmallIcon(R.drawable.icon_white)
//                    .setContentTitle(title)
//                    .setContentText(text)
//                    .setAutoCancel(false)
//                    .setOngoing(false)
////                    .setSound(sound)
//                    .setVibrate(new long[]{5000, 5000, 5000, 5000, 5000})
//                    .setDefaults(Notification.DEFAULT_LIGHTS);
//        } else if (notificationId == Constants.Notification.GPS) {
//            Intent gpsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//            PendingIntent gpsPendingIntent = PendingIntent.getActivity(context, 0, gpsIntent, 0);
//            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.Notification.GPS);
//
//            mBuilder = new NotificationCompat.Builder(context, NotificationsChannels.UBICATION_CHANNEL_ID)
//                    .addAction(R.drawable.gps, "GPS", gpsPendingIntent)
//                    .setWhen(Calendar.getInstance().getTimeInMillis())
//                    .setSmallIcon(R.drawable.icon_white)
//                    .setContentTitle(title)
//                    .setContentText(text)
//                    .setAutoCancel(false)
//                    .setOngoing(true)
//                    .setSound(soundDefault)
//                    .setVibrate(new long[]{5000})
//                    .setDefaults(Notification.DEFAULT_LIGHTS);
//        } else if (notificationId == Constants.Notification.LOW_BAND_WIDTH) {
//            Intent internetIntent = new Intent(Settings.ACTION_SETTINGS);
//            PendingIntent internetPendingIntent = PendingIntent.getActivity(context, 0, internetIntent, 0);
//            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.Notification.GPS);
//            mBuilder = new NotificationCompat.Builder(context, NotificationsChannels.INTERNET_CHANNEL_ID)
//                    .addAction(R.drawable.wifi, "Internet", internetPendingIntent)
//                    .setWhen(Calendar.getInstance().getTimeInMillis())
//                    .setSmallIcon(R.drawable.icon_white)
//                    .setContentTitle(context.getString(R.string.LowConecctionTitle))
//                    .setStyle(new NotificationCompat.BigTextStyle()
//                            .bigText(context.getString(R.string.low_bandwidth_msg)))
//                    .setAutoCancel(false)
//                    .setOngoing(true)
//                    .setSound(soundDefault)
//                    .setVibrate(new long[]{5000})
//                    .setDefaults(Notification.DEFAULT_LIGHTS);
//        } else {
//            Intent internetIntent = new Intent(Settings.ACTION_SETTINGS);
//            PendingIntent internetPendingIntent = PendingIntent.getActivity(context, 0, internetIntent, 0);
//            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.Notification.GPS);
//            notificationManager.cancel(Constants.Notification.LOW_BAND_WIDTH);
//            mBuilder = new NotificationCompat.Builder(context, NotificationsChannels.INTERNET_CHANNEL_ID)
//                    .addAction(R.drawable.wifi, "Internet", internetPendingIntent)
//                    .setWhen(Calendar.getInstance().getTimeInMillis())
//                    .setSmallIcon(R.drawable.icon_white)
//                    .setContentTitle(title)
//                    .setContentText(text)
//                    .setAutoCancel(false)
//                    .setOngoing(true)
//                    .setSound(soundDefault)
//                    .setVibrate(new long[]{5000})
//                    .setDefaults(Notification.DEFAULT_LIGHTS);
//        }
//
//        mNotificationManager.notify(notificationId, mBuilder.build());
    }


    public static void showActionNotification(Context context, String message, String serviceNumber) {
        Intent yesReceive = new Intent();
        yesReceive.setAction("take");
        yesReceive.putExtra("ticketId", serviceNumber);
        yesReceive.putExtra("message", message);
        PendingIntent pendingIntentYes = PendingIntent.getBroadcast(context, 12345, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent noReceive = new Intent();
        noReceive.setAction("reject");
        noReceive.putExtra("ticketId", serviceNumber);
        noReceive.putExtra("message", message);
        PendingIntent pendingIntentNo = PendingIntent.getBroadcast(context, 12346, noReceive, PendingIntent.FLAG_UPDATE_CURRENT);

//        Uri defaultSoundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.alarm_long);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.drawable.icon)
//                .setContentTitle(context.getString(R.string.app_name))
////                .setContentText(message)
//                .setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText(message))
//                .setOngoing(true)
//                .setSound(defaultSoundUri)
//                .addAction(R.drawable.btn_active_user, "Tomar", pendingIntentYes)
//                .addAction(R.drawable.btn_active_user, "Rechazar", pendingIntentNo);
        //.setContentIntent(intent);

//
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(3, notificationBuilder.build());

    }

    public static void hideNotification(final Context context, int notificationId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancel(notificationId);
    }

    /**
     * Returns the {@code location} object as a human readable string.
     *
     * @param location The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "No se pudo enviar tu ubicación" :
                "Actualizando ubicación";
    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.app_name);
    }

    public static String handleCoordinate(double coordinate) {
        return String.valueOf(coordinate);
    }

    public static float dpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static void dialPhoneNumber(String phoneNumber, final Context context) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static void whatsAppContactOpen(String contact, Context context) {
        String url = "https://api.whatsapp.com/send?phone=" + contact;
        try {
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(context, "No tienes instalado WhatsApp", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static JSONObject bundleToJSON(Bundle bundle) {
        JSONObject json = new JSONObject();
        Set<String> keys = bundle.keySet();
        for (String key : keys) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    json.put(key, JSONObject.wrap(bundle.get(key)));
                } else {
                    json.put(key, bundle.get(key));
                }
            } catch (JSONException e) {
                //Handle exception here
            }
        }

        return json;
    }

    public static JSONObject getAuth(boolean hasData) throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject auth = new JSONObject();

        auth.put("Timestamp", DateUtils.getTimestamp());
        json.put("Auth", auth);


        if (hasData) {
            json.put("Data", new JSONObject());
        }

        return json;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void deleteFileDebug(Context context) {
        String rootPath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath() + "/Debug/";
        File root = new File(rootPath);
        File file = new File(root, "Request.txt");
        if (root.exists()) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    public static void saveInDebugFile(Context context, String url, String params, boolean isPost) {
        if (context != null) {
            try {
                String rootPath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                        .getAbsolutePath() + "/Debug/";
                File root = new File(rootPath);
                if (!root.exists()) {
                    root.mkdir();
                }
                File file = new File(rootPath, "Request.txt");
                String data = "";
                if (isPost) {
                    data = "\n" + url + "\n" + params;
                } else {
                    if (params != null && params.length() > 0)
                        data = "\n" + url + "?" + params.substring(1);
                    else
                        data = "\n" + url;
                }
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        String content = getStringFromFile(file.getAbsolutePath());
                        data = content + data;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                FileOutputStream inputStream = new FileOutputStream(file.getAbsolutePath(), false);
                inputStream.write(data.getBytes());
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static String getHashmapString(HashMap<String, String> map) {
        ArrayList<String> keys = new ArrayList<>();
        String params = "";
        for (String key : map.keySet()) {
            keys.add(key);
        }
        if (keys != null) {
            for (int i = 0; i < keys.size(); i++) {
                if (i > 0) {
                    params += "&";
                }
                params += keys.get(i) + "=" + map.get(keys.get(i));
            }
        }
        return params;
    }

    public static void saveInDebugFile(Context context, String url, HashMap params, boolean isPost) {
        String rootPath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath() + "/Debug/";
        File root = new File(rootPath);
        if (!root.exists()) {
            root.mkdir();
        }
        File file = new File(rootPath, "Request.txt");
        String data = "";
        if (isPost) {
            data = "\n" + url + "\n" + params;
        } else {
            if (params != null && params.size() > 0) {
                data = "\n" + url + "?" + getHashmapString(params);
            } else
                data = "\n" + url;
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                String content = getStringFromFile(file.getAbsolutePath());
                data = content + data;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream inputStream = new FileOutputStream(file.getAbsolutePath(), false);
            inputStream.write(data.getBytes());
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile(String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public static boolean stringNotNull(String text) {
        boolean res = true;
        if (text == null) {
            res = false;
        } else if (text.length() == 0) {
            res = false;
        } else if (text.equals("null")) {
            res = false;
        } else if (text.trim().length() == 0) {
            res = false;
        }

        return res;
    }

//    public static void removeServiceCounter(Context context) {
//        SharedPreferences.Editor editor = Utils.getSharedPreferences(context).edit();
//        editor.remove(Constants.NUM_SERVICE);
//        editor.remove(Constants.FECHA_DE_SERVICIOS);
////        editor.remove(Constants.NotificationHistory.NOTIFICATION_HISTORY);
//        editor.apply();
//    }

    public static String findAddress(String address, String apikey) {
        String url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=[address]&inputtype=textquery&fields=formatted_address,name,geometry&key=[apikey]";
        url = url.replace("[address]", address);
        url = url.replace("[apikey]", apikey);
        return url;
    }

    public static String getLocationsData(String origin, String destination, String apikey) {
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=[origin]&destination=[destination]&key=[apikey]";
        url = url.replace("[origin]", origin);
        url = url.replace("[destination]", destination);
        url = url.replace("[apikey]", apikey);
        return url;
    }

    public static void saveFailLocation(Context context, Bundle info) {
        String jsonText = getSharedPreferences(context).getString("FailedLocation", "");
        JSONObject json = null;
        JSONObject data = Utils.bundleToJSON(info);
        if (Utils.stringNotNull(jsonText)) {
            try {
                json = new JSONObject(jsonText);
                JSONArray array = json.getJSONArray("list");
                array.put(data);
                json.put("list", array);
            } catch (Exception e) {

            }
        } else {
            try {
                json = new JSONObject();
                JSONArray array = new JSONArray();
                array.put(data);
                json.put("list", array);
            } catch (Exception e) {

            }
        }
        if (json != null) {
            SharedPreferences.Editor editor = getSharedPreferences(context).edit();
            editor.putString("FailedLocation", json.toString());
            editor.apply();
        }


    }

    public static void saveSuccessLocation(Context context, Bundle info) {
        String jsonText = getSharedPreferences(context).getString("SuccessLocation", "");
        JSONObject json = null;
        JSONObject data = Utils.bundleToJSON(info);
        if (Utils.stringNotNull(jsonText)) {
            try {
                json = new JSONObject(jsonText);
                JSONArray array = json.getJSONArray("list");
                array.put(data);
                json.put("list", array);
            } catch (Exception e) {

            }
        } else {
            try {
                json = new JSONObject();
                JSONArray array = new JSONArray();
                array.put(data);
                json.put("list", array);
            } catch (Exception e) {

            }
        }
        if (json != null) {
            SharedPreferences.Editor editor = getSharedPreferences(context).edit();
            editor.putString("SuccessLocation", json.toString());
            editor.apply();
        }


    }


    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static RequestOptions getOptionCenterCrop() {
        RequestOptions options = new RequestOptions()
                .centerCrop();
        return options;
    }

    public static RequestOptions getOptionFitCenter() {
        RequestOptions options = new RequestOptions()
                .fitCenter();
        return options;
    }

    public static ArrayList<Bundle> getArrayListFromXML(XmlPullParser xpp, String etiqueta) {
        ArrayList<Bundle> list = new ArrayList<>();
        String ns = null;
        try {
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    Log.e("Parse", "Start tag " + xpp.getName());
                    if (xpp.getName().equals(etiqueta)) {
                        list.add(getItemParse(xpp));
                    }
                }
                xpp.next();
                eventType = xpp.getEventType();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    private static void saltarEtiqueta(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private static Bundle getItemParse(XmlPullParser parser) throws IOException, XmlPullParserException {
        Bundle b = new Bundle();
//        try {
//            while (parser.next() != XmlPullParser.END_TAG) {
//                String name = parser.getName();
//
//                switch (name) {
//                    case Constants.Auxiliar.AUXILIAR:
//                        if (b.containsKey(name)) {
//                            String aux = b.getString(name);
//                            String result = aux + " " + obtenerTexto(parser);
//                            b.putString(name, result);
//                        } else {
//                            b.putString(name, obtenerTexto(parser));
//                        }
//                        break;
//                    case Constants.Auxiliar.DESCRIPCION:
//                    case Constants.Auxiliar.EQUIVALENCIA:
//                    case Constants.Auxiliar.ID:
//                        b.putString(name, obtenerTexto(parser));
//                        break;
//
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return b;
    }

    private static String obtenerTexto(XmlPullParser parser) throws IOException, XmlPullParserException {
        String resultado = "";
        if (parser.next() == XmlPullParser.TEXT) {
            resultado = parser.getText();
            parser.nextTag();
        }
        return resultado;
    }

    public static ArrayList<Bundle> createList(Context context) {
        String[] titles = context.getResources().getStringArray(R.array.menulistTitle);
        String[] sections = context.getResources().getStringArray(R.array.menulistSection);
        ArrayList<Bundle> list = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            Bundle b = new Bundle();
            b.putString(Constants.Menu.TITLE, titles[i]);
            b.putString(Constants.Menu.SECTION, sections[i]);
            list.add(b);
        }
        return list;
    }

    public static void saveDataList(Context context, ArrayList<Bundle> list, String key) {
        SharedPreferences preferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        if (list != null) {
            JSONArray array = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                array.put(bundleToJSON(list.get(i)));
            }
            editor.putString(key, array.toString());
            editor.apply();
        }
    }

    public static ArrayList<Bundle> getSavedDataList(Context context, String key) {
        ArrayList<Bundle> list = new ArrayList<>();
        SharedPreferences preferences = getSharedPreferences(context);
        String arrayText = preferences.getString(key, "");
        if (stringNotNull(arrayText)) {
            try {
                JSONArray array = new JSONArray(arrayText);
                for (int i = 0; i < array.length(); i++) {
                    list.add(createBundle(array.getJSONObject(i)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }


}

