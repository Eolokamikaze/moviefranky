package com.frank.plantilla.model.classes.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by Frank on 9/28/15.
 */
public class GCUtils {

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static String getRegistrationId(final Context context) {

        SharedPreferences preferences = Utils.getSharedPreferences(context);

        String registrationId = preferences.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }

//        int registeredVersion = preferences.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
//        int currentVersion = getAppVersion(context);
//        if (registeredVersion != currentVersion) {
//            return "";
//        }
        return registrationId;
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static boolean checkPlayServices(final Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("GCM", "This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    public static void storeRegistrationId(Context context, String regId) {
        SharedPreferences preferences = Utils.getSharedPreferences(context);
        int appVersion = GCUtils.getAppVersion(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(GCUtils.PROPERTY_REG_ID, regId);
        editor.putInt(GCUtils.PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }

    public static String returnToken(final Context context) {
        SharedPreferences preferences = Utils.getSharedPreferences(context);
        return preferences.getString(PROPERTY_REG_ID, "");
    }
}