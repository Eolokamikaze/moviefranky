package com.frank.plantilla.presenter;

import android.os.Bundle;

import com.frank.plantilla.model.classes.blueprints.BaseRestPresenter;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.views.fragments.MainMovieListFragment;
import com.frank.plantilla.views.fragments.MainSearchMovieListFragment;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Frank on 4/5/17.
 */

public class MainSearchMovieListPresenter extends BaseRestPresenter implements Responses.OnResponse {

    private MainSearchMovieListFragment view;
    private boolean isLogout;
    private boolean isActive;

    public MainSearchMovieListPresenter(MainSearchMovieListFragment view) {
        super(view.getActivity().getApplicationContext());
        this.view = view;
    }

    public void getSearchMovieList(String query) {

        String url = Constants.Endpoints.MOVIE_SEARCH;
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.MovieList.API_KEY, Constants.Auth.API_KEY);
        params.put(Constants.MovieList.QUERY, query);
        view.showLoader();
        getBen(Constants.Service.GET_SEARCH_MOVIE_LIST, url, params, this);

    }


    @Override
    public void saveState(Bundle state) {

    }

    @Override
    public void setState(Bundle state) {

    }

    @Override
    public void tryRest() {

    }

    @Override
    public void showMessage(String msg) {
        view.showError(msg);
    }

    @Override
    public void networkError() {
        view.displayNetworkError();
    }


    @Override
    public void onResponse(String requestCode, Object response, Object data) {
        super.onResponse(requestCode, response, data);

        switch (requestCode) {
            case Constants.Service.GET_SEARCH_MOVIE_LIST:
                view.hideLoader();
                if (response != null) {
                    Bundle output = (Bundle) response;
                    ArrayList<Bundle> movieList = output.getParcelableArrayList(Constants.MovieList.RESULTS);
                    view.sendData(movieList);
                } else {
                    view.showMsg(Constants.Service.DIALOG_DISCLAIMER, "Error");
                }
                break;

            case Constants.Service.NO_CONNECTION:
                view.hideLoader();
                view.showMsg(Constants.Service.DIALOG_DISCLAIMER, "Datos no disponibles");
                break;
        }
    }


}
