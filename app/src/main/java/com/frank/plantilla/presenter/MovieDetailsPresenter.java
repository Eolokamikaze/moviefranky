package com.frank.plantilla.presenter;

import android.os.Bundle;

import com.frank.plantilla.model.classes.blueprints.BaseRestPresenter;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.views.fragments.MainMovieListFragment;
import com.frank.plantilla.views.fragments.MovieDetailFragment;

import java.util.ArrayList;

/**
 * Created by Frank on 4/5/17.
 */

public class MovieDetailsPresenter extends BaseRestPresenter implements Responses.OnResponse {

    private MovieDetailFragment view;
    private boolean isLogout;
    private boolean isActive;

    public MovieDetailsPresenter(MovieDetailFragment view) {
        super(view.getActivity().getApplicationContext());
        this.view = view;
    }

    public void getMovieDetail(String id) {
        String url = Constants.Endpoints.MOVIE_DETAIL + id;
        view.showLoader();
        getBen(Constants.Service.GET_MOVIE_DETAIL, url, null, this, id);

    }

    public void getVideoList(String id) {
        String url = Constants.Endpoints.MOVIE_VIDEO_LIST;
        url = url.replace("[id_movie]", id);
        getBen(Constants.Service.GET_VIDEO_LIST, url, null, this);

    }


    @Override
    public void saveState(Bundle state) {

    }

    @Override
    public void setState(Bundle state) {

    }

    @Override
    public void tryRest() {

    }

    @Override
    public void showMessage(String msg) {
        view.showError(msg);
    }

    @Override
    public void networkError() {
        view.displayNetworkError();
    }


    @Override
    public void onResponse(String requestCode, Object response, Object data) {
        super.onResponse(requestCode, response, data);

        switch (requestCode) {
            case Constants.Service.GET_MOVIE_DETAIL:
                if (response != null) {
                    Bundle output = (Bundle) response;
                    view.setData(output);
                    getVideoList((String) data);
                } else {
                    view.hideLoader();
                    view.showMsg(Constants.Service.DIALOG_DISCLAIMER, "Error");
                }
                break;

            case Constants.Service.GET_VIDEO_LIST:
                view.hideLoader();
                if (response != null) {
                    Bundle output = (Bundle) response;
                    ArrayList<Bundle> videoList = output.getParcelableArrayList(Constants.MovieList.RESULTS);
                    view.setVideoList(videoList);
                } else {
                    view.showMsg(Constants.Service.DIALOG_DISCLAIMER, "Error");
                }
                break;

            case Constants.Service.NO_CONNECTION:
                view.hideLoader();
                view.showMsg(Constants.Service.DIALOG_DISCLAIMER, "Datos no disponibles");
                break;
        }
    }


}
