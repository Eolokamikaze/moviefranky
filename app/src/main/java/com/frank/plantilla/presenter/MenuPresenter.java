package com.frank.plantilla.presenter;

import android.os.Bundle;

import com.frank.plantilla.model.classes.blueprints.BaseRestPresenter;
import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.views.fragments.MenuFragment;

/**
 * Created by Frank on 4/5/17.
 */

public class MenuPresenter extends BaseRestPresenter implements Responses.OnResponse {

    private MenuFragment view;
    private boolean isLogout;
    private boolean isActive;

    public MenuPresenter(MenuFragment view) {
        super(view.getActivity().getApplicationContext());
        this.view = view;
    }

//    public void toggleStatus() {
//        view.showLoader();
//
//        boolean isUserActive = Utils.isUserActive(view.getContext());
//
//        HashMap<String, String> params = new HashMap<>();
//        params.put("note", "");
//        params.put("credentials", NetworkUtils.getCredentials(view.getContext()));
//
//        String requestCode = isUserActive ? Constants.Service.SESSION_END : Constants.Service.SESSION_INIT;
//
//        post(requestCode, NetworkUtils.urlEncode(params), this);
//    }


    @Override
    public void saveState(Bundle state) {

    }

    @Override
    public void setState(Bundle state) {

    }

    @Override
    public void tryRest() {

    }

    @Override
    public void showMessage(String msg) {
        view.showError(msg);
    }

    @Override
    public void networkError() {
        view.displayNetworkError();
    }


    @Override
    public void onResponse(String requestCode, Object response, Object data) {
        super.onResponse(requestCode, response, data);

        switch (requestCode) {

        }
    }
}
