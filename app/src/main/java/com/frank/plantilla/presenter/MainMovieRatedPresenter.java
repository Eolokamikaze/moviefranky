package com.frank.plantilla.presenter;

import android.os.Bundle;

import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.model.classes.blueprints.BaseRestPresenter;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.NetworkUtils;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.views.fragments.MainMovieRatedFragment;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Frank on 4/5/17.
 */

public class MainMovieRatedPresenter extends BaseRestPresenter implements Responses.OnResponse {

    private MainMovieRatedFragment view;
    private boolean isLogout;
    private boolean isActive;

    public MainMovieRatedPresenter(MainMovieRatedFragment view) {
        super(view.getActivity().getApplicationContext());
        this.view = view;
    }

    public void getMovieRated() {
        String url = Constants.Endpoints.MOVIE_RATED;
        view.showLoader();
        getBen(Constants.Service.GET_MOVIE_RATED, url, null, this);

    }


    @Override
    public void saveState(Bundle state) {

    }

    @Override
    public void setState(Bundle state) {

    }

    @Override
    public void tryRest() {

    }

    @Override
    public void showMessage(String msg) {
        view.showError(msg);
    }

    @Override
    public void networkError() {
        view.displayNetworkError();
    }


    @Override
    public void onResponse(String requestCode, Object response, Object data) {
        super.onResponse(requestCode, response, data);

        switch (requestCode) {
            case Constants.Service.GET_MOVIE_RATED:
                view.hideLoader();
                if (response != null) {
                    Bundle output = (Bundle) response;
                    ArrayList<Bundle> movieList = output.getParcelableArrayList(Constants.MovieList.RESULTS);
                    Utils.saveDataList(getContext(), movieList, Constants.Offline.MOVIE_RATED);
                    view.sendData(movieList);
                } else {
                    view.showMsg(Constants.Service.DIALOG_DISCLAIMER, "Error");
                }
                break;

            case Constants.Service.NO_CONNECTION:
                view.hideLoader();
                ArrayList<Bundle> movieList = Utils.getSavedDataList(getContext(), Constants.Offline.MOVIE_RATED);
                if (movieList != null && movieList.size() > 0) {
                    view.sendData(movieList);
                } else {
                    view.showMsg(Constants.Service.DIALOG_DISCLAIMER, "Datos no disponibles");
                }
                break;
        }
    }
}
