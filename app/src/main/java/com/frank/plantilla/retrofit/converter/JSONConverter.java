package com.frank.plantilla.retrofit.converter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by Frank on 12/29/15.
 */
public final class JSONConverter extends Converter.Factory {

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        if (JSONObject.class.equals(type)) {
            return new Converter<ResponseBody, JSONObject>() {

                @Override
                public JSONObject convert(ResponseBody responseBody) throws IOException {

                    JSONObject data;
                    try {
                        data = new JSONObject(responseBody.string());
                    } catch (JSONException e) {
                        data = null;
                    }

                    return data;
                }
            };
        }
        return super.responseBodyConverter(type, annotations, retrofit);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        if (JSONObject.class.equals(type)) {
            return new Converter<JSONObject, RequestBody>() {
                @Override
                public RequestBody convert(JSONObject value) throws IOException {
                    return RequestBody.create(MediaType.parse("text/plain"), value.toString());
                }
            };
        }
        return super.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit);
    }

}