package com.frank.plantilla.retrofit;

import com.frank.plantilla.model.classes.utils.Utils;

import java.io.IOException;
import java.util.TreeMap;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Frank on 12/29/15.
 */
public class SignatureInterceptor implements Interceptor {
    private static final String ALL_SECTIONS_PREFIX = "2XVxeSE48Tu3D2#EaAa+!rdLTr8X@r74";

    @Override
    public Response intercept(Chain chain) throws IOException {
        String[] params, tmp;

        Request initialRequest = chain.request();

        String url = initialRequest.url().toString();
        params = url.split("&");

        TreeMap<String, String> orderedParams = new TreeMap<>();

        for (int i = 1; i < params.length; i++) {
            tmp = params[i].split("=");
            orderedParams.put(tmp[0], tmp.length > 1 ? tmp[1] : "");
        }

        String tmpUrl = "";
        for (String key : orderedParams.keySet()) {
            if (!orderedParams.get(key).equals("")) {
                tmpUrl += key + "=" + orderedParams.get(key) + "&";
            }
        }
        tmpUrl = java.net.URLDecoder.decode(tmpUrl.substring(0, tmpUrl.length() - 1), "UTF-8");

        String signedUrl = params[0] + tmpUrl + "&signature=" + Utils.StringToMD5(ALL_SECTIONS_PREFIX + tmpUrl.toLowerCase());
        return chain.proceed(initialRequest.newBuilder().url(signedUrl).build());
    }
}

