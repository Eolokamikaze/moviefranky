package com.frank.plantilla.retrofit.observer;

import android.content.Context;
import android.os.Bundle;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.utils.NetworkUtils;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.Responses;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Response;
import rx.Observer;

/**
 * Created by Frank on 12/29/15.
 */
public class TicketObserver implements Observer {

    private boolean onNext = false;
    private final String requestKey;
    private final Responses.OnResponse mResponse;
    private final Context context;
    private final Object mData;

    public TicketObserver(final String requestKey, final Context context, final Responses.OnResponse mResponse, final Object... mData) {
        this.requestKey = requestKey;
        this.mResponse = mResponse;
        this.context = context;
        this.mData = mData != null && mData.length > 0 ? mData[0] : null;
    }

    @Override
    public void onCompleted() {
        if (!onNext) {
            mResponse.onResponse(requestKey, null, mData);
        }
    }

    @Override
    public void onError(Throwable e) {
        mResponse.onResponse(requestKey, context.getString(R.string.network_issue), mData);
    }

    @Override
    public void onNext(Object o) {
        onNext = true;
        Response<JSONObject> bundleResponse = (Response<JSONObject>) o;

        if (bundleResponse.isSuccessful()) {
            Bundle response = null;
            try {
                response = Utils.createBundle(bundleResponse.body());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (NetworkUtils.isSuccess(response)) {
                mResponse.onResponse(requestKey, response, mData);
            } else {
                mResponse.onResponse(requestKey, NetworkUtils.getMessage(response), null);
            }
        } else {
            mResponse.onResponse(requestKey, bundleResponse.message(), null);
        }
    }
}
