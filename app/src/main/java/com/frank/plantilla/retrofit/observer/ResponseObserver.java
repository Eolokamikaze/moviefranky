package com.frank.plantilla.retrofit.observer;

import android.content.Context;
import android.os.Bundle;

import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.MyProgressDialog;
import com.frank.plantilla.model.classes.utils.NetworkUtils;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.retrofit.Responses;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import retrofit2.Response;
import rx.Observer;

/**
 * Created by Frank on 12/29/15.
 */
public class ResponseObserver implements Observer {

    private boolean onNext = false;
    private final String requestKey;
    private final Responses.OnResponse mResponse;
    private final Context context;
    private final Object mData;

    public ResponseObserver(final String requestKey, final Context context, final Responses.OnResponse mResponse, final Object... mData) {
        this.requestKey = requestKey;
        this.mResponse = mResponse;
        this.context = context;
        this.mData = mData != null && mData.length > 0 ? mData[0] : null;
    }

    @Override
    public void onCompleted() {
        if (!onNext) {
            mResponse.onResponse(requestKey, null, mData);
        }
    }

    @Override
    public void onError(Throwable e) {
        MyProgressDialog.hide();
        if (e instanceof UnknownHostException) {
            mResponse.onResponse(Constants.Service.NO_CONNECTION, context.getString(R.string.ServerErrorConecction), mData);
        } else if (e instanceof SocketTimeoutException || e instanceof TimeoutException) {
            mResponse.onResponse(Constants.Service.NO_CONNECTION, context.getString(R.string.ServerErrorConecction2), mData);
        } else {
            mResponse.onResponse(requestKey, "Contacta a mesa de ayuda\n" + e, mData);
        }

    }


    @Override
    public void onNext(Object o) {
        onNext = true;
        Response<JSONObject> bundleResponse = (Response<JSONObject>) o;

        if (bundleResponse.isSuccessful()) {
            Bundle response = null;
            try {
                String raw = ((Response<JSONObject>) o).raw().body().toString();
                response = Utils.createBundle(bundleResponse.body());
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            }
            if (response != null) {
                mResponse.onResponse(requestKey, response, mData);
            } else {
                mResponse.onResponse(requestKey, context.getString(R.string.ServerErrorConecction), mData);
            }
        } else {
            if (bundleResponse.errorBody() != null) {
                try {
                    Bundle tmp = Utils.createBundle(new JSONObject(bundleResponse.errorBody().string()));
                    mResponse.onResponse(requestKey, "Error", requestKey);
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    mResponse.onResponse(requestKey, bundleResponse.message(), requestKey);
                }
            } else {
                mResponse.onResponse(requestKey, bundleResponse.message(), requestKey);
            }
        }
    }
}
