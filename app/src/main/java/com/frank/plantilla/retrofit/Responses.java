package com.frank.plantilla.retrofit;

/**
 * Created by Frank on 3/4/15.
 */
//TODO rename to interface class. This is not really a handler
public class Responses {

    public interface OnResponse<T> {
        void onResponse(String requestKey, T response, T data);
    }
}
