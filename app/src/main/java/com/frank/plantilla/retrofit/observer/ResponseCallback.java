package com.frank.plantilla.retrofit.observer;

import android.os.Bundle;
import android.util.Xml;

import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.Utils;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by CDeni on 12/29/15.
 */
public class ResponseCallback implements Callback {

    private final String requestCode;
    private final Responses.OnResponse mResponse;

    public ResponseCallback(final String requestCode, final Responses.OnResponse mResponse) {
        this.requestCode = requestCode;
        this.mResponse = mResponse;
    }

    @Override
    public void onResponse(Call call, retrofit2.Response response) {

        if (response.isSuccessful()) {
            String body = response.body().toString();
            switch (requestCode) {
//                case Constants.Service.GET_GUIA:
//                    Bundle data = null;
//                    try {
//                        String json = body.replace("\\", "");
//                        json = json.substring(1, json.length());
//                        JSONObject object = new JSONObject(json);
//                        data = Utils.createBundle(object);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    mResponse.onResponse(requestCode, data, null);
//
//                    break;

//                case Constants.Service.GET_POSTAL_CODE_CONTENT:
//                    XmlPullParser parser = Xml.newPullParser();
//                    try {
//                        InputStream stream = new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8));
//                        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
//                        parser.setInput(stream, null);
//                        ArrayList<Bundle> list = Utils.getArrayListFromXML(parser, "auxiliar");
//                        mResponse.onResponse(requestCode, list, null);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

//                    break;

            }
        } else {
            mResponse.onResponse(requestCode, "Error", null);
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        mResponse.onResponse(requestCode, t.toString(), null);
    }
}
