package com.frank.plantilla.retrofit.observer;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.frank.plantilla.BuildConfig;
import com.frank.plantilla.R;
import com.frank.plantilla.model.classes.utils.NetworkUtils;
import com.frank.plantilla.model.classes.utils.Utils;
import com.frank.plantilla.model.database.AutolinkProvider;
import com.frank.plantilla.retrofit.Responses;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observer;

/**
 * Created by Frank on 06/26/17.
 */
public class TicketInfoObserver implements Observer {

    private final String requestKey;
    private final Responses.OnResponse mResponse;
    private final Context context;

    private ArrayList<Bundle> result;

    public TicketInfoObserver(final String requestKey, final Context context, final Responses.OnResponse mResponse) {
        this.requestKey = requestKey;
        this.mResponse = mResponse;
        this.context = context;

        result = new ArrayList<>();
    }

    @Override
    public void onCompleted() {
        mResponse.onResponse(requestKey, result, null);
    }

    @Override
    public void onError(Throwable e) {
        mResponse.onResponse(requestKey, context.getString(R.string.network_issue), null);
    }

    private String getTicketId(Bundle ticketInfo) {
        ArrayList<Bundle> msgs = ticketInfo.getParcelableArrayList("opposite_msgs");
        if (msgs != null && msgs.size() > 0) {
            return msgs.get(0).getString("ticket_id", "");
        }

        msgs = ticketInfo.getParcelableArrayList("mine_msgs");
        if (msgs != null && msgs.size() > 0) {
            return msgs.get(0).getString("ticket_id", "");
        }

        return "";
    }

    @Override
    public void onNext(Object o) {
        Response<JSONObject> bundleResponse = (Response<JSONObject>) o;

        if (bundleResponse.isSuccessful()) {
            Bundle response = null;
            try {
                response = Utils.createBundle(bundleResponse.body());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (NetworkUtils.isSuccess(response)) {
                AutolinkProvider db = new AutolinkProvider(context);
                Bundle ticket = db.updateTicket(getTicketId(response), response.getString("ticket_status", ""));
                db.close();

                result.add(ticket);
            }

            if (BuildConfig.DEBUG) {
                Log.i("RESPONSE", bundleResponse.body().toString());
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.i("RESPONSE", bundleResponse.message());
            }
        }

    }
}
