package com.frank.plantilla.retrofit;

import android.util.Log;

import com.frank.plantilla.model.classes.constants.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Frank on 12/29/15.
 */
public class BearerInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request initialRequest = chain.request();
        String url = initialRequest.url().toString();
        return chain.proceed(initialRequest.newBuilder()
                .addHeader("Authorization", "Bearer " + Constants.Auth.API_TOKEN)
                .addHeader("Content-Type", "application/json")
                .url(url).build());
    }
}

