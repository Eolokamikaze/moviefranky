package com.frank.plantilla.retrofit;

import android.util.LruCache;

import com.frank.plantilla.retrofit.converter.JSONConverter;
import com.frank.plantilla.retrofit.converter.StringConverter;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Frank on 9/29/16.
 * FROM:  https://www.captechconsulting.com/blogs/a-mvp-approach-to-lifecycle-safe-requests-with-retrofit-20-and-rxjava
 */

public class NetworkService {

    private LruCache<String, Observable<Response<JSONObject>>> apiObservables;

    /**
     * Handle the connect and read timeout in minutes
     **/
    private final static int TIMEOUT_MINS = 1;

    /**
     * Unsigned NetworkAPI to execute postWithAuth requests
     **/
    private NetworkAPI postAPI;

    /**
     * Signed NetworkAPI. This object is signed using the SignatureInterceptor to implement our GET Signature Algorithm.
     **/
    private NetworkAPI getAPI;

    /**
     * Unsigned NetworkAPI for simple gets.
     **/
    private NetworkAPI getBenAPI;
    private NetworkAPI postAuthAPI;
    private NetworkAPI fileAPI;

    private final static String USERNAME = "Tr3scoAPI";
    private final static String PASSWORD = "t835C04P1@#2017";

    /**
     * Class' constructor. This is used to create the NetworkAPI objects.
     **/
    public NetworkService() {

        apiObservables = new LruCache<>(10);

        postAPI = createApi(true, false);
        getAPI = createApi(false, true);
        getBenAPI = createApi(false, false);
        fileAPI = getFileText();
    }

    /**
     * @return
     */
    private NetworkAPI createApi(boolean isPost, boolean isSigned) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.readTimeout(TIMEOUT_MINS, TimeUnit.MINUTES);
        okHttpClient.connectTimeout(TIMEOUT_MINS, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(TIMEOUT_MINS, TimeUnit.MINUTES);


        Retrofit retrofit;

        if (!isPost) {
            if (isSigned) {
                okHttpClient.interceptors().add(new SignatureInterceptor());
            } else {
                okHttpClient.interceptors().add(new BearerInterceptor());
            }
        }

        retrofit = new Retrofit.Builder()
                .client(okHttpClient.build())
                .addConverterFactory(new JSONConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl("http://apps.tr3sco.net/")
                .build();

        return retrofit.create(NetworkAPI.class);
    }

    private NetworkAPI getFileText() {
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .addConverterFactory(new StringConverter())
                .baseUrl("http://apps.tr3sco.net/")
                .build();

        return retrofit.create(NetworkAPI.class);
    }

    /**
     * returns postAPI.
     *
     * @return
     */
    public NetworkAPI getPostAPI() {
        if (postAPI == null) {
            postAPI = createApi(true, false);
        }
        return postAPI;
    }

    public NetworkAPI getFileAPI() {
        if (fileAPI == null) {
            fileAPI = getFileText();
        }
        return postAPI;
    }


    /**
     * returns getAPI. This NetworkApi uses the SignatureInterceptor
     *
     * @return
     */
    public NetworkAPI getGetAPI() {
        if (getAPI == null) {
            getAPI = createApi(false, true);
        }
        return getAPI;
    }

    /**
     * returns getBenAPI. Regular get API
     *
     * @return
     */
    public NetworkAPI getBenApi() {
        if (getBenAPI == null) {
            getBenAPI = createApi(false, false);
        }
        return getBenAPI;
    }

    public NetworkAPI getPostAuthAPI() {
        if (postAuthAPI == null) {
            postAuthAPI = createApiAuth(true);
        }
        return postAuthAPI;
    }


    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache() {
        apiObservables.evictAll();
    }


    private NetworkAPI createApiAuth(boolean isPost) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.readTimeout(TIMEOUT_MINS, TimeUnit.MINUTES);
        okHttpClient.connectTimeout(TIMEOUT_MINS, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(TIMEOUT_MINS, TimeUnit.MINUTES);


        Retrofit retrofit;
        okHttpClient.interceptors().add(new BasicAuthInterceptor(USERNAME, PASSWORD));

        retrofit = new Retrofit.Builder()
                .client(okHttpClient.build())
                .addConverterFactory(new JSONConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl("http://apps.tr3sco.net/")

                .build();

        return retrofit.create(NetworkAPI.class);
    }


    /**
     * Method to either return a cached observable or prepare a new one.
     *
     * @param unPreparedObservable
     * @param key
     * @param useCache
     * @return Observable ready to be subscribed to
     */
    public Observable<Response<JSONObject>> getPreparedObservable(Observable<Response<JSONObject>> unPreparedObservable, String key, boolean useCache) {

        Observable<Response<JSONObject>> preparedObservable = null;

        if (useCache)//this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(key);

        if (preparedObservable != null)
            return preparedObservable;

        //we are here because we have never created this observable before or we didn't want to use the cache...
        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        if (useCache) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(key, preparedObservable);
        }

        return preparedObservable;
    }

    public interface NetworkAPI {
        /**
         * @param url    : Full URL for the request.
         * @param params : HashMap containing parameters for the request in the form of Map<key, value> that will be added to the url for the getCache request.
         * @return
         */
        @GET
        @Headers("Content-Type: application/json")
        Observable<Response<JSONObject>> get(@Url String url, @QueryMap Map<String, String> params);

        /**
         * @param url  : Full URL for the request.
         * @param body : JSONObject that will be the raw body for the postWithAuth request.
         * @return
         */
        @POST
        @Headers("Content-Type: application/json")
        Observable<Response<JSONObject>> post(@Url String url, @Body JSONObject body);

        /**
         * @param url  : Full URL for the request.
         * @param body : String that will be the raw body for the postWithAuth request.
         * @return
         */
        @POST
        @Headers("Content-Type: application/x-www-form-urlencoded")
        Observable<Response<JSONObject>> post(@Url String url, @Body RequestBody body);

        @GET
        @Headers("Content-Type: application/x-www-form-urlencoded")
        Observable<Response<JSONObject>> get(@Url String url);


    }


    private class BasicAuthInterceptor implements Interceptor {

        private String credentials;

        public BasicAuthInterceptor(String user, String password) {
            this.credentials = Credentials.basic(user, password);
        }


        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request authenticatedRequest = request.newBuilder()
                    .header("Authorization", credentials).build();
            return chain.proceed(authenticatedRequest);
        }


    }


}
