package com.frank.plantilla.retrofit;


import android.os.Bundle;

import com.frank.plantilla.retrofit.converter.StringConverter;
import com.frank.plantilla.retrofit.observer.ResponseCallback;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;


public class RestClient {
    public static final String PROPERTY_REG_ID = "registration_id";
    public static RestInterface mRestInterface;
    private static Call call = null;


    public static void getFileText(final Responses.OnResponse mOnResponse,
                                   String url, String request, JSONObject data) {
        Retrofit client = new Retrofit.Builder()
                .client(new OkHttpClient())
                .addConverterFactory(new StringConverter())
                .baseUrl("http://apps.tr3sco.net/")
                .build();
        RestInterface mRestInterface = client.create(RestInterface.class);
        call = mRestInterface.post(url, data.toString());
        call.enqueue(new ResponseCallback(request, mOnResponse));
    }

    public static void postText(final Responses.OnResponse mOnResponse,
                                   String url, String request, String data) {
        Retrofit client = new Retrofit.Builder()
                .client(new OkHttpClient())
                .addConverterFactory(new StringConverter())
                .baseUrl("http://apps.tr3sco.net/")
                .build();
        RestInterface mRestInterface = client.create(RestInterface.class);
        call = mRestInterface.post(url, data);
        call.enqueue(new ResponseCallback(request, mOnResponse));
    }


    public interface RestInterface {

        @GET
        @Headers("Content-Type: application/json")
        Call<Bundle> getVersionControl(
                @Url String url,
                @Query("appId") String app,
                @Query("platform") String platform,
                @Query("version") String version
        );

        @GET
        @Headers("Content-Type: application/json")
        Call<JSONObject> getAllSections(
                @Url String url,
                @QueryMap Map<String, String> params
        );

        @POST
        @Headers("Content-Type: application/json")
        Call<String> post(@Url String url, @Body String body);

        @GET
        @Headers("Content-Type: application/json")
        Call<String> getFileText(
                @Url String url
        );

    }
}
