package com.frank.plantilla.retrofit.observer;

import android.content.Context;
import android.os.Bundle;

import com.frank.plantilla.R;

import com.frank.plantilla.retrofit.Responses;
import com.frank.plantilla.model.classes.constants.Constants;
import com.frank.plantilla.model.classes.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import retrofit2.Response;
import rx.Observer;


/**
 * Created by Frank on 12/29/15.
 */
public class CacheObserver implements Observer {

    private boolean onNext = false;
    private final String requestKey;
    private final Responses.OnResponse mResponse;
    private final Context context;

    public CacheObserver(final String requestKey, final Context context, Responses.OnResponse mResponse) {
        this.requestKey = requestKey;
        this.mResponse = mResponse;
        this.context = context;
    }

    @Override
    public void onCompleted() {
        if (!onNext) {
            mResponse.onResponse(requestKey, "Fail", null);
        }
    }

    @Override
    public void onError(Throwable e) {
        Bundle cache = null;
        try {
            cache = Utils.getCache(context, requestKey);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (cache != null) {
            mResponse.onResponse(requestKey, cache, null);
        } else {
            if (e instanceof UnknownHostException) {
                mResponse.onResponse(Constants.Service.NO_CONNECTION, context.getString(R.string.ServerErrorConecction), null);
            } else if (e instanceof SocketTimeoutException || e instanceof TimeoutException) {
                mResponse.onResponse(Constants.Service.NO_CONNECTION, context.getString(R.string.ServerErrorConecction2), null);
            } else {
                mResponse.onResponse(requestKey, "Contacta a mesa de ayuda\n" + e, null);
            }
        }
    }

    @Override
    public void onNext(Object o) {
        onNext = true;
        Response<JSONObject> bundleResponse = (Response<JSONObject>) o;
        JSONObject response = bundleResponse.body();

        Bundle resp;
        try {
            resp = Utils.createBundle(response);
        } catch (JSONException e) {
            resp = null;
        }

        if (bundleResponse.isSuccessful()) {
            Utils.insertCache(context, requestKey, response);
            mResponse.onResponse(requestKey, resp, null);
        } else {
            mResponse.onResponse(requestKey, bundleResponse.message(), null);
        }

    }
}
