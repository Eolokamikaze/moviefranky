package com.frank.plantilla.retrofit;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Frank on 12/29/15.
 */
public class BenInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request initialRequest = chain.request();

        String url = initialRequest.url().toString();

        Log.e("BEN", url);

        return chain.proceed(initialRequest.newBuilder()
                .addHeader("Authorization", "Basic VHIzc2NvQVBJOnQ4MzVDMDRQMUAjMjAxNw==")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Content-Type", "application/json")
                .url(url).build());
    }
}

